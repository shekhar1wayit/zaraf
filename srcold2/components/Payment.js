import React, {Component} from 'react';
import {Platform, StyleSheet, TextInput, SafeAreaView, Text, View, Image, ImageBackground, Button, TouchableOpacity} from 'react-native';
import Constants from '../constants/Constants';
import Service from '../services/Service';
import MyView from './MyView';
import { strings } from '../services/stringsoflanguages';
import OfflineNotice from './OfflineNotice';
export default class Payment extends Component {
 constructor(props){
     super(props);
     service = new Service();
     constants = new Constants();
     this.state = {
        userData: { picture_large:{ data:{}}},
        search : true,
        notCount : "",
        dummyTextFav: strings.NorecordFound
      };
   
 }

 componentDidMount() {
   service.getUserData('notificationCount').then((keyValue) => {
    if(keyValue !== "none" || keyValue !== undefined || keyValue !== "")
    {
    this.setState({notCount : keyValue})
    }
    }, (error) => {
    console.log(error) //Display error
    });
 }

 goToNotification = () => {
  this.props.navigation.navigate('Notifications')
  }

 openDrawer = () => {
   this.props.navigation.openDrawer()}

   searchPage = () =>{
    this.setState({ search: false});
        }

  render() {
   
    return (
        
     <SafeAreaView
      source={constants.loginbg}
      style={styles.container}>
    <OfflineNotice/> 
    <View style={styles.topView}>
    <MyView style={styles.tabsToolbar} >
        <TouchableOpacity onPress={() => this.openDrawer()}>
        <Image source={constants.menuicon} style={styles.hamburgerIcon} />
        </TouchableOpacity>
         <Text style={styles.toolbarTitle}>{strings.Payment}</Text>
         <TouchableOpacity onPress={() => this.goToNotification()}>
        <Image source={constants.notificationIcongrey} style={styles.searchIcon} />
        { this.state.notCount == 0 || this.state.notCount == "" ?  null : <TouchableOpacity
         style={{
          borderWidth:1,
          borderColor:'rgba(0,0,0,0.2)',
          alignItems:'center',
          justifyContent:'center',
          width:18,
          height:18,
          position:'absolute',
          top:2,
          left:14,
          backgroundColor:'#fb913b',
          borderRadius:9,
        }}
 >
   <Text style={{color:"#fff"}}>{this.state.notCount}</Text>
 </TouchableOpacity>}
</TouchableOpacity>
     </MyView>
     </View>
     <View>
     <View style={{marginTop:10}}>
     <Text style={styles.defaultTextSize}>{this.state.dummyTextFav}</Text>
      </View>
     </View>
 </SafeAreaView>
      
     
    );
  }
}
