import React, { Component } from 'react';
import { Platform, Keyboard,StyleSheet, SafeAreaView, ScrollView, Alert, TouchableHighlight, TextInput, Modal, FlatList, Text, View, Image, ImageBackground, Button, TouchableOpacity } from 'react-native';
import Constants from '../constants/Constants';
import Service from '../services/Service';
import Tabs from './Tabs';
import Details from './Details';
import Loader from './Loader';
import FEED from './Feed';
import FCM, {FCMEvent} from "react-native-fcm";
import MyView from './MyView';
import firebase  from './Config';
import CustomToast from './CustomToast';
import OfflineNotice from './OfflineNotice';
import strings from '../services/stringsoflanguages';
import { colors, fonts, padding, dimensions, align } from '../styles/base';
export default class Home extends Component {

state = {
userResponse: {},
feeds: { request_list: [] },
favourites: { request_list: [] },
heartIcon: constants.heartIcon,
showSoundImg: true,
isFeed: true,
loading: false,
isFav: false,
isCat: false,
search: true,
itemFav: false,
modalVisible: false,
noText: true,
noTextFav : true,
dummyText: "",
favStatus: false,
notCount : "",
Item: 0,
dummyTextFav :"",
refreshing: false
};

constructor(props) {
super(props);
service = new Service();
constants = new Constants();
}


updateValue(props) {
service.getUserData('user').then((keyValue) => {
console.log("local", keyValue);
var parsedData = JSON.parse(keyValue);
console.log("json", parsedData);
this.setState({ userResponse: parsedData });
this.getFeedRes();
this.getActiveJobs();
}, (error) => {
console.log(error) //Display error
});
}

pressIcon = (val, index) => {
console.log(val);
if (val.isFavourite == 0) {
this.setState({ itemFav: true });
this.setState({ Item: 1 });
}
else {
this.setState({ Item: 0 });
}
this.setState({ favStatus: !this.state.favStatus })
console.log(this.state.Item)

this.calApi(val);

}

calApi = (val) => {

  var b = 0;

  if (val.isFavourite == "0"){

b = 1;
  }else{

    b = 0;

  }
var c = "shdsdh";

if (val.job_id == undefined){

  c = val.jobid;

}else{

  c = val.job_id;

}


service.addFav(this.state.userResponse.api_token, c,  b).then((res) => {
console.log("checkres", res);

newres = JSON.stringify(res);
json = JSON.parse(newres);
if (json.status_code == 200) {
if (this.state.Item == 1) {
this.refs.defaultToastBottom.ShowToastFunction(strings.JobAddedtoFavouritesSuccessfully);
}
else {
this.refs.defaultToastBottom.ShowToastFunction(strings.JobRemovedFromFavoritesSuccessfully);
}
this.getFeedRes();
this.getActiveJobs();
}
})
}

setModalVisible(visible) {
this.setState({ modalVisible: visible });
}

openDetails = (val) => {
// console.log(this.props);
this.props.navigation.navigate('Detail', { details: val })
}


checkLanguage = () => {
service.getUserData("language").then(
keyValue => {
if (keyValue == "true") {
strings.setLanguage("en");
} else {
strings.setLanguage("ar");
}

},
error => {
console.log(error); //Display error
}
);

}



componentDidMount() {
 
  firebase.notifications().getBadge()
  .then( count => {
   console.log("count", count)
if(count == 0)
{
FCM.getInitialNotification().then(notif => {
console.log("newtest", notif)
if(notif.from !== undefined)
{
this.props.navigation.navigate('Notifications') 
}
})
}
})
  const {navigation} = this.props;
  navigation.addListener ('willFocus', () =>
  this.getFeedRes()
  );
    service.getUserData('notificationCount').then((keyValue) => {
    console.log(keyValue) 
    if(keyValue == "none" || keyValue == undefined || keyValue == "")
    {
    service.saveUserData("notificationCount", 0); 
    }
    else
    {
        this.setState({notCount : keyValue})
    }
    }, (error) => {
    console.log(error) //Display error
    });
  
this.setState({ loading: true });
setTimeout(() => {
service.getUserData('user').then((keyValue) => {
console.log("local", keyValue);
var parsedData = JSON.parse(keyValue);
console.log("json", parsedData);
this.setState({ userResponse: parsedData });
this.getFeedRes();
this.getActiveJobs();
this.checkLanguage();
this.setState({ loading: false });
}, (error) => {
console.log(error) //Display error
});
}, 3000)
}

addToFavourites = () => {
console.log("heartfilled" + constants.paymentIcon)
if (this.state.icon === constants.heartIcon) {
this.setState({ heartIcon: constants.heartIconfilled });
}
else {
this.setState({ heartIcon: constants.heartIcon });
}
}

getFeedRes = () => {
service.getFeedList(this.state.userResponse.api_token).then((res) => {
console.log("checkres", res);
if (res != undefined) {
if (res.request_list == "") {
this.setState({ dummyText: strings.NoRequestFound });
this.setState({ noText: false });
this.setState(
  {
    refreshing: false
  })
}
newres = JSON.stringify(res);
json = JSON.parse(newres);
this.setState({ feeds: json });
}
})
}

getActiveJobs = () => {
service.getFavJobList(this.state.userResponse.api_token).then((res) => {
console.log("listcheckres", res);
if (res != undefined) {
  if (res.request_list == "") {
    this.setState({ dummyTextFav: strings.NorecordFound  });
    this.setState({ noTextFav: false });
  }
  else
  {
    this.setState({ noTextFav: true });
    this.setState({ dummyTextFav: ""  });
  }
  this.setState(
    {
      refreshing: false
    })
newres = JSON.stringify(res);
json = JSON.parse(newres);
this.setState({ favourites: json });
}
})
}

hideSearch = () => {
this.setState({ search: true });
}


handleRefresh() {
  this.setState(
    {
      refreshing: true
    })
    
    this.getFeedRes()
    this.getActiveJobs()
  
}

hideTab = () => {
this.setState({ isFeed: true });
this.setState({ isFav: false });
this.getFeedRes()
this.getActiveJobs()
}


hideTab2 = () => {
this.setState({ isFeed: false });
this.setState({ isFav: true });
this.getFeedRes()
this.getActiveJobs()
}

openDrawer = () => {
Keyboard.dismiss();
this.props.navigation.openDrawer()
}

goToNotification = () => {
this.props.navigation.navigate('Notifications')
}

searchPage = () => {
this.setState({ search: false });
}

openDetails = (val) => {
console.log(val);
service.saveUserData('userdetails', val);
this.props.navigation.navigate('Details', { details: val })
}

notification = (val) => {
    service.getUserData('notificationCount').then((keyValue) => {
      if(keyValue !== "none" || keyValue !== undefined || keyValue !== "")
      {
      var count = ++keyValue;
      service.saveUserData("notificationCount", count); 
      }
      }, (error) => {
      console.log(error) //Display error
      });
     // this.props.navigation.navigate('Notifications')
  }

  OpenNotification = (value) => {
    console.log("val", value)
   this.props.navigation.navigate('Notifications')
  }


render() {
// console.log(this.state.favourites)
return (

<SafeAreaView
style={styles.safeAreaContainer}>
<OfflineNotice />


<MyView style={styles.tabsToolbar} hide={!this.state.search}>
<TouchableOpacity onPress={() => this.openDrawer()}>
<Image source={constants.menuicon} style={styles.hamburgerIcon} />
</TouchableOpacity>
<Text style={styles.toolbarTitle}>{strings.Freelancerstring}</Text>
<TouchableOpacity onPress={() => this.goToNotification()}>
<Image source={constants.notificationIcongrey} style={styles.searchIcon} />
{ this.state.notCount == 0 || this.state.notCount == "" ?  null : <TouchableOpacity
         style={{
          borderWidth:1,
          borderColor:'rgba(0,0,0,0.2)',
          alignItems:'center',
          justifyContent:'center',
          width:18,
          height:18,
          position:'absolute',
          top:2,
          left:14,
          backgroundColor:'#fb913b',
          borderRadius:9,
        }}
 >
   <Text style={{color:"#fff"}}>{this.state.notCount}</Text>
 </TouchableOpacity>}
</TouchableOpacity>

</MyView>


<View style={styles.tabs} >
<View style={styles.empty}>
</View>
<TouchableOpacity  style= {[ this.state.isFeed == true ? styles.selectedTab1 : styles.Tab1]} onPress={() => this.hideTab()}>
<Text style={{ color: this.state.isFeed == false ? "white" : colors.theme_orange, textAlign: 'center', paddingTop: 15, left: -10, width: dimensions.fullWidth / 2 }}>{strings.FEEDstring}</Text>
</TouchableOpacity>
<TouchableOpacity  style= {[ this.state.isFav == true ? styles.selectedTab3 : styles.Tab3]} onPress={() => this.hideTab2()}>
<Text style={{ color: this.state.isFeed == true ? "white" : colors.theme_orange, textAlign: 'center', paddingTop: 15, width: dimensions.fullWidth / 2 }}>{strings.FAVOURITEstring}</Text>
</TouchableOpacity>
<View style={styles.empty}>
</View>
</View>

<MyView hide={!this.state.isFeed} >

<MyView style={styles.noTextContainer} hide={this.state.noText}>
<Text style={styles.defaultTextSize}>{this.state.dummyText}</Text>
</MyView>
<FlatList
data={this.state.feeds.request_list}
keyExtractor={(item, index) => index}
style={{ marginTop: 10, marginBottom:dimensions.fullHeight - 570 }}
extraData={this.state.feeds.request_list}
onRefresh={this.handleRefresh.bind(this)}
refreshing={this.state.refreshing}
renderItem={({ item, index }) => (
<View style={styles.spaceFromTop}>
{item.jobStatus === "Rejected" ? null :
<TouchableOpacity style= {[ item.jobStatus == "Accepted" || item.jobStatus == "Hired" ? styles.listCardActive : styles.listCardJobs]} onPress={() => this.openDetails(item)}>
<View style={styles.textInRow} >
<Text style= {[ item.jobStatus == "Accepted" || item.jobStatus == "Hired" ? styles.textWrapWhite : styles.textWrap]} > {item.job_data.title}
</Text>
</View>
<View style={styles.textInRow} >
<View >
<Text style= {[ item.jobStatus == "Accepted" || item.jobStatus == "Hired" ? styles.status : styles.priceText]}>{strings.FixedPrice}</Text>
</View>
<View style={styles.contPadding}>
<Text style= {[ item.jobStatus == "Accepted" || item.jobStatus == "Hired" ? styles.whiteText: styles.textB]}>-</Text>
</View>
<View >
<Text style= {[ item.jobStatus == "Accepted" || item.jobStatus == "Hired" ? styles.dateActive : styles.date]}>{item.job_data.budget} SAR </Text>
</View>
</View>
<View style={styles.paddingAbove}>
<View style={styles.textInRow} >
<View >
<Text style= {[ item.jobStatus == "Accepted" || item.jobStatus == "Hired" ? styles.status : styles.priceText]}>{strings.Status}</Text>
</View>
<View style={styles.contPadding}>
<Text style= {[ item.jobStatus == "Accepted" || item.jobStatus == "Hired" ? styles.whiteText : styles.textB]}>-</Text>
</View>
<View >
<Text style= {[ item.jobStatus == "Accepted" || item.jobStatus == "Hired" ? styles.dateActive : styles.date]}>{item.jobStatus} </Text>
</View>
</View>
</View>
<View style={styles.paddingAbove}>
<View style={styles.textInRow2}>
<View style={styles.skillWidth} >
<Text style={styles.skillText}></Text>
</View>
<View style={styles.budgetWidth}>
</View>
<TouchableOpacity style={{ width: 60, height: 50, marginTop: -70, marginLeft: -20, alignItems: "center", justifyContent: "center" }} onPress={() => this.pressIcon(item, index)}>
<Image source={item.isFavourite == 0 ? constants.heartIcon : constants.heartIconfilled} style={{ width: 20, height: 20 }} />
</TouchableOpacity>
</View>
</View>
</TouchableOpacity>
}
</View>
)}
/>

</MyView>


<MyView hide={!this.state.isFav}>
<MyView hide={this.state.noTextFav}>
<Text style={styles.defaultTextSize2}>{this.state.dummyTextFav}</Text>
</MyView>
<FlatList
data={this.state.favourites.request_list}
keyExtractor={(item, index) => index}
style={{ marginTop: 10 }}
refreshing={this.state.refreshing}
onRefresh={this.handleRefresh.bind(this)}
renderItem={({ item, index }) => (
<View style={styles.spaceFromTop}>
{item.request_status === "Rejected" ? null :
<TouchableOpacity  style= {[ item.request_status == "Accepted" || item.request_status == "Hired" ? styles.listCardActive : styles.listCardJobs]} onPress={() => this.openDetails(item)}>
<View style={styles.textInRow} >
<Text style= {[ item.request_status == "Accepted" || item.request_status == "Hired" ? styles.textWrapWhite : styles.textWrap]}> {item.title}
</Text>
</View>
<View style={styles.textInRow} >
<View >
<Text style= {[ item.request_status == "Accepted" || item.request_status == "Hired" ? styles.status : styles.priceText]}>{strings.FixedPrice}</Text>
</View>
<View style={styles.contPadding}>
<Text style= {[ item.request_status == "Accepted" || item.request_status == "Hired" ? styles.whiteText: styles.textB]}>-</Text>
</View>
<View >
<Text style= {[ item.request_status == "Accepted" || item.request_status == "Hired" ? styles.dateActive : styles.date]}>{item.budget} SAR</Text>
</View>
</View>
<View style={styles.paddingAbove}>
<View style={styles.textInRow} >
<View >
<Text style= {[ item.request_status == "Accepted" || item.request_status == "Hired" ? styles.status : styles.priceText]}>{strings.Status}</Text>
</View>
<View style={styles.contPadding}>
<Text style= {[ item.request_status == "Accepted" || item.request_status == "Hired" ? styles.whiteText : styles.textB]}>-</Text>
</View>
<View >
<Text style= {[ item.request_status == "Accepted" || item.request_status == "Hired" ? styles.dateActive : styles.date]}>{item.request_status} </Text>
</View>
</View>
</View>
<View style={styles.paddingAbove}>
<View style={styles.textInRow2}>
<View style={styles.skillWidth} >
<Text style={styles.skillText}></Text>
</View>
<View style={styles.budgetWidth}>
</View>
<TouchableOpacity style={{ width: 60, height: 50, marginTop: -70, marginLeft: -20, alignItems: "center", justifyContent: "center" }} onPress={() => this.pressIcon(item, index)}>
<Image source={item.isFavourite == 0 ? constants.heartIcon : constants.heartIconfilled} style={{ width: 20, height: 20 }}/>
</TouchableOpacity>
</View>
</View>
</TouchableOpacity>
}
</View>
)}
/>
</MyView>

<CustomToast ref="defaultToastBottom" />
<Loader
loading={this.state.loading} />
</SafeAreaView>
);
}
}