import React, {Component} from 'react';
import {Platform, StyleSheet, TextInput, SafeAreaView, Text, View, Image, ImageBackground, Button, TouchableOpacity} from 'react-native';
import Constants from '../constants/Constants';
import Service from '../services/Service';
import MyView from './MyView';
import { strings } from '../services/stringsoflanguages';
import Stars from 'react-native-stars';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
// import Stars from 'react-native-stars-rating';

export default class Rating extends Component {
 constructor(props){
     super(props);
     service = new Service();
     constants = new Constants();
     this.state = {
        userData: { picture_large:{ data:{}}},
        search : true,
        starCount : 2,
        reviewText : "",
        userResponse: {},
        freelancer_Key : "bw53xw2H8T92UN6BqoMbBIexeUobBOp65jK1erTSEgOKmQpxdXdaAJPc9zDx",
        project_id:"95"
      };
   
 }

 componentDidMount() {

service.getUserData("user").then(
keyValue => {
console.log("local", keyValue);
var parsedData = JSON.parse(keyValue);
console.log("json", parsedData);
this.setState({ userResponse: parsedData });
},
error => {
console.log(error); //Display error
}
);
}


 openDrawer = () => {
   this.props.navigation.openDrawer()}

   submitReview = () => {
    console.log("submit");
    
    if (this.state.reviewText == "" || this.state.reviewText.length == 0){
    
    
    }else{
    
    service.ratingAction(this.state.userResponse.api_token,this.state.freelancer_Key,this.state.project_id, this.state.reviewText,this.state.starCount)
    .then(res => {
    console.log(this.state.userResponse.api_token);
    console.log(this.state.feedback);
    
    if (res != undefined) {
    if (res.status_code == 200) {
    if (res.status == "success") {
    
    
    } else {
    
    
    }
    }
    } else {
    }
    });
    }
    
    }
    
    cancelReview = () => {
    console.log("cancel");
    
    
    }
    
    
    onStarRatingPress(rating) {
    console.log(rating);
    this.setState({
    starCount: rating
    });
    }


  render() {
   
    return (
        
     <SafeAreaView
      source={constants.loginbg}
      style={styles.container}>
     {/* <CreditCardInput onChange={this._onChange} />  */}
    <View style={styles.topView}>
       <MyView  hide={this.state.search} style={styles.searchContainer}>
          <View style={styles.topSearchbar}>
              <Image source={constants.searchicon} style={styles.newsearchIcon} />
              <View style={styles.empty}>
              </View>
            <TextInput  style={styles.searchContainer} placeholder="Search job"  placeholderTextColor="white" style={styles.topInput}/>
          </View>
      </MyView>
      <View style={styles.toolbar}>
        <Text style={styles.backButton} onPress={() => this.goToHome()}>
        <Image source={constants.backicon} style={styles.iconRating}/>
        </Text>
          <Text style={styles.toolbarTitle}>RATE&REVIEW</Text>
          <TouchableOpacity onPress={() => this.goToUpdateProfile()}>
          <Image  style={styles.searchIcon} />
          </TouchableOpacity>
        </View>
     </View>
     <View style={styles.homeContent}>
         
         <TextInput
            style={styles.review}
            underlineColorAndroid="transparent"
            placeholder={strings.Writeareview}
            onChangeText={(text)=>this.setState({ reviewText:text})}
            placeholderTextColor="#AEA9A8"
            autoCapitalize="none"
            returnKeyType='done'
            value={this.state.about}
            multiline={true}
            numberOfLines={4}
          />
         
         
     </View>
     <View style={{marginTop:20 }}>
     <Stars
    default={2.5}
    count={5}
    half={true}
    starSize={50} 
    update={(val)=>{this.onStarRatingPress(val)}}
    fullStar={<Icon name={'star'} style={[styles.myStarStyle]}/>}
    emptyStar={<Icon name={'star-outline'} style={[styles.myStarStyle, styles.myEmptyStarStyle]}/>}
    halfStar={<Icon name={'star-half'} style={[styles.myStarStyle]}/>}
  />
      
       
      </View>
      <View  style={styles.sideAlign}>
              <View style={styles.emptySpaceRating}>
              </View>
              <View style={styles.buttonWidthRating}>
              <TouchableOpacity style={styles.cancelButton} onPress={() => this.requestAcceptReject('a')}>
		          <Text style={styles.buttonText}>{strings.Cancel}</Text>
		           </TouchableOpacity>
               {/* <Button  style={styles.buttonColor} title="Accept" onPress={() => this.requestAcceptReject('a')}></Button> */}
              </View>
              <View style={styles.emptySpaceRating}>
              </View>
              <View style={styles.buttonWidthRating}>
              <TouchableOpacity style={styles.cancelButton} onPress={() => this.submitReview()}>
		          <Text style={styles.buttonText}>{strings.Submit}</Text>
		           </TouchableOpacity>
                {/* <Button  style={styles.buttonColor} title="Reject" onPress={() => this.requestAcceptReject('r')}></Button> */}
              </View>
                <View style={styles.emptySpaceRating}>
              </View>
      </View> 
     
     
              
 </SafeAreaView>
      
     
    );
  }
}
