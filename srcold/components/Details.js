// Screen for accept and refusal of freelancer requests
import React, {Component} from 'react';
import {Platform, StyleSheet, Text,Alert, View, Button, ScrollView, SafeAreaView,Image, ImageBackground, ActivityIndicator, TouchableOpacity, TouchableNativeFeedback} from 'react-native';
import styles from '../styles/styles';
import Constants from '../constants/Constants';
import Loader from './Loader';
import HTMLView from 'react-native-htmlview';
import CustomToast from './CustomToast';
import Service from '../services/Service';
import MyView from './MyView';
import Home from './Home';
import { strings } from '../services/stringsoflanguages';
import OfflineNotice from './OfflineNotice';
import { dimensions } from '../styles/base';
export default class Details extends Component {
  constructor(props){
    super(props);
    constants = new Constants();
    service = new Service();
    home = new Home();
    this.state = { 
      userResponse: {},
        details : {},
        loading: false,
        accepted : false,
        localData : {},
        hired : true,
        clientName : "",
        clientId  : "",
        newHide : true,
        clientDetails: "",
        clientImage : ""
      }
  }

 componentDidMount() {
    this.setState ({ loading: true});
    setTimeout(() => {
      this.setState ({ loading: false});
      service.getUserData('user').then((keyValue) => {
        console.log("local", keyValue);
        var parsedData = JSON.parse(keyValue);
        console.log("json", parsedData);
        this.setState({ userResponse: parsedData});
     }, (error) => {
        console.log(error) //Display error
      });
      service.getUserData('userdetails').then((keyValue) => {
        console.log("local", keyValue);
        var parsedData = JSON.parse(keyValue);
        console.log("json", parsedData);
        this.setState({ details: parsedData});
        this.getDetails(parsedData);
     }, (error) => {
        console.log(error) //Display error
      });
      if(this.props.navigation.state.params)
    {
   // this.setState({ details: this.props.navigation.state.params.details})
      if(this.props.navigation.state.params.details.accepted != "Pending" && this.props.navigation.state.params.details.accepted != undefined )
      {
        this.setState({ accepted : true})
      }
     }    
      }, 1000)
    
  }

  getDetails = (object) => {
    // console.log(object)


    var c = "shdsdh";

if (object.job_id == undefined){

  c = object.jobid;

}else{

  c = object.job_id;

}
    service.getFreelancerDetails(this.state.userResponse.api_token, c).then((res) => {
      console.log("listcheckres", res);
      if(res != undefined)
      {
        this.setState({newHide:false});
      if(res.request_list.accepted == "Hired" || res.request_list.accepted == "Accepted")
      {
        this.setState({ hired: false});
      }
      if(res.users)
      {
        this.setState({  clientId : res.users.id})
        this.setState({  clientName : res.users.username})
        this.setState({  clientName : res.users.username})
      }
      this.setState({ details: res.request_list});
      this.setState({ clientDetails: res.users});
      if(this.state.details.accepted != "Pending")
      {
        this.setState({ accepted : true})
      }
      }
    })
   }

  requestAcceptReject = (val) => 
  {
  this.setState ({ loading: true});
  setTimeout(() => {
    this.setState ({ loading: false});
    if(val == "a")
    {
      service.requestResponse(this.state.userResponse.api_token, "Accepted", this.state.details.jobid).then(res => {
        console.log("reslocal", res);
        if(res)
        {
          this.refs.defaultToastBottom.ShowToastFunction(strings.Requestaccpetsuccessfully);
          this.goToHome();
        }
        else
        {
          this.refs.defaultToastBottom.ShowToastFunction('An Error Occured');
        }
      });
     
    }
    else
    {
      service.requestResponse(this.state.userResponse.api_token, "Rejected", this.state.details.jobid).then(res => {
        console.log("reslocal", res);
        if(res)
        {
          this.refs.defaultToastBottom.ShowToastFunction(strings.RequestReject);
          this.goToHome();
        }
        else
        {
          this.refs.defaultToastBottom.ShowToastFunction('An Error Occured');
        }
      });
    }
    }, 2000)
  }

  openChat = () => {
   var Details = {
     id : this.state.clientId,
     receiverName : this.state.clientName,
     receiverImage : this.state.clientImage
   }
   this.props.navigation.navigate('Chat', { chatDetails: Details})
  }
  goBack = () =>{
    this.props.navigation.navigate('Home')
   }

goToHome()
{

  this.props.navigation.navigate('Home')


// setTimeout(() => {
// this.props.navigation.navigate('Home')
// }, 1000)
}

createMilestone()
{
setTimeout(() => {
this.props.navigation.navigate('Create')
}, 1000)
}

viewClientProfile = () => {
  this.props.navigation.navigate('ClientProfile', { chatDetails: this.state.clientDetails})
}
  
  render() {
     // console.log(this.state.details)
    return (
  <SafeAreaView style = { styles.MainContainerRequest }>
    <OfflineNotice/> 
        <View style={styles.commontoolbar}>
          <TouchableOpacity style={styles.commontoolbarButton} onPress={() => this.goBack()}>
          <Image source={constants.backicon} style={styles.commonBackIcon}/>
          </TouchableOpacity>
          <Text style={styles.toolbarTitle}>{strings.Jobdetail}</Text>
          <Text style={styles.commontoolbarButton}></Text>
        </View>
        <MyView style = { {justifyContent:"center",alignItems:"center",marginTop:10,marginLeft:10, width:dimensions.fullWidth - 20, height:50, backgroundColor:'#F0F0F0',  borderWidth:1} } >
      <TouchableOpacity onPress={() => this.viewClientProfile()}>
         <Text style={styles.textStyleChat}>{strings.ViewClientProfile}</Text>
      </TouchableOpacity>
      </MyView>
        <ScrollView>
        <View style={{marginTop:20}}>
       <View style={styles.rowAlignSideMenuJob}>
                 <View style={{width:"20%"}}> 
                  </View>
                  <View style={{width:"25%"}}> 
                  <Text style={styles.textWrapDetails}> {strings.Title}
                  </Text>
                  </View>
                 
                  <View  style={{width:"10%"}}>
                  </View>
                  <View style={{width:"45%"}}> 
                  <Text style={styles.textWrap2Details}> {this.state.details.title}
                  </Text>
                  </View>
         </View>
         <View style={styles.rowAlignSideMenuJob2}>
                 <View style={{width:"20%"}}> 
                  </View>
                    <View style={{width:"25%"}}>
                  <Text style={styles.textWrapDetails}> {strings.City}
                  </Text>
                  </View>
                  <View  style={{width:"10%"}}>
                  </View>
                  <View style={{width:"45%"}}>
                  <Text style={styles.textWrap2Details}> {this.state.details.country}
                  </Text>
                  </View>
         </View>
        
         <View style={styles.rowAlignSideMenuJob}>
                   <View style={{width:"20%"}}> 
                  </View>
                  <View style={{width:"25%"}}>
                  <Text style={styles.textWrapDetails}>{strings.Budget}
                  </Text>
                  </View>
                 
                  <View  style={{width:"10%"}}>
                  </View>
                  <View style={{width:"45%"}}> 
                  <Text style={styles.amountDetails}>{this.state.details.budget} SAR
                  </Text>
                  </View>
         </View>
         <View style={styles.rowAlignSideMenuJob2}>
                  <View style={{width:"20%"}}> 
                  </View>
                  <View style={{width:"25%"}}>
                  <Text style={styles.textWrapDetails}>  {strings.Date}
                  </Text>
                  </View>
                  <View  style={{width:"10%"}}>
                  </View>
                  <View style={{width:"45%"}}> 
                  <Text style={styles.textWrap2Details}> {this.state.details.end_date}
                  </Text>
                  </View>
         </View>
        
         <View style={styles.rowAlignSideMenuJob}>
                 <View style={{width:"20%"}}> 
                  </View>
                  <View style={{width:"25%"}}>  
                  <Text style={styles.textWrapDetails}> {strings.Status}
                  </Text>
                  </View>
                  <View  style={{width:"10%"}}>
                  </View>
                  <View style={{width:"45%"}}> 
                  <Text style={styles.textWrap2Details}> {this.state.details.accepted}
                  </Text>
                  </View>
         </View>
         <View style={styles.rowAlignSideMenuJob2}>
                <View style={{width:"20%"}}> 
                  </View>
                  <View style={{width:"25%"}}> 
                  <Text style={styles.textWrapDetails}> {strings.Description}
                  </Text>
                  </View>
                  <View style={{width:"10%"}}>
                  </View>
                  <View  style={{width:"45%"}}> 
                  <Text style={{ textTransform: 'capitalize'}}> {this.state.details.description}
                  </Text>
                  </View>
         </View>
        
         <Loader
              loading={this.state.loading} /> 
              </View>
     </ScrollView>
     <Loader
              loading={this.state.loading} />
    <MyView hide={this.state.newHide} style={styles.footer}>
    <MyView  hide={ this.state.accepted }>
              <View  style={styles.rowAlignSideMenu}>
              <View style={styles.emptySpaceRequest}>
              </View>
              <View style={styles.buttonWidthRequest}>
              <TouchableOpacity style={styles.buttonBackgroundrequest} onPress={() => this.requestAcceptReject('a')}>
		          <Text style={styles.buttonText}>{strings.Accept}</Text>
		           </TouchableOpacity>
               {/* <Button  style={styles.buttonColor} title="Accept" onPress={() => this.requestAcceptReject('a')}></Button> */}
              </View>
              <View style={styles.emptySpaceRequest}>
              </View>
              <View style={styles.buttonWidthRequest}>
              <TouchableOpacity style={styles.buttonBackgroundrequest} onPress={() => this.requestAcceptReject('r')}>
		          <Text style={styles.buttonText}>{strings.Reject}</Text>
		           </TouchableOpacity>
                {/* <Button  style={styles.buttonColor} title="Reject" onPress={() => this.requestAcceptReject('r')}></Button> */}
              </View>
                <View style={styles.emptySpaceRequest}>
              </View>
              </View> 
      </MyView> 
      <MyView style = { {justifyContent:"center",alignItems:"center",marginTop:10,marginLeft:10, width:dimensions.fullWidth - 20, height:50, backgroundColor:'#F0F0F0', borderWidth:1} } >
      <TouchableOpacity onPress={() => this.openChat()}>
         <Text style={styles.textStyleChat}>{strings.Chatwithclient}</Text>
      </TouchableOpacity>
      </MyView>
      </MyView>
     
      <CustomToast ref = "defaultToastBottom"/>  
   </SafeAreaView>
	   
    );
  }
}

