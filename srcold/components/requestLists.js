// Screen for accept and refusal of freelancer requests
import React, {Component} from 'react';
import {Platform,FlatList, StyleSheet, Text,Alert, View, Button, ScrollView, SafeAreaView,Image, ImageBackground, ActivityIndicator, TouchableOpacity, TouchableNativeFeedback} from 'react-native';
import styles from '../styles/styles';
import Constants from '../constants/Constants';
import Loader from './Loader';
import HTMLView from 'react-native-htmlview';
import CustomToast from './CustomToast';
import Service from '../services/Service';
import MyView from './MyView';
import Home from './Home';
import { strings } from '../services/stringsoflanguages';
import OfflineNotice from './OfflineNotice';
import { dimensions } from '../styles/base';
import { NavigationActions } from 'react-navigation'

export default class Details extends Component {
  constructor(props){
    super(props);
    constants = new Constants();
    service = new Service();
    home = new Home();
    this.state = { 
      userResponse: {},
        details : [],
        loading: false,
        accepted : false,
        localData : {},
        hired : true,
        clientName : "",
        clientId  : "",
        newHide : true,
        clientDetails: "",
        jobID: "",
        requestsText : " ",
        refreshing : true
      }
  }

 componentDidMount() {
   

    this.setState ({ loading: true});
    setTimeout(() => {
      this.setState ({ loading: false});
      service.getUserData('user').then((keyValue) => {
        console.log("local", keyValue);
        var parsedData = JSON.parse(keyValue);
        console.log("json", parsedData);
        this.setState({ userResponse: parsedData});
     }, (error) => {
        console.log(error) //Display error
      });
      if(this.props.navigation.state.params)
    {
        this.setState({jobID : this.props.navigation.state.params.job_id})
        this.getDetails()
        
     }    
      }, 1000)  
  }

  getDetails = () => {
    // console.log(object)
    service.getFreelancerDetails(this.state.userResponse.api_token, this.state.jobID).then((res) => {
      console.log("new data", res);
      if(res != undefined)
      {
        this.setState({ details: res.job_request});
        this.setState({ requestsText : strings.NoRequestFound});
        console.log("here are details",this.state.details)

      }
    })
    this.setState(
      {
        refreshing: false
      })
   }


   openProfile = (details) => {
    console.log(details)
   this.props.navigation.navigate('FProfile',  { fDetails: details });
   }
 

   
  goBack = () =>{

    const backAction = NavigationActions.back({
        key: null
      }) 


      this.props.navigation.dispatch(backAction);
       //this.props.navigation.dispatch(goBack())
}

onRefresh = () => {
  this.setState(
    {
      refreshing: true
    })
    this.getDetails();
}
      





  
  render() {
    const defaultImg =
    'https://satishrao.in/wp-content/uploads/2016/06/dummy-profile-pic-male.jpg'
     // console.log(this.state.details)
    return (
  <SafeAreaView style = { styles.MainContainerRequest }>
    <OfflineNotice/> 
        <View style={styles.commontoolbar}>
          <TouchableOpacity style={styles.commontoolbarButton} onPress={() => this.goBack()}>
          <Image source={constants.backicon} style={styles.commonBackIcon}/>
          </TouchableOpacity>
          <Text style={styles.toolbarTitle}>{strings.Requests}</Text>
          <Text style={styles.commontoolbarButton}></Text>
        </View>
       {this.state.details.length > 0 ? <FlatList
                    data={this.state.details}
                    keyExtractor={(item, index) => index}
                    style={{ marginTop: 10,marginBottom:30 }}
                    onRefresh={() => this.onRefresh()}
                    refreshing={this.state.refreshing}
                    extraData={this.state.details}
                    renderItem={({ item, index }) => (
                        <View style={styles.spaceFromTop}>
                        
                            <TouchableOpacity style={{width:dimensions.fullWidth - 30, backgroundColor:'#F0F0F0'}} onPress={() => this.openProfile(item)}>
                                {/* <View style={styles.textInRow} >
                                <Image source={{uri: item.userapi[0].image_path|| defaultImg  }} style={styles.freeLancerprofilePic} />
                                    <View style={{}}><Text style={{margin:5,fontSize:18,color:'#FF8000'}}> {item.userapi[0].username}
                                    </Text>
                                    <Text style={{margin:5,fontSize:16,color:'#FF8000', textTransform:"lowercase"}}> {item.userapi[0].email}
                                    </Text>
                                    <Text style={{margin:8,fontSize:16,color:'#B3B7BB'}}>{ item.jobStatus == null ? "Pending" : item.jobStatus}</Text>
                                    </View>
                                </View>  */}
                  <View style={styles.textInRowlist}> 
                 <View style = {styles.imageFreelancerContainer}>
                 <Image source={{uri: item.userapi[0].image_path|| defaultImg  }} style={styles.requestprofilePic} />
                  </View>
                  <View style={styles.textFreelancerContainer}>
                  <Text style={styles.email}>{item.userapi[0].username} <Text style={styles.requestUsernameText}>{item.userapi[0].email}</Text> </Text>
                  </View>
                  <View style={styles.emptyFreelancerContainer}>
                  <Text style={{margin:8,fontSize:16,color:'#B3B7BB'}}>{ item.jobStatus == null ? "Pending" : item.jobStatus}</Text>
                  </View>
                  <View style={styles.iconFreelancerContainer}>
                  <TouchableOpacity >
                  <Image style={styles.nextIcon} />
                   </TouchableOpacity>
                  </View>
              </View>                              
                            </TouchableOpacity>
                        </View>
                    )}
                        /> : <Text style={styles.defaultTextSize}> {this.state.requestsText}</Text>}
                        <Loader
loading={this.state.loading} />
   </SafeAreaView>
	   
    );
  }
}

