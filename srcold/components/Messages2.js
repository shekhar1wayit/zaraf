import React, { Component } from "react";
import {
  Platform,
  StyleSheet,
  FlatList,
  SafeAreaView,
  Text,
  View,
  Image,
  ImageBackground,
  Button,
  TextInput, 
  TouchableOpacity,
  Modal,
  ScrollView,
  Alert
} from "react-native";
import Constants from "../constants/Constants";
import Service from "../services/Service";
import styles from "../styles/styles";
import MyView from './MyView';
import Loader from './Loader';
import SideMenu from './SideMenu';
import firebase  from './Config';
import OfflineNotice from './OfflineNotice';
import { strings } from "../services/stringsoflanguages";
import { db } from './db';
let itemsRef = db.ref('/items');
export default class Messages2 extends Component {
  constructor(props) {
    super(props);
    service = new Service();
    constants = new Constants();
    this.state = {
      userResponse: {},
      jobs: [],
      failed: false,
      search : true,
      loading:false,
      noProject : " ",
      modalVisible: false,
      senderId : " ", 
      noMessage : " ",
      items: [],
      notCount : 0,
      refreshing: true,
      senderImage : "https://satishrao.in/wp-content/uploads/2016/06/dummy-profile-pic-male.jpg"
    };
    this.arrayholder = []
   
   
  }


  goToNotification = () => {
    this.props.navigation.navigate('Notifications')
    }

  

  componentDidMount() {

    service.getUserData('notificationCount').then((keyValue) => {
      if(keyValue !== "none" || keyValue !== undefined || keyValue !== "")
      {
      this.setState({notCount : keyValue})
      }
      }, (error) => {
      console.log(error) //Display error
      });
    this.setState({ loading: true });
    setTimeout(() => {
    this.setState({ loading: false });
    service.getUserData('user').then((keyValue) => {
      console.log("local", keyValue);
      var parsedData = JSON.parse(keyValue);
      console.log("json", parsedData);
      this.setState({ userResponse: parsedData});
      this.setState({ senderId: parsedData.id});
      if(parsedData.image_path !== null)
{
  this.setState({ senderImage: parsedData.image_path}); 
}
      this.chat();
   }, (error) => {
      console.log(error) //Display error
    });
  }, 3000 );
    
   }

   chat = () => {
    console.log("senderId", this.state.senderId);
    var messagesList = [];
    var senderId = this.state.senderId;
    itemsRef.child("users").on('value', (snapshot) => {
      console.log("usevalue", snapshot.val());
      snapshot.forEach(function(childSnapshot) {
       
        console.log("receiverID",  childSnapshot.val());
     //   console.log(this.state.receiverId);
        if(childSnapshot.val().senderId == senderId || childSnapshot.val().receiverId == senderId)
        {
         messagesList.push(childSnapshot.val());
        }
        console.log(childSnapshot.val())
      });
      console.log('messagesList', messagesList.length)
     // console.log(snapshot.val())
      if(messagesList.length !== 0)
      {    
      this.setState({items: messagesList});
      }
      else 
      {
      // alert('this obeee')
      this.setState ({  noMessage: strings.NomessagesFound });
      }
   });
   this.setState(
    {
      refreshing: false
    })
   }
  
    
   openDrawer = () => {
    this.props.navigation.openDrawer()}

 checkLanguage=()=>{
  service.getUserData("language").then(
    keyValue => {
     if(keyValue == "true")
     {
      strings.setLanguage("en");
    }else{
      strings.setLanguage("ar");
    }
        
    },
    error => {
      console.log(error); //Display error
    }
  );

 }
    
 onRefresh = () => {
  this.setState(
    {
      refreshing: true
    })
    this.chat();
}
      
 openChat = (val) => {
  // console.log(val)
   if(val.senderId !== this.state.userResponse.id)
   {
    var Ids = val.senderId;
    val.senderId = val.receiverId ;
    val.receiverId = Ids;
   }

   if(val.senderName !== this.state.userResponse.username)
   {
    var Name = val.senderName;
    val.senderName = val.receiverName ;
    val.receiverName = Name;
   }
  this.props.navigation.navigate('Chat', { chatDetails: val})
 }

  render() {
    return (
      <SafeAreaView source={constants.loginbg} style={styles.container}>
      <OfflineNotice/>
        <View style={styles.topView}>
        <MyView style={styles.tabsToolbar}>
        <TouchableOpacity onPress={() => this.openDrawer()}>
        <Image source={constants.menuicon} style={styles.hamburgerIcon} />
        </TouchableOpacity>
         <Text style={styles.toolbarTitle}>{strings.Messages}</Text>
        <TouchableOpacity onPress={() => this.goToNotification()}>
        <Image source={constants.notificationIcongrey} style={styles.searchIcon} />
        { this.state.notCount == 0 || this.state.notCount == "" || this.state.notCount == null ?  null : <TouchableOpacity
         style={{
          borderWidth:1,
          borderColor:'rgba(0,0,0,0.2)',
          alignItems:'center',
          justifyContent:'center',
          width:18,
          height:18,
          position:'absolute',
          top:2,
          left:14,
          backgroundColor:'#fb913b',
          borderRadius:9,
        }}
 >
   <Text style={{color:"#fff"}}>{this.state.notCount}</Text>
 </TouchableOpacity>}
        </TouchableOpacity>
        </MyView>
       </View>
       
      
       <Text style={styles.centerTextMessage}>{this.state.noMessage}</Text>
        <View style={styles.listCenter}>
      
        <FlatList
              data={this.state.items}
              keyExtractor={(item, index) => index}
              style={styles.listCardWidth}
              onRefresh={() => this.onRefresh()}
             refreshing={this.state.refreshing}
              extraData={this.state.items}
              renderItem={({ item, index }) => (
                <View  style={styles.spaceFromTop}>
                    <TouchableOpacity style={styles.listCardMessages}  onPress={() => this.openChat(item)}>

                      <View style={styles.textInRow} >
                                <View style={{width:'20%', marginTop:5}} >
                                {item.senderImage  == this.state.senderImage ?  <Image source={{ uri: item.receiverImage  }} style={{margin:5, width:50, height:50, borderRadius:25}} /> :  <Image source={{ uri: item.senderImage  }} style={{margin:5, width:50, height:50, borderRadius:25}} />  } 
                                    </View>
                                    <View style={{width:'80%'}} >
                                    <Text style={styles.messageTextColor}> {item.senderName == this.state.userResponse.username ? item.receiverName : item.senderName  } 
                                    </Text>
                                    <Text style={styles.date}> {item.time} 
                                    </Text>
                                    </View>
                                </View>          
                    
                        </TouchableOpacity>
                </View>
              )}
            />
        </View>
        <Loader
              loading={this.state.loading} />
      </SafeAreaView>
    );
  }
}
