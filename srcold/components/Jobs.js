import React, { Component } from "react";
import {
  Platform,
  StyleSheet,
  FlatList,
  SafeAreaView,
  Text,
  View,
  Image,
  ImageBackground,
  Button,
  TextInput, 
  TouchableOpacity,
  Modal,
  ScrollView,
  Alert,
  Keyboard
} from "react-native";
import Constants from "../constants/Constants";
import Service from "../services/Service";
import styles from "../styles/styles";
import FCM, {FCMEvent} from "react-native-fcm";
import MyView from './MyView';
import Loader from './Loader';
import SideMenu from './SideMenu';
import firebase  from './Config';
import OfflineNotice from './OfflineNotice';
import { strings } from "../services/stringsoflanguages";
export default class Jobs extends Component {
  constructor(props) {
    super(props);
    service = new Service();
    sidemenu = new SideMenu();
    constants = new Constants();
    this.state = {
      userResponse: {},
      jobs: [],
      failed: false,
      search : true,
      loading:false,
      noProject : " ",
      modalVisible: false,
      notCount : "",
      refreshing: true
    };
    this.arrayholder = []
   
   
  }

  findFreelancer = () => {

  }


  searchFilterFunction = text => {
    const newData = this.arrayholder.filter(item => {
      const itemData = `${item.title.toUpperCase()}`;
      const textData = text.toUpperCase();
      return itemData.indexOf(textData) > -1;
    });
    this.setState({
      jobs: newData
    });
  };


 checkLanguage=()=>{

  
  service.getUserData("language").then(
    keyValue => {
      console.log(keyValue)
   //   console.log("val", keyValue);
     if(keyValue == "true")
     {
      strings.setLanguage("en");
    }else{
      strings.setLanguage("ar");
    }
        
    },
    error => {
      console.log(error); //Display error
    }
  );

 }


  componentDidMount() {
    this.setState ({ loading: true});
    firebase.notifications().getBadge()
          .then( count => {
           console.log("count", count)
  if(count == 0)
   {
      FCM.getInitialNotification().then(notif => {
      console.log("newtest", notif)
      if(notif.from !== undefined)
      {
      this.props.navigation.navigate('Notifications') 
      }
     })
   }
  })
   
    service.getUserData('notificationCount').then((keyValue) => {
      console.log(keyValue) 
      if(keyValue == "none" || keyValue == undefined || keyValue == "" || keyValue == '""')
      {
      service.saveUserData("notificationCount", 0); 
      }
      else
        {
            this.setState({notCount : keyValue})
        }
      }, (error) => {
      console.log(error) //Display error
      });
  
      
    this.checkLanguage();

  setTimeout(() => {
    this.setState ({ loading: false});
    service.getUserData("user").then(
      keyValue => {
        console.log("local", keyValue);
        var parsedData = JSON.parse(keyValue);
        console.log("json", parsedData);
        this.setState({ userResponse: parsedData });
        this.getFreelancersResponse();
      },
      error => {
        console.log(error); //Display error
      }
    );
   // service.saveUserData('count', '1');
    }, 3000)
  }

  
  openDrawer = () => {
    // sidemenu.userData();
    Keyboard.dismiss();
    this.props.navigation.openDrawer();
  };

  

  getFreelancersResponse = () => {
    service.jobs(this.state.userResponse.api_token).then(res => {
      console.log(res);
     //  console.log("reslocal", res.Job.length);
      if(res.results.length ==  0)
      {
      //   alert("test")
        this.setState ({ noProject : strings.NoJobFound });
      }
      else
      {
      this.arrayholder =res.results;
      this.setState({ jobs: res.results});
      }

      this.setState(
        {
          refreshing: false
        })
    });
  };

  onRefresh = () => {
    this.setState(
      {
        refreshing: true
      })
      this.getFreelancersResponse();
  }
  goToNotification = () => {
    this.props.navigation.navigate('Notifications')
    }

 
  goToPostproject = () => {
    var projectData = {
      "startdate": "Start Date",
      "enddate":"End Date",
      "selectedCity" :"Select City",

    }

    var userInputData =  {
      selectedCategory : "Select Category",
      inputData : projectData
    }
     this.props.navigation.navigate('PostProject',  { category: userInputData }) 
  };

  openDetails = (val) => {
   var jobData = {
     token : this.state.userResponse.api_token,
     details : val
   }
   this.props.navigation.navigate('JobDetails',  { details: jobData }) 
  }

  notification = (val) => {
    service.getUserData('notificationCount').then((keyValue) => {
      if(keyValue !== "none" || keyValue !== undefined || keyValue !== "")
      {
      var count = ++keyValue;
      service.saveUserData("notificationCount", count); 
      }
      }, (error) => {
      console.log(error) //Display error
      });
    //  this.props.navigation.navigate('Notifications')
  }

  render() {
    return (
      <SafeAreaView source={constants.loginbg} style={{flex:1}}>
      <OfflineNotice/>
        <View style={styles.topView}>
        <MyView style={styles.tabsToolbar}>
        <TouchableOpacity onPress={() => this.openDrawer()}>
        <Image source={constants.menuicon} style={styles.hamburgerIcon} />
        </TouchableOpacity>
         <Text style={styles.toolbarTitle}> {strings.Jobs} </Text>
         <TouchableOpacity onPress={() => this.goToNotification()}>
         <Image source={constants.notificationIcongrey} style={styles.searchIcon} />
         { this.state.notCount == 0 || this.state.notCount == "" ?  null : <TouchableOpacity
         style={{
          borderWidth:1,
          borderColor:'rgba(0,0,0,0.2)',
          alignItems:'center',
          justifyContent:'center',
          width:18,
          height:18,
          position:'absolute',
          top:2,
          left:14,
          backgroundColor:'#fb913b',
          borderRadius:9,
        }}
 >
   <Text style={{color:"#fff"}}>{this.state.notCount == null ? "0" : this.state.notCount}</Text>
 </TouchableOpacity>}
        </TouchableOpacity>
         <TouchableOpacity onPress={() => this.goToPostproject()}>
         <Image source={constants.addIcon} style={styles.searchIcon} />
        </TouchableOpacity>
        </MyView>
       </View>
       <View style={styles.searchPadding}>
       <MyView  style={styles.searchContainer}>
          <View style={styles.topSearchbar}>
              <Image source={constants.searchicon} style={styles.newsearchIcon} />
              <View style={styles.empty}>
              </View>
            <TextInput placeholder={strings.Searchstring}  placeholderTextColor="#a2a2a2" style={styles.searchfieldInput}  onChangeText={text => this.searchFilterFunction(text)}/>
          </View>
      </MyView>
      </View>
      
       <Text style = {styles.defaultTextSize}>{this.state.noProject}</Text>
        <View style={styles.listCenterJobs}>
        <FlatList
              data={this.state.jobs}
              keyExtractor={(item, index) => index}
              style={styles.listCardWidth}
              extraData={this.state.jobs}
              onRefresh={() => this.onRefresh()}
               refreshing={this.state.refreshing}
              renderItem={({ item, index }) => (
                <View  style={styles.spaceFromTop}>
                    <TouchableOpacity  style= {[ item.accepted == "Accepted" || item.accepted == "Hired" ? styles.listCardJobsActive : styles.listCardJobs]}  onPress={() => this.openDetails(item)}>
                     <View style={styles.rowAlignSideMenuRequest}>
                          <View style={styles.firstText}> 
                          <Text style={styles.textWrap}> {item.title} 
                          </Text>
                          </View>
                          <View style={styles.emptyText}> 
                          </View>
                          <View style={styles.secondText}> 
                          <Text  style= {[ item.accepted == "Accepted" || item.accepted == "Hired" ? styles.textWrap2Active : styles.textWrap2]}> {item.start_date}
                          </Text>
                          </View>
                      </View>
                      <View style={styles.rowAlignSideMenuRequest}>
                          <View style={styles.firstText2}> 
                              <View style={styles.textInRow}> 
                                <View >
                                    <Text  style= {[ item.accepted == "Accepted" || item.accepted == "Hired" ? styles.priceTextActive : styles.priceText]}>{strings.FixedPrice}</Text>
                                  </View>
                                  <View style={styles.contPadding}>
                                    <Text >-</Text>
                                  </View>
                                  <View >
                                    <Text style= {[ item.accepted == "Accepted" || item.accepted == "Hired" ? styles.dateActive : styles.date]}> {item.budget} SAR </Text>
                                  </View>
                                </View>
                              </View>
                          <View style={styles.emptyText2}> 
                          </View>
                          <View style={styles.secondText2}> 
                          <TouchableOpacity onPress={() => this.goToPostproject()}>
                          <Image source={constants.nextIcon} style={styles.searchIcon} />
                           </TouchableOpacity>
                          </View>
                      </View>
                        </TouchableOpacity>
                </View>
              )}
            />
        </View>
       
     
         
        <Loader
              loading={this.state.loading} />
      </SafeAreaView>
    );
  }
}
