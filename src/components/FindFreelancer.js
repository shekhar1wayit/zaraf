import React, {Component} from 'react';
import {Platform, StyleSheet, TextInput,Keyboard, FlatList, ScrollView,Alert, SafeAreaView, Text, View, Image, ImageBackground, Button, TouchableOpacity} from 'react-native';
import Constants from '../constants/Constants';
import Service from '../services/Service';
import Loader from './Loader';
import MyView from './MyView';
import { strings } from "../services/stringsoflanguages";
import OfflineNotice from './OfflineNotice';
import firebase  from './Config';
import FCM, {FCMEvent} from "react-native-fcm";
import Stars from 'react-native-stars';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
export default class FindFreelancer extends Component {
 constructor(props){
     super(props);
     service = new Service();
     constants = new Constants();
     this.state = {
        userData: { picture_large:{ data:{}}},
        userResponse: {}, 
        freelancers : {freelancer: []},
        loading:false,
        search : true,
        dummyText : "",
        noText : true,
        notCount : 0,
        refreshing: false
      };
      this.arrayholder = []
   
 }

 
 componentDidMount ()   {
  if(this.props.navigation.state.params)
  {
    this.setState ({ loading: true});
   console.log(this.props.navigation.state.params)
   }  
  this.setState ({ loading: true});
  setTimeout(() => {
    this.setState ({ loading: false});
    this.setCount();
    firebase.notifications().onNotification((res) => {
      this.notification(res)
    });
    FCM.on(FCMEvent.Notification, (notif) => {
      this.notification(notif); 
    });
    service.getUserData('user').then((keyValue) => {
      console.log("local", keyValue);
      var parsedData = JSON.parse(keyValue);
      console.log("json", parsedData);
      this.setState({ userResponse: parsedData});
       this.CheckInternetConnection();
   }, (error) => {
      console.log(error) //Display error
    });

    }, 2000)
   
   }


   setCount = () =>
{
 service.getUserData('notificationCount').then((keyValue) => {
   if(keyValue !== "none" || keyValue !== undefined || keyValue !== "")
   {
   this.setState({notCount : keyValue})
   }
   }, (error) => {
   console.log(error) //Display error
   });
}


notification = (val) => {
 console.log('this', val)
 service.getUserData('notificationCount').then((keyValue) => {
   if(keyValue !== "none" || keyValue !== undefined || keyValue !== "")
   {
   var count = ++keyValue;
   service.saveUserData("notificationCount", count); 
   console.log(count)
   }
   this.setCount();
   }, (error) => {
   console.log(error) //Display error
   });
  
}
   searchFilterFunction = text => {
    const newData = this.arrayholder.filter(item => {
      const itemData = `${item.email.toUpperCase()}  ${item.username.toUpperCase()} `;
      const textData = text.toUpperCase();
      return itemData.indexOf(textData) > -1;
    });
    this.setState({
      freelancers: newData
    });
  }; 


 openDrawer = () => {
   Keyboard.dismiss();
   this.props.navigation.openDrawer()
  }


  CheckInternetConnection=()=>{
    service.handleConnectivityChange().then((res) => {
    if(res.type == "none")
    {
      Alert.alert('Alert!', 'Check your internet connection');
    }
    else
    {
      this.getFreelancersResponse();
    }
    })

 
 }



  getFreelancersResponse = () => {
    if( this.props.navigation.state.params.client_Details.jobId !== '')
    {
    service.findFreelancer(this.state.userResponse.api_token, this.props.navigation.state.params.client_Details.category).then((res) => {
      console.log("checkres", res);
      newres = JSON.stringify(res);
      json = JSON.parse(newres);
      if(res.freelancer.length ===  0)
      {
        this.setState ({ dummyText: strings.NoFreelancerFound});
        this.setState ({  noText: false});
      }
      
      else
      {
        this.arrayholder =  res.freelancer;
      this.setState({ freelancers: res.freelancer});
      }
    })
   }
   else
   {
    service.findFreelancer(this.state.userResponse.api_token).then((res) => {
      console.log("checkres", res);
      newres = JSON.stringify(res);
      json = JSON.parse(newres);
      if(res.freelancer.length ===  0)
      {
        this.setState ({ dummyText: strings.NoFreelancerFound});
        this.setState ({  noText: false});
      }
      
      else
      {
       this.arrayholder =  res.freelancer;
      this.setState({ freelancers: res.freelancer});
      }
    })
   }
   this.setState(
    {
      refreshing: false
    })
   }

   openFreelancerDetails = (val) => {
   if(this.props.navigation.state.params)
   {
    var clientDetails = 
    {
      clt_Details : this.props.navigation.state.params,
      freelancerDetails : val
    }
  }
  else 
  {
      var clientDetails = 
    {
      freelancerDetails : val
    }
  }
   this.props.navigation.navigate('FreelancerDetails', { freelancerdetails: clientDetails }) 
 }

 goToNotification = () => {
  this.props.navigation.navigate('Notifications')
  }

  onRefresh = () => {
    this.setState(
      {
        refreshing: true
      })
      this.getFreelancersResponse();
  }

  goback = () => {
    
    this.props.navigation.navigate("JobDetails", { details: this.props.navigation.state.params.client_Details.jobDetails })
  }

 searchPage = () =>{
  this.setState({ search: false});
      }

      goToNotification = () => {
        this.props.navigation.navigate('Notifications')
        }

  
  render() {
    const defaultImg =
    'https://satishrao.in/wp-content/uploads/2016/06/dummy-profile-pic-male.jpg'

   // console.log(this.state.freelancers.freelancer)
    
    return (
        
     <SafeAreaView
      source={constants.loginbg}
      style={{flex:1}}>
        <OfflineNotice/> 
       <View style={styles.topView}>
        <MyView style={styles.tabsToolbar}>
        {
          this.props.navigation.state.params.client_Details.icon == "back" ?  <TouchableOpacity onPress={() => this.goback()}>
          <Image source={constants.backicon} style={styles.hamburgerIcon} />
          </TouchableOpacity> :  <TouchableOpacity onPress={() => this.openDrawer()}>
        <Image source={constants.menuicon} style={styles.hamburgerIcon} />
        </TouchableOpacity>
        }
       
         <Text style={styles.toolbarTitle}>{strings.FindFreelancer}</Text>
         <TouchableOpacity>
        </TouchableOpacity>
         <TouchableOpacity onPress={() => this.goToNotification()}>
         <Image source={constants.notificationIcongrey} style={styles.searchIcon} />
        { this.state.notCount == 0 || this.state.notCount == "" || this.state.notCount == null ?  null : <TouchableOpacity
         style={{
          borderWidth:1,
          borderColor:'rgba(0,0,0,0.2)',
          alignItems:'center',
          justifyContent:'center',
          width:18,
          height:18,
          position:'absolute',
          top:2,
          left:14,
          backgroundColor:'#fb913b',
          borderRadius:9,
        }}
 >
   <Text style={{color:"#fff"}}>{this.state.notCount}</Text>
   </TouchableOpacity>}
        </TouchableOpacity>
        </MyView>
       </View>
       <View style={styles.searchPadding}>
       <MyView  style={styles.searchContainer}>
          <View style={styles.topSearchbar}>
              <Image source={constants.searchicon} style={styles.newsearchIcon} />
              <View style={styles.empty}>
              </View>
            <TextInput placeholder={strings.Searchstring}  placeholderTextColor="#a2a2a2" style={styles.searchfieldInput} onChangeText={text => this.searchFilterFunction(text)}/>
          </View>
      </MyView>
      </View>
      <MyView style={styles.defaultTextFreelancer} hide={this.state.noText}>
      <Text style = {styles.defaultTextSize}>{this.state.dummyText}</Text>
      </MyView>
     <View style={styles.listCenter}>
     <FlatList
        data={this.state.freelancers}
        style={styles.freelancerlistCardWidth}
        onRefresh={() => this.onRefresh()}
        refreshing={this.state.refreshing}
        renderItem={({ item }) => (
           <View  style={styles.spaceFromTop}>
              <TouchableOpacity style={styles.listCardFreelancer} onPress={() => this.openFreelancerDetails(item)}>
              <View style={styles.textInRowlist}> 
                 <View style = {styles.imageFreelancerContainer}>
                 <Image source={{ uri: item.image_path || defaultImg  }}    style={styles.freelancerprofilePic} />
                  </View>
                  <View style={styles.textFreelancerContainer}>
                  <Text style={styles.email}>{item.email} <Text style={styles.freelancerProfileText}>{item.username}</Text> </Text>
                 
                  </View>
                  <View style={styles.emptyFreelancerContainer2}>
                  </View>
                  <View style={styles.iconFreelancerContainer2}>
                  <TouchableOpacity >
                  <Image source={constants.nextIcon} style={styles.nextIcon} />
                   </TouchableOpacity>
                  </View>
              </View>
              <View style={styles.textInRowlist2}> 
                 <View style={{width:'22%'}}>
              
                  </View>
                  <View style={{width:'33%'}}>
                  <Stars
    default={item.ratings}
    count={5}
    disabled = {true}
    half={true}
    starSize={50} 
    fullStar={<Icon name={'star'} style={[styles.myStarStyle]}/>}
    emptyStar={<Icon name={'star-outline'} style={[styles.myStarStyle, styles.myEmptyStarStyle]}/>}
    halfStar={<Icon name={'star-half'} style={[styles.myStarStyle]}/>}
  />
                  </View>
                  <View style={{width:'30%'}}>
                 
                  </View>
                  
              </View>
             
              </TouchableOpacity>
          </View>
         )}
         />
     </View>
     <Loader
              loading={this.state.loading} />

 </SafeAreaView>
      
     
    );
  }
}
