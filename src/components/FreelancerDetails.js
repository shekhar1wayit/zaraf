import React, {Component} from 'react';
import {Platform, StyleSheet, Text, View,Dimensions,  SafeAreaView,Alert, Linking,  ScrollView, Image, ImageBackground, ActivityIndicator, TouchableOpacity, TouchableNativeFeedback} from 'react-native';
import styles from '../styles/styles';
import Constants from '../constants/Constants';
import Loader from './Loader';
import HTMLView from 'react-native-htmlview';
import CustomToast from './CustomToast';
import Service from '../services/Service';
import MyView from './MyView';
import { strings } from '../services/stringsoflanguages';
import OfflineNotice from './OfflineNotice';
const dimensions = {
  fullHeight: Dimensions.get('window').height,
  fullWidth: Dimensions.get('window').width
}
export default class FreelancerDetails extends Component {
  constructor(props){
    super(props);
    constants = new Constants();
    service = new Service();
    this.state = { 
        freelancerDetails : "",
        loading:false,
        search : true,
        isJobId : true
      }
  }

 componentDidMount() {
    this.setState ({ loading: true});
    setTimeout(() => {
      this.setState ({ loading: false});
      if(this.props.navigation.state.params)
  {
    console.log(this.props.navigation.state.params.freelancerdetails);
   if (this.props.navigation.state.params.freelancerdetails.clt_Details !== undefined)
   {
    this.setState({ isJobId : false })
   }
   this.setState({ freelancerDetails: this.props.navigation.state.params.freelancerdetails.freelancerDetails})
  }    
      }, 500)
   
  }

  Chat = () => {
    this.props.navigation.navigate('Chat', { chatDetails: this.props.navigation.state.params.freelancerdetails.freelancerDetails})
  }

  CheckInternetConnection=()=>{
    service.handleConnectivityChange().then((res) => {
    if(res.type == "none")
    {
      Alert.alert('Alert!', 'Check your internet connection');
    }
    else
    {
      this.submitProposal();
    }
    })

 
 }


 openlink = (doc) => {
  if(doc)
  {
  Linking.openURL(doc)
  }
}

openlink2 = (doc) => {
  if(doc)
  {
  Linking.openURL(doc)
  }
}
  submitProposal = () => {
    this.setState ({ loading: true});
  setTimeout(() => {
    service.sendProposal(this.props.navigation.state.params.freelancerdetails.clt_Details.client_Details.apiToken, this.state.freelancerDetails.id,  this.props.navigation.state.params.freelancerdetails.clt_Details.client_Details.jobId).then((res) => {
      console.log("checkres", res);
      if(res.status == "success")
      {
        this.refs.defaultToastBottom.ShowToastFunction(strings.RequestSendSuccessfully);
        this.goToHome();
      }
      else 
      {
        if(res.error == "Job already has been sent")
        {
        this.refs.defaultToastBottom.ShowToastFunction(strings.AlreadySentRequest); 
        }
        else
        {
          this.refs.defaultToastBottom.ShowToastFunction(res.error); 
        }
      }
    })
    this.setState ({ loading: false});
    }, 2000)
  }

  goToHome()
{
setTimeout(() => {
this.props.navigation.navigate('Jobs')
}, 500)
}

selectJob = () => {
  this.props.navigation.navigate('Jobs')
}
  
  goBack = () =>{
    this.props.navigation.navigate('FindFreelancer') 
   }
  
  render() {
    return (
  <SafeAreaView style = { styles.MainContainer }>
    <OfflineNotice/> 
	     <View style={styles.commontoolbar}>
        <TouchableOpacity onPress={() => this.goBack()}>
        <Image source={constants.backicon} style={styles.backIcon} />
        </TouchableOpacity>
          <Text style={styles.toolbarTitle}>{strings.Freelancerdetail}</Text>
          <Text style={styles.commontoolbarButton}></Text>
         </View>
         <View style={{height: dimensions.fullHeight}}>
         <ScrollView>
         <View style={{marginTop:20}}>
       <View style={styles.rowAlignSideMenuJob}>
       
                  
                  <View style={{width:"5%"}}> 
                  </View>
                  <View style={{width:"35%"}}> 
                  <Text style={styles.textWrapDetails}> {strings.Email}
                  </Text>
                  </View>
                  <View  style={{width:"5%"}}>
                  </View>
                  <View style={{width:"45%"}}> 
                  <Text style={styles.textWrap2Details}> {this.state.freelancerDetails.email}
                  </Text>
                  </View>
         </View>
         <View style={styles.rowAlignSideMenuJob2}>
                  <View style={{width:"5%"}}> 
                  </View>
                  <View style={{width:"35%"}}> 
                  <Text style={styles.textWrapDetails}> {strings.Name}
                  </Text>
                  </View>
                  <View  style={{width:"5%"}}>
                  </View>
                  <View style={{width:"45%"}}> 
                  <Text style={styles.textWrap2Details}> {this.state.freelancerDetails.username}
                  </Text>
                  </View>
         </View>
         <View style={styles.rowAlignSideMenuJob}>
                 <View style={{width:"5%"}}> 
                  </View>
                  <View style={{width:"35%"}}> 
                  <Text style={styles.textWrapDetails}> {strings.Shortbio}
                  </Text>
                  </View>
                  <View  style={{width:"5%"}}>
                  </View>
                  <View style={{width:"45%"}}> 
                  <Text style={styles.textWrap2Details}> {this.state.freelancerDetails.short_bio}
                  </Text>
                  </View>
         </View>
         <View style={styles.rowAlignSideMenuJob2}>
         <View style={{width:"5%"}}> 
                  </View>
               <View style={{width:"35%"}}>  
                  <Text style={styles.textWrapDetails}> {strings.Category}
                  </Text>
                  </View>
                  <View  style={{width:"5%"}}>
                  </View>
                  <View style={{width:"45%"}}> 
                  <Text style={styles.textWrap2Details}> {this.state.freelancerDetails.categoryId}
                  </Text>
                  </View>
         </View>
         <View style={styles.rowAlignSideMenuJob}>
         <View style={{width:"5%"}}> 
                  </View>
                  <View style={{width:"35%"}}>  
                  <Text style={styles.textWrapDetails}> {strings.Skills}
                  </Text>
                  </View>
                  <View  style={{width:"5%"}}>
                  </View>
                  <View style={{width:"45%"}}> 
                  <Text style={styles.textWrap2Details}> {this.state.freelancerDetails.skills}
                  </Text>
                  </View>
         </View>
         <View style={styles.rowAlignSideMenuJob2}>
         <View style={{width:"5%"}}> 
                  </View>
                  <View style={{width:"35%"}}>  
                  <Text style={styles.textWrapDetails}> {strings.CV}
                  </Text>
                  </View>
                  <View  style={{width:"5%"}}>
                  </View>
                  <View style={{width:"45%"}}> 
                  <Text style={styles.docDetails}  onPress={() => this.openlink(this.state.freelancerDetails.CV)}> {strings.clicktoview}
                  </Text>
                  </View>
         </View>
         <View style={styles.rowAlignSideMenuJob}>
         <View style={{width:"5%"}}> 
                  </View>
                  <View style={{width:"35%"}}>  
                  <Text style={styles.textWrapDetails}> {strings.IDProof}
                  </Text>
                  </View>
                  <View  style={{width:"5%"}}>
                  </View>
                  <View style={{width:"45%"}}> 
                  <Text style={styles.docDetails} onPress={() =>  this.openlink2(this.state.freelancerDetails.identityId)}> {strings.clicktoview}
                  </Text>
                  </View>
         </View>
       
              </View>
     </ScrollView>
     </View>
  
      
     { this.props.navigation.state.params.freelancerdetails.clt_Details.client_Details.jobId !== "" ?
     
      
      <View style={{flex:1, width:'100%', position:'absolute', bottom:20, alignItems: 'center'}}>
       <View  style={ styles.bottomViewSubmitnew3}>
       <TouchableOpacity onPress={() => this.Chat()}>
      <Text style={styles.textStyle}>{strings.ChatwithFreelancer}</Text>
      </TouchableOpacity>
      </View>
      <MyView  style={ styles.bottomViewSubmitnew}>
      <TouchableOpacity onPress={() => this.CheckInternetConnection()}>
      <Text style={styles.textStyle}>{strings.SubmitProposal}</Text>
      </TouchableOpacity>
      </MyView>
      </View> :
       <MyView  style={ styles.bottomViewSubmitnew}>
       <TouchableOpacity onPress={() => this.selectJob()}>
       <Text style={styles.textStyle}>{strings.SelectJob}</Text>
       </TouchableOpacity>
       </MyView>
      

     }
     
    <Loader
              loading={this.state.loading} /> 
      <CustomToast ref = "defaultToastBottom"/> 
   </SafeAreaView>
	   
    );
  }
}

