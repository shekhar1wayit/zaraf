import React, {Component} from 'react';
import {Platform, StyleSheet, Text, Button, Alert, View, ScrollView, KeyboardAvoidingView, SafeAreaView,Image, TextInput, ImageBackground, ActivityIndicator, TouchableOpacity, TouchableNativeFeedback} from 'react-native';
import styles from '../styles/styles';
import Constants from '../constants/Constants';
import Service from '../services/Service';
import CustomToast from './CustomToast';
import Loader from './Loader';
import ImagePicker from "react-native-image-picker";
import MyView from './MyView';
import OfflineNotice from './OfflineNotice';
import { DocumentPicker, DocumentPickerUtil } from 'react-native-document-picker';
import { strings } from '../services/stringsoflanguages';
export default class UpdateProfile extends Component {
  
  constructor(props){
    super(props);
    service = new Service();
    constants = new Constants();
    this.state = { 
       userResponse: {},
        email:'',
        name:'',
        about: '',
        loading: false,
        userType : "",
        category :'Category',
        pickedImage: null,
        docImage: null,
        resumeImage: null,
        imagePath : '',
        imageExists : false,
        isFreelancer : false,
        document : 'CV',
        proof : 'ID proof',
        file : "",
        fileID : "",
        ifCV : false,
        skills: '',
        image_path:'https://satishrao.in/wp-content/uploads/2016/06/dummy-profile-pic-male.jpg'
      }
  }
 
  

  
 
  componentDidMount ()   {

    
    if(this.props.navigation.state.params)
    {  

      service.handleConnectivityChange().then((res) => {
        if(res.type == "none")
        {
          Alert.alert('Alert!', 'Check your internet connection');
        }
        else
        {
          service.getUserProfileData(this.props.navigation.state.params.chatDetails.api_token).then((res) => {
            console.log("local", res);
            this.setState({ userResponse: res.user});
            this.setClientData(res.user);
         }, (error) => {
            console.log(error) //Display error
          });
  
        }
     })
       
     
    }
    
   }

   setClientData = (ClientProfileData) =>
   {
    if(ClientProfileData.usertype == "1")
    {
    this.setState({ userType: "Client"});
    }
    else
    {
    this.setState({ userType: "Freelancer"}); 
    }
     if (ClientProfileData.username !== null)
     {
     this.setState ({ name: ClientProfileData.username});
     }
     if(ClientProfileData.email !== null)
     {
     this.setState ({ email: ClientProfileData.email});
     }
     if(ClientProfileData.short_bio !== "null")
     {
     this.setState ({ about: ClientProfileData.short_bio});
     }
   }
   
 

 
 

 
 goBack = () => {
 
  this.props.navigation.navigate('JobRequestDetails')

}

 


  render() {
      defaultImg = 'https://satishrao.in/wp-content/uploads/2016/06/dummy-profile-pic-male.jpg';
      const  ImagePicked =   <TouchableOpacity ><Image source={this.state.pickedImage} style={styles.profilePic}/></TouchableOpacity>
      const  NewImage =   <TouchableOpacity ><Image source={{uri: this.state.userResponse.image_path || defaultImg  }} style={styles.profilePic}/></TouchableOpacity>
    return (
  <SafeAreaView style={styles.MainContainerProfile}>
   <OfflineNotice/> 
	   
        <View style={styles.commontoolbar}>
            <TouchableOpacity  onPress={() => this.goBack()}>
			     <Image source={constants.backicon} style={styles.hamburgerIcon}/>
			      </TouchableOpacity>
          <Text style={styles.toolbarTitle}>{strings.Profile}</Text>
          <Text style={styles.commontoolbarButton}></Text>
        </View>
     
     <ScrollView>
     <KeyboardAvoidingView
      style={styles.container}
      behavior="padding"  
    >
     <View style={{alignItems:'center'}}>
      <MyView style={styles.profileContainer} hide={this.state.imageExists}>
      { NewImage}
      </MyView>
      <MyView style={styles.profileContainer} hide={!this.state.imageExists}>
      { ImagePicked}
      </MyView>
      </View>
      <TouchableOpacity style={styles.camera} >
         <Image  style={styles.cameraIcon} />
        </TouchableOpacity>
      <View style={{padding:10}}>
      <Text style={styles.themetextColor}>
           {strings.Username}
      </Text>
      <TextInput
            style={styles.postprojectinputprofile}
            underlineColorAndroid="transparent"
            placeholder={strings.Name}
            onChangeText={(text)=>this.setState({ name:text})}
            placeholderTextColor="#AEA9A8"
            autoCapitalize="none"
            returnKeyType='done'
            editable={false}
            value={this.state.name}
          />
          </View>
          <View style={{padding:10}}>
      <Text style={styles.themetextColor}>
           {strings.Emailaddress}
      </Text>
      <TextInput
            style={styles.postprojectinputprofile}
            underlineColorAndroid="transparent"
            placeholder={strings.Email}
            onChangeText={(text)=>this.setState({ email:text})}
            placeholderTextColor="#AEA9A8"
            autoCapitalize="none"
            returnKeyType='done'
            editable={false}
            value={this.state.email}
          />
          </View>
          <View style={{padding:10}}>
      <Text style={styles.themetextColor}>
         {strings.AboutMe}
      </Text>
      <TextInput
            style={styles.about}
            underlineColorAndroid="transparent"
            placeholder={strings.AboutMe}
            onChangeText={(text)=>this.setState({ about:text})}
            placeholderTextColor="#AEA9A8"
            autoCapitalize="none"
            returnKeyType='done'
            value={this.state.about}
            multiline={true}
            blurOnSubmit={true}
            editable={false}
            numberOfLines={4}
          />
          </View>
          <View style={{padding:10}}>
      <Text style={styles.themetextColor}>
           {strings.Usertype}
      </Text>
      <TextInput
            style={styles.postprojectinputprofile}
            underlineColorAndroid="transparent"
            placeholder="User Type"
            placeholderTextColor="#AEA9A8"
            autoCapitalize="none"
            returnKeyType='done'
            editable={false}
            value={this.state.userType} editable={false}
          />
          </View>    
      </KeyboardAvoidingView>
      </ScrollView>
      <View style={styles.toastCenter}>
	    <CustomToast ref = "defaultToastBottom"/>
      </View>
      <Loader
          loading={this.state.loading} />
       </SafeAreaView>
	   
    );
  }
}

