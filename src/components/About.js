// About Us Page
import React, { Component } from "react";
import {
  Platform,
  StyleSheet,
  SafeAreaView,
  ScrollView,
  Text,
  View,
  Image,
  ImageBackground,
  Button,
  TouchableOpacity
} from "react-native";
import firebase  from './Config';
import FCM, {FCMEvent} from "react-native-fcm";
import Constants from "../constants/Constants";
import Service from "../services/Service";
import { strings } from "../services/stringsoflanguages";
import OfflineNotice from './OfflineNotice';

export default class About extends Component {
  constructor(props) {
    super(props);
    service = new Service();
    constants = new Constants();
    this.state = {
      userData: { picture_large: { data: {} } },
      stt : 1,
      notCount : 0
    };
  }

  componentDidMount() {
    this.checkStatus();
    this.setCount();
    firebase.notifications().onNotification((res) => {
      this.notification(res)
    });
    FCM.on(FCMEvent.Notification, (notif) => {
      this.notification(notif); 
    });
  }

  setCount = () =>
   {
    service.getUserData('notificationCount').then((keyValue) => {
      if(keyValue !== "none" || keyValue !== undefined || keyValue !== "")
      {
      this.setState({notCount : keyValue})
      }
      }, (error) => {
      console.log(error) //Display error
      });
  }


  notification = (val) => {
    console.log('this', val)
    service.getUserData('notificationCount').then((keyValue) => {
      if(keyValue !== "none" || keyValue !== undefined || keyValue !== "")
      {
      var count = ++keyValue;
      service.saveUserData("notificationCount", count); 
      console.log(count)
      }
      this.setCount();
      }, (error) => {
      console.log(error) //Display error
      });
     
  }

  openDrawer = () => {
    this.props.navigation.openDrawer();
  };

  goToProfile = () => {
    this.props.navigation.navigate("Profile");
  };

  goToNotification = () => {
    this.props.navigation.navigate('Notifications')
    }

  checkStatus=()=>{

    service.getUserData('user').then((keyValue) => {
      console.log("local", keyValue);
      var parsedData = JSON.parse(keyValue);
      console.log("json", parsedData.api_token);

      service.getUserProfileData(parsedData.api_token).then((res) => {
        console.log("local", res);
        //this.setState({ userResponse: res.user});
       if(res.user.isLogin == 1)
       {
        this.setState({stt:1})
       }else{

        this.setState({stt:0})

       }
     }, (error) => {
        console.log(error) //Display error
      });
    //  this.setState({ userResponse: parsedData});
    
      
   }, (error) => {
      console.log(error) //Display error
    });
    
    
    
    }

  render() {
    return (
      <SafeAreaView source={constants.loginbg} style={styles.MainContainer}>
       <OfflineNotice/>
        <View style={styles.toolbar}>
          <TouchableOpacity onPress={() => this.openDrawer()}>
            <Image source={constants.menuicon} style={styles.hamburgerIcon} />
          </TouchableOpacity>
          <Text style={styles.toolbarTitle}>{strings.About}</Text>
          <TouchableOpacity onPress={() => this.goToNotification()}>
        <Image source={constants.notificationIcongrey} style={styles.searchIcon} />
        { this.state.notCount == 0 || this.state.notCount == "" || this.state.notCount == null ?  null : <TouchableOpacity
         style={{
          borderWidth:1,
          borderColor:'rgba(0,0,0,0.2)',
          alignItems:'center',
          justifyContent:'center',
          width:18,
          height:18,
          position:'absolute',
          top:2,
          left:14,
          backgroundColor:'#fb913b',
          borderRadius:9,
        }}
 >
   <Text style={{color:"#fff"}}>{this.state.notCount}</Text>
 </TouchableOpacity>}
        </TouchableOpacity>
        </View>
        <ScrollView>
          <View style={{ width: "90%", paddingTop: 30 }}>
            <Text style={{ fontSize: 20 }}>
             {strings.AboutTest1}
            </Text>
          </View>
          <View style={{ width: "90%", paddingTop: 20 }}>
            <Text style={{ fontSize: 20 }}>
            {strings.AboutTest2}
            </Text>
          </View>
          <View style={{ width: "90%", paddingTop: 20 }}>
            <Text style={{ fontSize: 20 }}>
            {strings.AboutTest3}
            </Text>
          </View>
        </ScrollView>

        {this.state.stt == 0 ? <TouchableOpacity
          style={styles.bottomViewAbout}
          onPress={() => this.goToProfile()}
        >
          <Text style={styles.textStyle}>{strings.Gotoprofile}</Text>
        </TouchableOpacity> : null}
      </SafeAreaView>
    );
  }
}
