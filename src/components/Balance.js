import React, {Component} from 'react';
import {Platform, StyleSheet, SafeAreaView,ScrollView, Text, View, Image, ImageBackground, Button, TouchableOpacity} from 'react-native';
import Constants from '../constants/Constants';
import Service from '../services/Service';
import firebase  from './Config';
import FCM, {FCMEvent} from "react-native-fcm";
import { strings } from '../services/stringsoflanguages';
import OfflineNotice from './OfflineNotice';
export default class Balance extends Component {
 constructor(props){
     super(props);
     service = new Service();
     constants = new Constants();
     this.state = {
        userData: { picture_large:{ data:{}}},
        notCount : 0,
        dummyTextFav: strings.NorecordFound
      };
   
 }
 
 componentDidMount() {
    this.setCount();
    firebase.notifications().onNotification((res) => {
      this.notification(res)
    });
    FCM.on(FCMEvent.Notification, (notif) => {
      this.notification(notif); 
    });
 }
 openDrawer = () => {
   this.props.navigation.openDrawer()}

   setCount = () =>
   {
    service.getUserData('notificationCount').then((keyValue) => {
      if(keyValue !== "none" || keyValue !== undefined || keyValue !== "")
      {
      this.setState({notCount : keyValue})
      }
      }, (error) => {
      console.log(error) //Display error
      });
  }


  notification = (val) => {
    console.log('this', val)
    service.getUserData('notificationCount').then((keyValue) => {
      if(keyValue !== "none" || keyValue !== undefined || keyValue !== "")
      {
      var count = ++keyValue;
      service.saveUserData("notificationCount", count); 
      console.log(count)
      }
      this.setCount();
      }, (error) => {
      console.log(error) //Display error
      });
     
  }

goToNotification = () => {
  this.props.navigation.navigate('Notifications')
  }

  render() {
   
    return (
        
     <SafeAreaView
      source={constants.loginbg}
      style={styles.container}>
      <OfflineNotice/>
    <View style={styles.toolbar} >
        <TouchableOpacity onPress={() => this.openDrawer()}>
        <Image source={constants.menuicon} style={styles.hamburgerIcon} />
        </TouchableOpacity>
         <Text style={styles.toolbarTitle}>{strings.MyBalance}</Text>
        <TouchableOpacity onPress={() => this.goToNotification()}>
        <Image source={constants.notificationIcongrey} style={styles.searchIcon} />
        { this.state.notCount == 0 || this.state.notCount == "" || this.state.notCount == null ?  null : <TouchableOpacity
         style={{
          borderWidth:1,
          borderColor:'rgba(0,0,0,0.2)',
          alignItems:'center',
          justifyContent:'center',
          width:18,
          height:18,
          position:'absolute',
          top:2,
          left:14,
          backgroundColor:'#fb913b',
          borderRadius:9,
        }}
 >
   <Text style={{color:"#fff"}}>{this.state.notCount}</Text>
   </TouchableOpacity>}
        </TouchableOpacity>
     </View>
     <ScrollView>
     <View style={styles.homeContent}>
     <Text style={styles.defaultTextSize}>{this.state.dummyTextFav}</Text>
     </View>
     </ScrollView>
 </SafeAreaView>
      
     
    );
  }
}
