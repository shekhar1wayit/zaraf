// Account Page
import React, {Component} from 'react';
import {Platform, StyleSheet, SafeAreaView, ScrollView, Text, View, Image, ImageBackground, Button, TouchableOpacity} from 'react-native';
import Constants from '../constants/Constants';
import Service from '../services/Service';
import OfflineNotice from './OfflineNotice';
import { strings } from '../services/stringsoflanguages';

export default class PaymentCard extends Component {
 constructor(props){
     super(props);
     service = new Service();
     constants = new Constants();
     this.state = {
        userData: { picture_large:{ data:{}}},
      };
   
 }

 goToHome = () => {
  this.props.navigation.navigate('Jobs')
 }


 componentDidMount() {

 }

 _onChange = (form) =>{
  console.log(form);
 }


 
  render() {
    return (
     <SafeAreaView
      source={constants.loginbg}
      style={{flex:1}}>
      <OfflineNotice/>
    <View style={styles.toolbar} >
        <TouchableOpacity onPress={() => this.goToHome()}>
        <Image source={constants.backicon} style={styles.hamburgerIcon} />
        </TouchableOpacity>
         <Text style={styles.toolbarTitle}>{strings.MakePayment}</Text>
         <TouchableOpacity >
        <Image  style={styles.searchIcon} />
        </TouchableOpacity>
     </View>
    
     <View style={{marginTop:20}}>
   
     </View>
  
 </SafeAreaView>
      
     
    );
  }
}
