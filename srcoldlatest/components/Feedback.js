import React, { Component } from "react";
import {
  Platform,
  StyleSheet,
  Alert,
  SafeAreaView,
  TextInput,
  Text,
  View,
  Image,
  ImageBackground,
  Button,
  TouchableOpacity,
  Keyboard
} from "react-native";
import Constants from "../constants/Constants";
import Service from "../services/Service";
import OfflineNotice from './OfflineNotice';
import CustomToast from "./CustomToast";
import Loader from "./Loader";
import firebase  from './Config';
import FCM, {FCMEvent} from "react-native-fcm";
import { strings } from "../services/stringsoflanguages";
export default class Feedback extends Component {
  constructor(props) {
    super(props);
    service = new Service();
    constants = new Constants();
    this.state = {
      userResponse: {},
      feedback: " ",
      loading: false,
      notCount : 0
    };
  }
  componentDidMount() {

    this.setCount();
    firebase.notifications().onNotification((res) => {
      this.notification(res)
    });
    FCM.on(FCMEvent.Notification, (notif) => {
      this.notification(notif); 
    });


    service.getUserData("user").then(
      keyValue => {
        console.log("local", keyValue);
        var parsedData = JSON.parse(keyValue);
        console.log("json", parsedData);
        this.setState({ userResponse: parsedData });
      },
      error => {
        console.log(error); //Display error
      }
    );
  }
  openDrawer = () => {
    Keyboard.dismiss()
    this.props.navigation.openDrawer();
  };

  
  CheckInternetConnection=()=>{
    service.handleConnectivityChange().then((res) => {
    if(res.type == "none")
    {
      Alert.alert('Alert!', 'Check your internet connection');
    }
    else
    {
      this.submit();
    }
    })

 
 }

  submit = () => {
    if (this.state.feedback.trim() === "") {
      this.refs.defaultToastBottom.ShowToastFunction(strings.PleaseEnterFeedback);
    } else {
      this.setState({ loading: true });
      Keyboard.dismiss()
      setTimeout(() => {
        this.setState({ loading: false });

        service
          .feebback(this.state.userResponse.api_token, this.state.feedback)
          .then(res => {
            console.log(this.state.userResponse.api_token);
            console.log(this.state.feedback);

            if (res != undefined) {
              if (res.status_code == 200) {
                if (res.status == "success") {
                  this.refs.defaultToastBottom.ShowToastFunction(
                   strings.SubmitSuccessfully
                  );
                  this.setState({ feedback: " " })
                  //this.navigation.(res);
                } else {
                  this.refs.defaultToastBottom.ShowToastFunction(
                    "Network Error"
                  );
                }
              }
            } else {
              this.refs.defaultToastBottom.ShowToastFunction("Network Error");
            }
          });
      }, 3000);
    }
  };

  getTextOnChange=(textNew)=>{
    this.setState({ feedback: textNew })
  }

  goToNotification = () => {
    this.props.navigation.navigate('Notifications')
    }

    setCount = () =>
   {
    service.getUserData('notificationCount').then((keyValue) => {
      if(keyValue !== "none" || keyValue !== undefined || keyValue !== "")
      {
      this.setState({notCount : keyValue})
      }
      }, (error) => {
      console.log(error) //Display error
      });
  }


  notification = (val) => {
    console.log('this', val)
    service.getUserData('notificationCount').then((keyValue) => {
      if(keyValue !== "none" || keyValue !== undefined || keyValue !== "")
      {
      var count = ++keyValue;
      service.saveUserData("notificationCount", count); 
      console.log(count)
      }
      this.setCount();
      }, (error) => {
      console.log(error) //Display error
      });
     
  }

  render() {
    return (
      <SafeAreaView source={constants.loginbg} style={styles.container}>
        <OfflineNotice/> 
        <View style={styles.toolbar}>
          <TouchableOpacity onPress={() => this.openDrawer()}>
            <Image source={constants.menuicon} style={styles.hamburgerIcon} />
          </TouchableOpacity>
          <Text style={styles.toolbarTitle}>{strings.Feedback}</Text>
          <TouchableOpacity  onPress={() => this.goToNotification()}>
              <Image  source={constants.notificationIcongrey} style={styles.searchIcon} />
              { this.state.notCount == 0 || this.state.notCount == "" || this.state.notCount == null ?  null : <TouchableOpacity
         style={{
          borderWidth:1,
          borderColor:'rgba(0,0,0,0.2)',
          alignItems:'center',
          justifyContent:'center',
          width:18,
          height:18,
          position:'absolute',
          top:2,
          left:14,
          backgroundColor:'#fb913b',
          borderRadius:9,
        }}
 >
   <Text style={{color:"#fff"}}>{this.state.notCount}</Text>
 </TouchableOpacity>}
            </TouchableOpacity>
        </View>

        <TextInput
          style={styles.feedbacknputdiscrpation}
          underlineColorAndroid="transparent"
          placeholder={strings.EnterFeedback}
          onChangeText={text => this.getTextOnChange(text) }
          placeholderTextColor="#AEA9A8"
          autoCapitalize="none"
          blurOnSubmit={true}
          multiline={true}
          numberOfLines={4}
          onSubmitEditing={()=>{Keyboard.dismiss()}}
          returnKeyType='done'
        />

        <TouchableOpacity
          style={styles.bottomViewRequest}
          onPress={() => this.CheckInternetConnection()}
        >
          <Text style={styles.textStyle}>{strings.Submit}</Text>
          <CustomToast ref="defaultToastBottom" />
        </TouchableOpacity>
        <Loader loading={this.state.loading} />
      </SafeAreaView>
    );
  }
}
