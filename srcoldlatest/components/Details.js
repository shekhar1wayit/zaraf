// Screen for accept and refusal of freelancer requests
import React, {Component} from 'react';
import {Platform, StyleSheet, Text,Alert, View, Button, ScrollView, SafeAreaView,Image, ImageBackground, ActivityIndicator, TouchableOpacity, TouchableNativeFeedback} from 'react-native';
import styles from '../styles/styles';
import Constants from '../constants/Constants';
import Loader from './Loader';
import HTMLView from 'react-native-htmlview';
import CustomToast from './CustomToast';
import Service from '../services/Service';
import MyView from './MyView';
import Home from './Home';
import Moment from 'moment';
import { strings } from '../services/stringsoflanguages';
import OfflineNotice from './OfflineNotice';
import { dimensions } from '../styles/base';
import { db } from './db';

let itemsRef = db.ref('/items');
export default class Details extends Component {
  constructor(props){
    super(props);
    constants = new Constants();
    service = new Service();
    home = new Home();
    this.state = { 
      userResponse: {},
        details : {},
        loading: false,
        accepted : false,
        localData : {},
        hired : true,
        jobId : '',
        clientName : "",
        clientId  : "",
        newHide : true,
        clientDetails: "",
        clientImage : "",
        hideProjectButton : true,
        jobDetails : "",
        chatString : "",
        senderId : "",
        receiverId : "",
        userUniqueKey : "",
        skills : "",
        category : ""
      }
  }

 componentDidMount() {
  // itemsRef.child('users').child('-LVrn98NU0NRt2QjjFKp').once('value').then((snapshot) => {
  //   console.log('val', snapshot.val())
  // });
    this.setState ({ loading: true});
    setTimeout(() => {
      this.setState ({ loading: false});
      service.getUserData('user').then((keyValue) => {
        console.log("local", keyValue);
        var parsedData = JSON.parse(keyValue);
        console.log("json", parsedData);
        this.setState({ userResponse: parsedData});
     }, (error) => {
        console.log(error) //Display error
      });
      service.getUserData('userdetails').then((keyValue) => {
        console.log("local", keyValue);
        var parsedData = JSON.parse(keyValue);
        console.log("json", parsedData);
        this.setState({ details: parsedData});
        this.getDetails(parsedData);
     }, (error) => {
        console.log(error) //Display error
      });
      if(this.props.navigation.state.params)
    {
   // this.setState({ details: this.props.navigation.state.params.details})
   console.log('props', this.props.navigation.state.params)
    if(this.props.navigation.state.params.details != undefined)
    {
      if(this.props.navigation.state.params.details.accepted != "Pending" && this.props.navigation.state.params.details.accepted != undefined )
      {
        
        this.setState({ accepted : true})
      }
    }
    else
    {
      this.setState({ jobId: this.props.navigation.state.params.jobDetails.job_id})
      this.getJobDetails();
    }
     }    
      }, 1000)

      
    
  }

  getDate = (val) => {
    var newDate = Moment(val).format('DD/MM/YYYY');
    return newDate;
  }

  getJobDetails = () => {
    service.getFreelancerDetails(this.state.userResponse.api_token, this.state.jobId).then((res) => {
    console.log('jobResponse', res);
    this.setState({ details: res.request_list})
    this.setState({newHide:false});
    if(res.request_list.skills != null)
    {
      this.setState({skills : res.request_list.skills}) 
    }
    if(res.request_list.catid != null)
    {
      this.setState({category : res.request_list.catid}) 
    }
    if(res.accepted != "Pending" && res.accepted != undefined )
    {
      
      this.setState({ accepted : true})
    }
    // if(res.request_list.accepted == "Accepted")
    // {
    //   this.setState({ chat : false})
    //   this.setState({ hireText: strings.Hirefreelancer})
    // }
    // else if(res.request_list.accepted == "Hired")
    // {
    //   this.setState({ hireText: "Go To Milestones"})
    // }
    })
  }

  deleteFirebaseChat = () => {
    var idExist3 = false;

  // itemsRef.child('users').child(this.state.userUniqueKey[0]).once('value').then((snapshot) => {
  //    console.log('val', snapshot.val())
  // });
    itemsRef.child(this.state.chatString).once('value').then((snapshot) => {
      console.log(snapshot.val())
   
if (snapshot.val()){
  itemsRef.child(this.state.chatString).remove();
  
  }
     
     });

     console.log('key', this.state.userUniqueKey[0])
     var newKey = this.state.userUniqueKey[0];
     itemsRef.child('users').once('value').then((snapshot) => {
      if(snapshot.val())
      {
        itemsRef.child('users').child(newKey).remove();
      }
     });
  
  }

  getDetails = (object) => {
    // console.log(object)


    var c = "shdsdh";

if (object.job_id == undefined){

  c = object.jobid;

}else{

  c = object.job_id;

}
    service.getFreelancerDetails(this.state.userResponse.api_token, c).then((res) => {
    console.log("listcheckres", res);
    if(res.request_list.skills != null)
    {
      this.setState({skills : res.request_list.skills}) 
    }
    if(res.request_list.catid != null)
    {
      this.setState({category : res.request_list.catid}) 
    }
// firebase users 
      itemsRef.child('users').once('value', (snapshot) => {
        // itemsRef.child('users').on('value', (snapshot) => {
        
        // console.log("users", snapshot.val());
        var senderId = res.users.id;
        var receiverId = this.state.userResponse.id;
        var messages = [];
        snapshot.forEach(function(childSnapshot) {
        // console.log("user", childSnapshot.val())
        
        if(childSnapshot.val().receiverId == receiverId && childSnapshot.val().senderId == senderId || childSnapshot.val().receiverId == senderId && childSnapshot.val().senderId == receiverId)
        {
        console.log( childSnapshot.ref.path);
        messages.push(childSnapshot.ref.path.pieces_[2]);
        }
        console.log('messages', messages[0])     
        })
      
        this.setState({userUniqueKey : messages})
        
        });
      
  //user chat messages
 const FirstString = res.users.id + "a" + this.state.userResponse.id;
const secondString = this.state.userResponse.id + "a" + res.users.id;
var idExist = false;
itemsRef.child(FirstString).on('value', (snapshot) => {
console.log('firstString', snapshot.val())
if (snapshot.val()){

idExist = true;

}

});


if (idExist){
  this.setState({chatString : FirstString})
}
else
{
  this.setState({chatString : secondString})
itemsRef.child(secondString).on('value', (snapshot) => {
console.log('secondString', snapshot.val())
});


}
      this.setState({jobDetails: res});
      if(res != undefined)
      {
        this.setState({newHide:false});
        if(res.request_list.accepted == "Hired")
        {
          this.setState({hideProjectButton : false})
        }
      if(res.request_list.accepted == "Hired" || res.request_list.accepted == "Accepted")
      {
        this.setState({ hired: false});
      }
      if(res.users)
      {
        this.setState({  clientId : res.users.id})
        this.setState({  clientName : res.users.username})
        this.setState({  clientName : res.users.username})
      }
      this.setState({ details: res.request_list});
      this.setState({ clientDetails: res.users});
      if(this.state.details.accepted != "Pending")
      {
        this.setState({ accepted : true})
      }
      }
    })
   }

  requestAcceptReject = (val) => 
  {
  this.setState ({ loading: true});
  setTimeout(() => {
    this.setState ({ loading: false});
    if(val == "a")
    {
      service.requestResponse(this.state.userResponse.api_token, "Accepted", this.state.details.jobid).then(res => {
        console.log("reslocal", res);
        if(res)
        {
          this.refs.defaultToastBottom.ShowToastFunction(strings.Requestaccpetsuccessfully);
          this.goToHome();
        }
        else
        {
          this.refs.defaultToastBottom.ShowToastFunction('An Error Occured');
        }
      });
     
    }
    else
    {
      service.requestResponse(this.state.userResponse.api_token, "Rejected", this.state.details.jobid).then(res => {
        console.log("reslocal", res);
        if(res)
        {
          this.refs.defaultToastBottom.ShowToastFunction(strings.RequestReject);
          this.goToHome();
        }
        else
        {
          this.refs.defaultToastBottom.ShowToastFunction('An Error Occured');
        }
      });

     this.deleteFirebaseChat();
     
    }
    }, 1000)
  }

  openProjects =  () => 
  {
    this.props.navigation.navigate('MilestoneList', { client_Details:  this.state.jobDetails })
  }


  openChat = () => {
   var Details = {
     id : this.state.clientId,
     receiverName : this.state.clientName,
     receiverImage : this.state.clientImage
   }
   this.props.navigation.navigate('Chat', { chatDetails: Details})
  }
  goBack = () =>{
    this.props.navigation.navigate('Home')
   }

goToHome()
{
 setTimeout(() => {
 this.props.navigation.navigate('Home')
}, 1000)
}

createMilestone()
{
setTimeout(() => {
this.props.navigation.navigate('Create')
}, 1000)
}

viewClientProfile = () => {
  this.props.navigation.navigate('ClientProfile', { chatDetails: this.state.clientDetails})
}
  
  render() {
     // console.log(this.state.details)
    return (
  <SafeAreaView style = { styles.MainContainerRequest }>
    <OfflineNotice/> 
        <View style={styles.commontoolbar}>
          <TouchableOpacity style={styles.commontoolbarButton} onPress={() => this.goBack()}>
          <Image source={constants.backicon} style={styles.commonBackIcon}/>
          </TouchableOpacity>
          <Text style={styles.toolbarTitle}>{strings.Jobdetail}</Text>
          <Text style={styles.commontoolbarButton}></Text>
        </View>
        <View style={{height: dimensions.fullHeight }}>
        <ScrollView>
        <View style={{marginTop:10}}>
        <View style={styles.rowAlignSideMenuJob}>
                 <View style={{width:"10%"}}> 
                  </View>
                  <View style={{width:"35%"}}> 
                  <Text style={styles.textWrapDetails}> {strings.Title}
                  </Text>
                  </View>
                 
                  <View  style={{width:"10%"}}>
                  </View>
                  <View style={{width:"45%"}}> 
                  <Text style={styles.textWrap2Details}> {this.state.details.title}
                  </Text>
                  </View>
         </View>
         <View style={styles.rowAlignSideMenuJob2}>
                 <View style={{width:"10%"}}> 
                  </View>
                    <View style={{width:"35%"}}>
                  <Text style={styles.textWrapDetails}> {strings.City}
                  </Text>
                  </View>
                  <View  style={{width:"10%"}}>
                  </View>
                  <View style={{width:"45%"}}>
                  <Text style={styles.textWrap2Details}> {this.state.details.country}
                  </Text>
                  </View>
         </View>
        
         <View style={styles.rowAlignSideMenuJob}>
                   <View style={{width:"11%"}}> 
                  </View>
                  <View style={{width:"34%"}}>
                  <Text style={styles.textWrapDetails}>{strings.Budget}
                  </Text>
                  </View>
                 
                  <View  style={{width:"11%"}}>
                  </View>
                  <View style={{width:"44%"}}> 
                  <Text style={styles.amountDetails}>{this.state.details.budget} SAR
                  </Text>
                  </View>
         </View>
         <View style={styles.rowAlignSideMenuJob2}>
                  <View style={{width:"9%"}}> 
                  </View>
                  <View style={{width:"36%"}}>
                  <Text style={styles.textWrapDetails}>  {strings.Startdate}
                  </Text>
                  </View>
                  <View  style={{width:"10%"}}>
                  </View>
                  <View style={{width:"45%"}}> 
                  <Text style={styles.textWrap2Details}> {this.getDate(this.state.details.start_date)}
                  </Text>
                  </View>
         </View>
         <View style={styles.rowAlignSideMenuJob}>
                  <View style={{width:"9%"}}> 
                  </View>
                  <View style={{width:"36%"}}>
                  <Text style={styles.textWrapDetails}>  {strings.Enddate}
                  </Text>
                  </View>
                  <View  style={{width:"10%"}}>
                  </View>
                  <View style={{width:"45%"}}> 
                  <Text style={styles.textWrap2Details}> {this.getDate(this.state.details.end_date)}
                  </Text>
                  </View>
         </View>
         <View style={styles.rowAlignSideMenuJob2}>
                   <View style={{width:"11%"}}> 
                  </View>
                  <View style={{width:"34%"}}>
                  <Text style={styles.textWrapDetails}>{strings.Skills}
                  </Text>
                  </View>
                 
                  <View  style={{width:"11%"}}>
                  </View>
                  <View style={{width:"44%"}}> 
                  <Text style={styles.amountDetails}>{this.state.skills} 
                  </Text>
                  </View>
         </View>
         <View style={styles.rowAlignSideMenuJob}>
                  <View style={{width:"9%"}}> 
                  </View>
                  <View style={{width:"36%"}}>
                  <Text style={styles.textWrapDetails}>  {strings.Category}
                  </Text>
                  </View>
                  <View  style={{width:"10%"}}>
                  </View>
                  <View style={{width:"45%"}}> 
                  <Text style={styles.textWrap2Details}> {this.state.category}
                  </Text>
                  </View>
         </View>
        
         <View style={styles.rowAlignSideMenuJob2}>
                 <View style={{width:"10%"}}> 
                  </View>
                  <View style={{width:"35%"}}>  
                  <Text style={styles.textWrapDetails}> {strings.Status}
                  </Text>
                  </View>
                  <View  style={{width:"10%"}}>
                  </View>
                  <View style={{width:"45%"}}> 
                  <Text style={styles.textWrap2Details}> {this.state.details.accepted}
                  </Text>
                  </View>
         </View>
         <View style={styles.rowAlignSideMenuJob}>
                <View style={{width:"10%"}}> 
                  </View>
                  <View style={{width:"35%"}}> 
                  <Text style={styles.textWrapDetails}> {strings.Description}
                  </Text>
                  </View>
                  <View style={{width:"10%"}}>
                  </View>
                  <View  style={{width:"45%"}}> 
                  <Text style={{ textTransform: 'capitalize'}}> {this.state.details.description}
                  </Text>
                  </View>
         </View>
        
        
              </View>
              <Loader
              loading={this.state.loading} />
            </ScrollView>
     </View>  
    <MyView hide={this.state.newHide} style={{position:'absolute', bottom:20, width:'100%'}}>
    <MyView  hide={ this.state.accepted } style={{width:'100%'}}>
              <View  style={styles.rowAlignSideMenuDetailNewButton}>
              <View style={styles.emptySpaceRequest}>
              </View>
              <View style={styles.buttonWidthRequest}>
              <TouchableOpacity style={styles.buttonBackgroundrequest} onPress={() => this.requestAcceptReject('a')}>
		          <Text style={styles.buttonText}>{strings.Accept}</Text>
		           </TouchableOpacity>
              </View>
              <View style={styles.emptySpaceRequest}>
              </View>
              <View style={styles.buttonWidthRequest}>
              <TouchableOpacity style={styles.buttonBackgroundrequest} onPress={() => this.requestAcceptReject('r')}>
		          <Text style={styles.buttonText}>{strings.Reject}</Text>
		           </TouchableOpacity>
              </View>
                <View style={styles.emptySpaceRequest}>
              </View>
              </View> 
      </MyView> 
    <MyView style = { {justifyContent:"center",alignSelf:"center",marginTop:10, width:'80%', height:50, backgroundColor:'#F0F0F0',  borderWidth:1} } >
      <TouchableOpacity onPress={() => this.viewClientProfile()}>
         <Text style={styles.textStyleChat}>{strings.ViewClientProfile}</Text>
      </TouchableOpacity>
      </MyView>
      <MyView style = { {justifyContent:"center",alignSelf:"center",marginTop:10, width:'80%', height:50, backgroundColor:'#F0F0F0', borderWidth:1} } >
      <TouchableOpacity onPress={() => this.openChat()}>
         <Text style={styles.textStyleChat}>{strings.Chatwithclient}</Text>
      </TouchableOpacity>
      </MyView>
      <MyView style = { {justifyContent:"center",alignSelf:"center",width:'80%',marginTop:10,  height:50, backgroundColor:'#F0F0F0', borderWidth:1} } hide={this.state.hideProjectButton}>
      <TouchableOpacity onPress={() => this.openProjects()} >
         <Text style={styles.textStyleChat}>{strings.GoToMilestones}</Text>
      </TouchableOpacity>
      </MyView>
      </MyView>
      
     
      <CustomToast ref = "defaultToastBottom"/>  
    
     
   </SafeAreaView>
	   
    );
  }
}

