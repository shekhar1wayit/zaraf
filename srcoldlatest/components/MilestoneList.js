import React, { Component } from "react";
import {
  Platform,
  StyleSheet,
  FlatList,
  SafeAreaView,
  Text,
  View,
  Image,
  ImageBackground,
  Button,
  TextInput, 
  TouchableOpacity,
  Modal,
  ScrollView,
  Dimensions,
  Alert
} from "react-native";
import OfflineNotice from './OfflineNotice';
import CustomToast from './CustomToast';
import Constants from "../constants/Constants";
import Service from "../services/Service";
import styles from "../styles/styles";
import MyView from './MyView';
import Loader from './Loader';
import SideMenu from './SideMenu';
import Moment from 'moment';
import { strings } from '../services/stringsoflanguages';
import { dimensions } from "../styles/base";
import RNPaytabs from 'rn-paytabs';
const { width , height } = Dimensions.get('window');



export default class MilestoneList extends Component {
  constructor(props) {
    super(props);
    service = new Service();
    sidemenu = new SideMenu();
    constants = new Constants();
    this.state = {
      userResponse: {},
      jobs: [],
      failed: false,
      search : true,
      loading:false,
      dummyText : "",
      projectId: " ",
      collapsed:false,
      projectAmount : " ",
      milestoneTotalAmount : 0,
      hidePlus : false,
      refreshing : false,
      jobId : '',
      jobAmount : 0,
      footerButton : strings.CreateMilestones,
      freeLancerId: ""
    };
   
    
  }

  setModalVisible(visible) {
    this.setState({modalVisible: visible});
  }

  findFreelancer = () => {

  }

  componentDidMount() {
    if (this.props.navigation.state.params) {
        console.log(this.props.navigation.state.params);
     
        if(this.props.navigation.state.params.client_Details.request_list != undefined)
        {
          console.log("this one")
          this.setState({ projectId: this.props.navigation.state.params.client_Details.request_list.jobid });
        }
        else
        {
          this.setState({ projectId: this.props.navigation.state.params.client_Details.jobid });
       // this.setState({ jobId: this.props.navigation.state.params.details.job_id });
      //  this.setState({ projectAmount: this.props.navigation.state.params.details.amount });
       
        }
        
      }
    this.setState ({ loading: true});
    service.getUserData("user").then(
      keyValue => {
        console.log("local", keyValue);
        var parsedData = JSON.parse(keyValue);
        console.log("json", parsedData);
        this.setState({ userResponse: parsedData });
        this.getMilestoneList();
        console.log('projectAmount', this.state.projectAmount)
        console.log('milestoneAmount', this.state.milestoneTotalAmount)
        
       
      },
      error => {
        console.log(error); //Display error
      }
    );
   
   // service.saveUserData('count', 1);
  }

  getDate = (val) => {
    var newDate = Moment(val).format('DD/MM/YYYY');
    return newDate;
  }

  changeStatus = (val) => 
  {
    console.log(val)
  if(this.state.userResponse.usertype == 1 )
  {
  var status = 3;
  // if(val.status == 2)
  // {
  // //  this.props.navigation.navigate('PaymentCard')
  //    status = 3
  // }
  if(val.status != 3)
  {
  //   this.setState ({ loading: true});   
    
          RNPaytabs.start({
            "pt_merchant_email": "sc.shankychugh@gmail.com",
            "pt_secret_key": "XIoOlzKmq720MMX6R2m3su2cJ08tZ3WWMlL7UwukDwlpjiwbkSeNnhZ6h8zhbweYRZ6LwA697rzrJ5C4YRSl8RsqZ2HbyWaqWgcs",// Add your Secret Key Here
            "pt_transaction_title": "Mr. John Doe",
            "pt_amount": "1.0",
            "pt_currency_code": "BHD",
            "pt_shared_prefs_name": "myapp_shared",
            "pt_customer_email": "test@example.com",
            "pt_customer_phone_number": "+97333109781",
            "pt_order_id": "1234567",
            "pt_product_name": "Tomato",
            "pt_timeout_in_seconds": "300", //Optional
            "pt_address_billing": "Flat 1 Building 123 Road 2345",
            "pt_city_billing": "Juffair",
            "pt_state_billing": "Manama",
            "pt_country_billing": "BHR",
            "pt_postal_code_billing": "00973", //Put Country Phone code if Postal code not available '00973'//
            "pt_address_shipping": "Flat 1 Building 123 Road 2345",
            "pt_city_shipping": "Juffair",
            "pt_state_shipping": "Manama",
            "pt_country_shipping": "BHR",
            "pt_postal_code_shipping": "00973", //Put Country Phone code if Postal
            "pt_color": "#3a33ff", // for IOS only
            "pt_theme_light": false // for IOS only
          }, (response) => {
            console.log('res', response)
            // Callback for success & fail.
           
            // { pt_token_customer_email: '',pt_token: '',pt_token_customer_password: '', pt_transaction_id: '123456',pt_response_code: '100' }
           
            RNPaytabs.log("on Response Payment");
            // Response Code: 100 successful otherwise fail
            if (response.pt_response_code == '100')
            {
              RNPaytabs.log("Transaction Id: " + response.pt_transaction_id);
              service.completeMilestone(this.state.userResponse.api_token, val.id, status).then(res => {
      console.log("reslocal", res);
      if(res != undefined) {
        if(res.status == "Success")
        {
           this.setState ({ loading: false}); 
           setTimeout(() => 
           {
          console.log(res)
        Alert.alert(strings.StatusUpdatedSuccessfully);
         this.getMilestoneList();
      }, 500);
    }
        else{
          Alert.alert("an error occured")
        }
      }
            },   error => {
              Alert.alert(JSON.stringify(error))
            }
         )
          } 
            else
            {
              RNPaytabs.log("Otherwise Response: " + response.pt_response_code);
            }
           
            // Tokenization
            //RNPaytabs.log(response.pt_token_customer_email);
            //RNPaytabs.log(response.pt_token_customer_password);
            //RNPaytabs.log(response.pt_token);
           
          });
   }
   }
  }

  openDrawer = () => {
    // sidemenu.userData();
    var jobId = {
      jobid : this.state.jobId
    }
    console.log('jobId', this.state.jobId);
    if(this.state.userResponse.usertype == 1 )
    {
    this.props.navigation.navigate('JobDetails') 
    }
    else
    {
    this.props.navigation.navigate('Details') 
    }
  };

  handleRefresh() {
    this.setState(
      {
        refreshing: true
      })
      
      this.getMilestoneList();
    
  }


  getMilestoneList = () => {
    console.log("projectId",  this.state.projectId)
    if(this.state.projectId)
    {
    service.getMilestoneList(this.state.userResponse.api_token, this.state.projectId).then(res => {
      console.log("reslocal", res);
      this.setState({freeLancerId : res.milestone[0].freelancer_api_token[0].api_token})
      this.setState ({ jobId : res.milestone[0].jobid});
      this.getJobDetails();
      if(res.status_code != 400)
      {
              setTimeout(() => {
              if (res){
                this.setState ({ loading: false});
                console.log('length', res.milestone.length)
                var totalAmount = 0;
                var paidArray = [];
                for (i=0; i<res.milestone.length; i++)
                {
                 if(res.milestone[i].status == 3)
                 {
                   paidArray.push(res)
                 }
                 console.log('amountjob', this.state.jobAmount)
                  totalAmount += parseFloat(res.milestone[i].amount);
                  this.setState ({ milestoneTotalAmount: totalAmount});
                  console.log(res.milestone[i])
                }
               // console.log('paid Response', paidArray)
             //   console.log('TA', totalAmount)
                this.setAmount(totalAmount)
               
                console.log('total', totalAmount);
                if(res.milestone.length ===  0)
                {
                    this.setState ({ dummyText: strings.NoMilestonefound});
                }
                else
                {
                this.setState({ jobs: res.milestone});
                this.setState ({ dummyText: " "});
                }
            }
            });
      }
      else
      {
        this.refs.defaultToastBottom.ShowToastFunction('An Error Occurred');
      }
   }, 3000)
   }
   this.setState ({ loading: false});
   this.setState(
    {
      refreshing: false
    })
  };

  getJobDetails = () => {
    service.getFreelancerDetails(this.state.userResponse.api_token, this.state.jobId).then((res) => {
      console.log("jobDetails", res.request_list.budget);
      console.log('amountjob2', this.state.milestoneTotalAmount)
     // this.setState({ jobAmount : res.request_list.budget})
      if(res.request_list.budget == this.state.milestoneTotalAmount )
      {
       this.setState({ footerButton : strings.GiveRating})
      }
    })
  }

  searchPage = () =>{
  this.setState({ search: false});
    }
  
    hideSearch = () =>{
      this.setState({ search: true});
    }

    setAmount = (totalAmount) => 
    {
      this.setState ({ milestoneTotalAmount: totalAmount});
      console.log('amount', totalAmount)
      console.log('jobAmount', this.state.jobAmount)
      console.log('projectAmount', this.state.projectAmount)
    //  alert(totalAmount);
  if(this.state.projectAmount != 0 && totalAmount != 0 )
  {
    if(this.state.jobAmount == totalAmount )
    {
     this.setState({ hidePlus : true})
    }
  }
  }


  goToPostproject = (val) => {
    if(this.state.userResponse.usertype == 1)
    {
      console.log('buttontext', val)
    if(val == "Create Milestones")
    {
    var projectMilestoneDetails = {
      projectAmount : this.state.projectAmount,
      milestoneAmount : this.state.milestoneTotalAmount,
      jobId : this.state.projectId
    }
    this.props.navigation.navigate("Create", { milestoneDetails: projectMilestoneDetails });
    }
    else
    {
      var projectMilestoneDetails = {
        api_token : this.state.freeLancerId,
        jobId : this.state.projectId
      }
      console.log('freelancerId', this.state.freeLancerId)
      this.props.navigation.navigate("Rating", {project : projectMilestoneDetails});
    }
  }
  };

  openDetails = (val) => {
   var jobData = {
     token : this.state.userResponse.api_token,
     details : val
   }
   this.props.navigation.navigate('JobDetails',  { details: jobData }) 
  }

  render() {
    console.log('amount', this.state.projectAmount)
    return (
      <SafeAreaView source={constants.loginbg} style={{flex:1}}>
        <OfflineNotice/> 
        
        <MyView style={styles.tabsToolbar}>
        <TouchableOpacity onPress={() => this.openDrawer()}>
        <Image source={constants.backicon} style={styles.backIcon} />
        </TouchableOpacity>
         <Text style={styles.toolbarTitle}>{strings.Milestonelist}</Text>
         <TouchableOpacity onPress={() => this.goToNotification()}>
        </TouchableOpacity>
         <TouchableOpacity style={styles.searchIcon}>
        
        </TouchableOpacity>
        </MyView>
      
       {/* <View style={styles.searchPadding}>
       <MyView  style={styles.searchContainer}>
          <View style={styles.topSearchbar}>
              <Image source={constants.searchicon} style={styles.newsearchIcon} />
              <View style={styles.empty}>
              </View>
            <TextInput placeholder="Search"  placeholderTextColor="#a2a2a2" style={styles.topInput}/>
          </View>
      </MyView>
      </View> */}
       {/* <ScrollView>
      
        <View style={styles.listCenter}>
        </View>
        
       
       </ScrollView> */}
       {this.state.dummyText ?   <Text style = {styles.defaultTextSize3}>{this.state.dummyText}</Text> : null}
     
        
        <FlatList
              data={this.state.jobs}
              keyExtractor={(item, index) => index}
              style={styles.listCardWidthMilestone}
              extraData={this.state.jobs}
              refreshing={this.state.refreshing}
             onRefresh={this.handleRefresh.bind(this)}
              renderItem={({ item, index }) => (
                <View  style={styles.spaceFromTop}>
                    <TouchableOpacity style={styles.listCardMilestoneList}>
                    <View style={{marginTop:10}}>
                  
                  <View style={{ marginTop:5, height:130}}>
                   <View style={styles.rowAlignSideMenuJob}>
                   <View style={{width:"5%"}}> 
                  </View>
                  <View  style={{width:"35%"}}> 
                  <Text style={styles.textWrapDetails}>{strings.Amount}
                  </Text>
                  </View>
                  <View  style={{width:"10%"}}>
                  </View>
                  <View style={{width:"45%"}}> 
                  <Text> {item.amount} SAR
                  </Text>
                  </View>
                  </View>
                  <View style={styles.rowAlignSideMenuJob2}>
                  <View style={{width:"5%"}}> 
                  </View>
                  <View style={{width:"35%"}}> 
                  <Text style={styles.textWrapDetails}>{strings.DueDate}
                  </Text>
                  </View>
                  <View  style={{width:"10%"}}>
                  </View>
                  <View style={{width:"45%"}}> 
                  <Text style={styles.textWrap2Details}> {this.getDate(item.end_date)}
                  </Text>
                  </View>
                  </View>
                  <View style={styles.rowAlignSideMenuJob}>
                  <View style={{width:"5%"}}> 
                  </View>
                  <View style={{width:"35%"}}> 
                  <Text style={styles.textWrapDetails}>{strings.Description}
                  </Text>
                  </View>
                  <View  style={{width:"10%"}}>
                  </View>
                  
                  <View style={{width:"45%"}}> 
                 
                  <Text style={styles.textWrap2Details}> {item.description}
                  </Text>
                 
                  </View>
                
                 
  
                  </View>
                  <View style={styles.rowAlignSideMenuJob2}>
                  <View style={{width:"5%"}}> 
                  </View>
                  <View style={{width:"35%"}}> 
                  <Text style={styles.textWrapDetails}>{strings.Status}
                  </Text>
                  </View>
                  <View  style={{width:"10%"}}>
                  </View>
                  <View style={{width:"45%"}}> 
                
                 {this.state.userResponse.usertype == 1 ? (item.status == 1  || item.status == 2 ?  <Text style={styles.textWrap2Details}> {strings.Created}</Text> :  <Text style={styles.textWrap2Details}> {strings.Paid}</Text>) : (item.status == 1  || item.status == 2 ?  <Text style={styles.textWrap2Details}> {strings.Created}</Text>  : <Text style={styles.textWrap2Details}>{strings.Paid} </Text> )}
                  </View>
                  </View>
                  <TouchableOpacity style={styles.statusButton} activeOpacity={0.5} onPress={() => this.changeStatus(item)}>
                    {this.state.userResponse.usertype == 1 ? (item.status == 1  || item.status == 2 ?  <Text style={styles.TextStyle2}> {strings.Pay}</Text> :  <Text style={styles.TextStyle2}> {strings.Paid}</Text>) : (item.status == 1  || item.status == 2 ?  <Text style={styles.TextStyle2}> {strings.Created}</Text>  : <Text style={styles.TextStyle2}>{strings.Paid} </Text> )}
                 </TouchableOpacity>
                  </View>
                  </View>
                     
                         
                  
                        </TouchableOpacity>
                </View>
           
     
              )}
            />
           <View style={{flex:1}}>
           {this.state.userResponse.usertype == 1 ?  <MyView style = { {justifyContent:"center",alignSelf:"center",   width:'80%',  height:50, backgroundColor:"#FF9800",   position:'absolute', bottom:( Platform.OS === 'ios' ) ? 0 : 20} } >
         <TouchableOpacity onPress={() => this.goToPostproject(this.state.footerButton)}>
         <Text style={styles.textStyleWhite}>{this.state.footerButton }</Text>
         </TouchableOpacity>
        </MyView> : null }
        </View>
        <Loader
              loading={this.state.loading} />
              <CustomToast ref = "defaultToastBottom"/> 
                    
      </SafeAreaView>
    );
  }
}
