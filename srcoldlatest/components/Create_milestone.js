import React, { Component } from "react";
import {
  Platform,
  StyleSheet,
  Alert,
  Text,
  View,
  TextInput,
  SafeAreaView,
  Image,
  ScrollView,
  KeyboardAvoidingView,
  ImageBackground,
  ActivityIndicator,
  TouchableOpacity,
  TouchableNativeFeedback,
  Dimensions
} from "react-native";
import OfflineNotice from './OfflineNotice';
import styles from "../styles/styles";
import Constants from "../constants/Constants";
const { width , height } = Dimensions.get('window');
import Service from '../services/Service';
import CustomToast from './CustomToast';
import Loader from './Loader';
import DateTimePicker from 'react-native-modal-datetime-picker'
import Moment from 'moment';
import { strings } from '../services/stringsoflanguages';

export default class Create_milestone extends Component {
  constructor(props) {
    super(props);
    constants = new Constants();

    this.state = { 
      userResponse: {},
      projectId: " ",
      isDateTimePickerVisible: false,
      amount:"",
      startDateText : strings.dueDate,
      discripation: '',
      jobAmount : '',
      loading: false,
      milestoneAmount : ' ',
      ddformatDate : strings.dueDate
     }

  }
  componentDidMount() {
    if (this.props.navigation.state.params) {
      console.log(this.props.navigation.state.params);
      if(this.props.navigation.state.params.milestoneDetails != undefined)
      {
        this.setState({ projectId: this.props.navigation.state.params.milestoneDetails.jobId });
      }
      else{
      this.setState({ projectId: this.props.navigation.state.params.projectDetails.jobId })
      }
    }
    service.getUserData("user").then(
      keyValue => {
        console.log("local", keyValue);
        var parsedData = JSON.parse(keyValue);
        console.log("json", parsedData);
        this.setState({ userResponse: parsedData });
        this.getMilestones();
        this.getJobDetails();
      },
      error => {
        console.log(error); //Display error
      }
    );
  }

  getMilestones = () => {
    service.getMilestoneList(this.state.userResponse.api_token, this.state.projectId).then(res => {
      console.log("totalMilestones", res);
      var totalAmount = 0;
      for (i=0; i<res.milestone.length; i++)
      {
        totalAmount += parseFloat(res.milestone[i].amount);
        console.log(res.milestone[i])
      }
      console.log(totalAmount);
      this.setState({milestoneAmount : totalAmount})
    })
  }

  getJobDetails = () => {
    service.getFreelancerDetails(this.state.userResponse.api_token, this.state.projectId).then((res) => {
      console.log("jobDetails", res);
      this.setState({jobAmount : res.request_list.budget})
    })
  }

  _showDateTimePicker = () => this.setState({ isDateTimePickerVisible: true });

  _hideDateTimePicker = () => this.setState({ isDateTimePickerVisible: false });

  _handleDatePicked = (date) => {
    console.log("date1", date);
    var newDate = Moment(date).format('YYYY-MM-DD');
    var newDate2 = Moment(date).format('DD/MM/YYYY');
    this.setState({ ddformatDate:newDate2})
    this.setState({ startDateText:newDate})
    this._hideDateTimePicker();
  };


  CheckInternetConnection=()=>{
    service.handleConnectivityChange().then((res) => {
    if(res.type == "none")
    {
      Alert.alert('Alert!', 'Check your internet connection');
    }
    else
    {
      this.submit();
    }
    })

 
 }




  submit = () => 
  {
      if(this.state.amount.trim() === "")
      {
        this.refs.defaultToastBottom.ShowToastFunction(strings.PleaseEnterAmount);
      }
      else if (this.state.startDateText.trim() === ""||this.state.startDateText=="Due Date") {
        this.refs.defaultToastBottom.ShowToastFunction(strings.PleaseentervalidDueDate);
      } 
      else if (this.state.discripation.trim() === "") {
        this.refs.defaultToastBottom.ShowToastFunction(strings.PleaseenterDescription);
      } 
      else
    {
      var newAmount = this.state.milestoneAmount + parseFloat(this.state.amount)
       if(this.state.jobAmount >= newAmount)
       {
          this.setState ({ loading: true});
          setTimeout(() => 
          {
            this.setState({loading: false})
            console.log("project ID", this.state.projectId)
          service.create_milestone(this.state.userResponse.api_token, this.state.projectId,this.state.amount, this.state.startDateText,this.state.discripation).then((res) => {
         if (res != undefined) 
         {
           console.log(res);
              if (res.status_code == 200)
              {
              if (res.status == "Success" )
              {
                this.refs.defaultToastBottom.ShowToastFunction(strings.MilestoneCreatedSuccessfully); 
                this.goToMilestone(res);
               // this.getMilestones();
             //   this.getJobDetails();
              }
              else 
              {
                this.refs.defaultToastBottom.ShowToastFunction(strings.NetworkError); 
              }
            
            }
          
        }
      else
      {
        this.refs.defaultToastBottom.ShowToastFunction(strings.NetworkError); 
      }
    })

      }, 3000)
      }
      else
      {
       Alert.alert(strings.milestoneAmountError)
      }
    
    
    }
  
}

goToMilestone(value)
{
  setTimeout(() => {
    var projectDetails = {
      jobid : this.state.projectId
    }
  this.props.navigation.navigate('MilestoneList', {client_Details : projectDetails  })
  }, 1000)
}

goToList = () => {
  if(this.props.navigation.state.params.milestoneDetails != undefined)
  {
  this.props.navigation.navigate('MilestoneList') 
  }
  else
  {
    this.props.navigation.navigate('Details')
  }
}


changeTextStartDate=(textString)=>{

  if (textString == "Due Date") {
    return strings.DueDate;
  }else{

    return textString;
  }
}


  render() {
    return (
      <SafeAreaView style = { styles.MainContainerProject }>
        <OfflineNotice/> 
         <View style={styles.tabsToolbar}>
        <TouchableOpacity onPress={() => this.goToList()}>
        <Image source={constants.backicon} style={styles.backIcon} />
        </TouchableOpacity>
         <Text style={styles.toolbarTitle}>{strings.CreateMilestones}</Text>
         <TouchableOpacity onPress={() => this.goToNotification()}>
        </TouchableOpacity>
         <TouchableOpacity>
         <Image style={styles.searchIcon} />
        </TouchableOpacity>
        </View>

        <ScrollView><View style={{width:width, height:height}}>
       
          <TextInput
            style={styles.Createmilestoneinput}
            underlineColorAndroid="transparent"
            
            placeholder={strings.Amount}
            onChangeText={(text)=>this.setState({ amount:text})}
            placeholderTextColor="#AEA9A8"
            autoCapitalize="none"
            returnKeyType='done'
            keyboardType="numeric"
            value={this.state.amount}
          />

          
          <View style={{ flexDirection: "row" }}>
            <View style={{ width: "100%" }}>
                  <TouchableOpacity onPress={this._showDateTimePicker} style={styles.postprojectinput}>
                  <Text style={styles.dateTextColor}>{this.changeTextStartDate(this.state.ddformatDate)}</Text>
                </TouchableOpacity>
            </View>
            
          </View>

          
          
         
          <TextInput
            style={styles.createmilestoneinputdiscrpation}
            underlineColorAndroid="transparent"
            placeholder={strings.Description}
            text
            onChangeText={(text)=>this.setState({ discripation:text})}
            placeholderTextColor="#AEA9A8"
            autoCapitalize="none"
            returnKeyType='done'
            multiline={true}
            numberOfLines={4}
           value={this.state.discripation}
           blurOnSubmit={true}
          />
        <DateTimePicker
          isVisible={this.state.isDateTimePickerVisible}
          onConfirm={this._handleDatePicked}
          onCancel={this._hideDateTimePicker}
          minimumDate={new Date()}
        />
       
      <TouchableOpacity style={ styles.bottomViewRequest} onPress={() => this.CheckInternetConnection()}>
         <Text style={styles.textStyle}>{strings.Submit}</Text>
         <CustomToast ref = "defaultToastBottom"/>  
      </TouchableOpacity>
      </View>
      </ScrollView>
       <Loader
          loading={this.state.loading} /> 
       
   </SafeAreaView>
    );
  }
}
