import React, { Component } from "react";
import {
  Platform,
  StyleSheet,
  TextInput,
  Alert,
  SafeAreaView,
  Text,
  View,
  Image,
  ImageBackground,
  Button,
  TouchableOpacity,
  Switch
} from "react-native";
import firebase  from './Config';
import FCM, {FCMEvent} from "react-native-fcm";
import Loader from './Loader';
import Constants from "../constants/Constants";
import Service from "../services/Service";
import MyView from "./MyView";
import OfflineNotice from './OfflineNotice';
import { colors, fonts, padding, dimensions, align } from "../styles/base.js";
import { strings } from "../services/stringsoflanguages";
export default class Settings extends Component {
  constructor(props) {
    super(props);
    service = new Service();
    constants = new Constants();
    this.state = {
      check: true,
      userResponse: {},
      search: true,
      switch1Value1: false,
      switch1Value2: false,
      switch1Value3: false,
      englishImage: constants.fillrdrIcon,
      arabicImage: constants.unfilledrIcon,
      notCount : 0,
      loading : false
    };
  }


  componentDidMount() {
    this.setState ({ loading: true});
    setTimeout(() => {
      this.setState ({ loading: false});
      this.setCount();
      firebase.notifications().onNotification((res) => {
        this.notification(res)
      });
      FCM.on(FCMEvent.Notification, (notif) => {
        this.notification(notif); 
      });
      service.getUserData('user').then((keyValue) => {
        console.log("local", keyValue);
        var parsedData = JSON.parse(keyValue);
        console.log("json", parsedData);
        this.setState({ userResponse: parsedData });
        this.getNotifyStatus();

        }, (error) => {
        console.log(error) //Display error
        });
    service.getUserData("language").then(
      keyValue => {
       if(keyValue == "true")
       {
         this.englishTap();
        }else{
        this.ArabicTap();
      }
          
      },
      error => {
        console.log(error); //Display error
      }
    );
    }, 3000);

  }

  setCount = () =>
{
 service.getUserData('notificationCount').then((keyValue) => {
   if(keyValue !== "none" || keyValue !== undefined || keyValue !== "")
   {
   this.setState({notCount : keyValue})
   }
   }, (error) => {
   console.log(error) //Display error
   });
}


notification = (val) => {
 console.log('this', val)
 service.getUserData('notificationCount').then((keyValue) => {
   if(keyValue !== "none" || keyValue !== undefined || keyValue !== "")
   {
   var count = ++keyValue;
   service.saveUserData("notificationCount", count); 
   console.log(count)
   }
   this.setCount();
   }, (error) => {
   console.log(error) //Display error
   });
  
}

  getNotifyStatus=()=>{

    service.getNotificationStatus(this.state.userResponse.api_token).then(res => {
      console.log(res);
      if(res.sound.sound == 2){

        console.log("here : ",res.sound.sound);
        this.setState({ switch1Value1: true });
      }else{
        console.log("there : ",res.sound.sound);
        this.setState({ switch1Value1: false });

      }

    });

  }
  
  goToNotification = () => {
    this.props.navigation.navigate('Notifications')
    }

  openDrawer = () => {
    this.props.navigation.openDrawer();
  };

  searchPage = () => {
    this.setState({ search: false });
  };

  ///////////keshav add

  toggleSwitch1 = value => {
    this.setState({ switch1Value1: value });
   if(value == true)
   {
    service.updateNotificationSound(this.state.userResponse.api_token, 2).then(res => {
      console.log(res);
    });
   }
   else
   {
    service.updateNotificationSound(this.state.userResponse.api_token, 1).then(res => {
      console.log(res);
    });
   }
   Alert.alert(strings.updatedsuccessfully)
  };
  toggleSwitch2 = value => {
    this.setState({ switch1Value2: value });
    console.log("Switch 1 is: " + value);
  };

  toggleSwitch3 = value => {
    this.setState({ switch1Value3: value });
    console.log("Switch 1 is: " + value);
  };

  englishTap = () => {
    this.setState({ englishImage: constants.fillrdrIcon });
    this.setState({ arabicImage: constants.unfilledrIcon });
    this.setState({ check: true });

    service.saveUserData("language", true);
    strings.setLanguage("en");

  };
  ArabicTap = () => {
    this.setState({ englishImage: constants.unfilledrIcon });
    this.setState({ arabicImage: constants.fillrdrIcon });
     this.setState({ check: false });

    service.saveUserData("language", false);
    strings.setLanguage("ar");

  };

  overLang() {
    if (this.state.check) {
      //this.setState({ check: false });
      strings.setLanguage("en");
      //alert("t_f");
    } else {
      //this.setState({ check: true });
      strings.setLanguage("ar");
      //alert("f_t");
    }
  }
  logOut = () => {
    Alert.alert(
      "Log Out",
      "Are you sure you want to logout?",
      [
        {
          text: "Cancel",
          style: "cancel"
        },
        {
          text: "OK",
          onPress: () => this.exit()
        }
      ],
      {
        cancelable: false
      }
    );
  };

  exit = () => {
    //service.clearLocalStorage();
    service.saveUserData("user", "");
    this.props.navigation.navigate("Welcome");
  };

  render() {
    return (
      <SafeAreaView source={constants.loginbg} style={styles.container}>
      <OfflineNotice/> 
        <View style={styles.topView}>
          <MyView style={styles.tabsToolbar} hide={!this.state.search}>
            <TouchableOpacity onPress={() => this.openDrawer()}>
              <Image source={constants.menuicon} style={styles.hamburgerIcon} />
            </TouchableOpacity>
            <Text style={styles.toolbarTitle}>{strings.Settings}</Text>
            <TouchableOpacity  onPress={() => this.goToNotification()}>
              <Image  source={constants.notificationIcongrey} style={styles.searchIcon} />
              { this.state.notCount == 0 || this.state.notCount == "" || this.state.notCount == null ?  null : <TouchableOpacity
         style={{
          borderWidth:1,
          borderColor:'rgba(0,0,0,0.2)',
          alignItems:'center',
          justifyContent:'center',
          width:18,
          height:18,
          position:'absolute',
          top:2,
          left:14,
          backgroundColor:'#fb913b',
          borderRadius:9,
        }}
 >
   <Text style={{color:"#fff"}}>{this.state.notCount}</Text>
 </TouchableOpacity>}
            </TouchableOpacity>
          </MyView>
        </View>
        <View
          style={{
            backgroundColor: "#f0f0f0",
            alignItems: "center",
            width: dimensions.fullWidth - 20,
            marginLeft: 10,
            marginTop: 30,
            height: 50,
            justifyContent: "flex-start",
            flexDirection: "row"
          }}
        >
          <Image
            source={constants.notifyIcon}
            style={{ marginLeft: 12, width: 25, height: 30 }}
          />
          <Text style={{ marginLeft: 12 }}>{strings.Notifications}</Text>
          <View
            style={{
              marginLeft: dimensions.fullWidth / 2 - 30,
              backgroundColor: "clear"
            }}
          >
            <Switch
              onValueChange={this.toggleSwitch1}
              value={this.state.switch1Value1}
            />
          </View>
        </View>
        
        <View
          style={{
            backgroundColor: "#f0f0f0",
            alignItems: "flex-start",
            width: dimensions.fullWidth - 20,
            marginLeft: 10,
            marginTop: 30,
            height: 50,
            justifyContent: "flex-start",
            flexDirection: "row",
            alignItems: "center"
          }}
        >
          <Image
            source={constants.langIcon}
            style={{ marginLeft: 12, width: 25, height: 25 }}
          />
          <Text style={{ marginLeft: 12 }}>{strings.Language}</Text>
          <View
            style={{
              left: dimensions.fullWidth / 2 - 150,
              backgroundColor: "clear",
              width: dimensions.fullWidth / 2,
              height: 50,
              flexDirection: "row"
            }}
          >
            <TouchableOpacity onPress={() => this.englishTap()}>
              <View
                style={{
                  marginLeft: 0,
                  height: 50,
                  backgroundColor: "clear",
                  width: dimensions.fullWidth / 4,
                  flexDirection: "row",
                  alignItems: "center"
                }}
              >
                <Image
                  source={this.state.englishImage}
                  style={{ marginLeft: 0, width: 25, height: 25 }}
                />
                <Text style={{ marginLeft: 8 }}>English</Text>
              </View>
            </TouchableOpacity>
            <TouchableOpacity onPress={() => this.ArabicTap()}>
              <View
                style={{
                  marginLeft: 0,
                  height: 50,
                  backgroundColor: "clear",
                  width: dimensions.fullWidth / 4,
                  flexDirection: "row",
                  alignItems: "center"
                }}
              >
                <Image
                  source={this.state.arabicImage}
                  style={{ marginLeft: 0, width: 25, height: 25 }}
                />
                <Text style={{ marginLeft: 8 }}>Arabic</Text>
              </View>
            </TouchableOpacity>
          </View>
        </View>

       <Loader
              loading={this.state.loading} />
      </SafeAreaView>
    );
  }
}
