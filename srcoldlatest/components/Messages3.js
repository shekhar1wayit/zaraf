import React, {Component} from 'react';
import {Platform, StyleSheet, SafeAreaView,Keyboard, KeyboardAvoidingView ,ScrollView, ImageBackground, Alert, FlatList, TouchableHighlight, Text, TextInput, View, Image, Button, TouchableOpacity,Dimensions} from 'react-native';
import Constants from '../constants/Constants';
import Service from '../services/Service';
import ImagePicker from "react-native-image-picker";
import MyView from './MyView';
import Loader from './Loader';
import { addItem } from '../services/ItemService';
import CustomToast from './CustomToast';
import { GiftedChat } from 'react-native-gifted-chat'
import { strings } from '../services/stringsoflanguages';
import { db } from './db';
import firebase  from './Config';
const keyboardVerticalOffset = Platform.OS === 'ios' ? 40 : 0
import {dimensions} from '../styles/base.js'
import OfflineNotice from './OfflineNotice';

import styles from "../styles/styles";
const {width,height} = Dimensions.get('window')
let itemsRef = db.ref('/items');
export default class Messages3 extends Component {
constructor(props){
super(props);
service = new Service();
constants = new Constants();
this.state = {
userData: { picture_large:{ data:{}}},
search : true,
loading:false,
dummyText : "",
name: '',
error: false,
items: [],
message : "",
userResponse: {},
senderId : "",
receiverId: "",
receiverName: "",
noMessage : " ",
senderName: " ",
timeStatus: 0,
MassageCount:0,
timerId : null, 
receiverImage : 'https://satishrao.in/wp-content/uploads/2016/06/dummy-profile-pic-male.jpg',
senderImage : 'https://satishrao.in/wp-content/uploads/2016/06/dummy-profile-pic-male.jpg',
};
this.handleChange = this.handleChange.bind(this);
this.handleSubmit = this.handleSubmit.bind(this);
this.listHeight = 0
this.footerY = 0

}


componentDidMount() {


  service.getUserData('notificationCount').then((keyValue) => {
    console.log(keyValue) 
    if(keyValue == "none" || keyValue == undefined || keyValue == "")
    {
    service.saveUserData("notificationCount", 0); 
    }
    }, (error) => {
    console.log(error) //Display error
    });
  
  // firebase.notifications().onNotification((notification) => {
  //  this.notification(notification);
  // });

  
if(this.props.navigation.state.params)
{
console.log(this.props.navigation.state.params)
if(this.props.navigation.state.params.chatDetails.id == undefined)
{
console.log('this one working')
this.setState({ receiverId: this.props.navigation.state.params.chatDetails.receiverId});
this.setState({ senderId: this.props.navigation.state.params.chatDetails.senderId});
this.setState({ receiverName: this.props.navigation.state.params.chatDetails.receiverName});
this.setState({ senderName: this.props.navigation.state.params.chatDetails.senderName});
}
else
{
  console.log('this chat is not  working')
if(this.props.navigation.state.params.chatDetails.userjob != undefined)
{
this.setState({ receiverId: this.props.navigation.state.params.chatDetails.userjob[0].id});
this.setState({ receiverName: this.props.navigation.state.params.chatDetails.userjob[0].username});
if(this.props.navigation.state.params.chatDetails.userjob[0].image_path !== null)
{
  this.setState({ receiverImage: this.props.navigation.state.params.chatDetails.userjob[0].image_path});
}
else
{
  this.setState({ receiverImage: 'https://satishrao.in/wp-content/uploads/2016/06/dummy-profile-pic-male.jpg'});
}
}
else
{
  console.log('this chat working');
 if(this.props.navigation.state.params.chatDetails.receiverImage !== undefined)
 {
      if(this.props.navigation.state.params.chatDetails.receiverImage !== null && this.props.navigation.state.params.chatDetails.receiverImage !== "")
    {
      this.setState({ receiverImage: this.props.navigation.state.params.chatDetails.receiverImage});
    }
    else
    {
      this.setState({ receiverImage: 'https://satishrao.in/wp-content/uploads/2016/06/dummy-profile-pic-male.jpg'});
    }
}
else
{
  this.setState({ receiverImage: 'https://satishrao.in/wp-content/uploads/2016/06/dummy-profile-pic-male.jpg'});
}
if(this.props.navigation.state.params.chatDetails.title !== undefined)
{
console.log('message', this.props.navigation.state.params.chatDetails.message);

this.setState({ receiverId: this.props.navigation.state.params.chatDetails.sender_id});
}
else
{
this.setState({ receiverId: this.props.navigation.state.params.chatDetails.id});
}
if(this.props.navigation.state.params.chatDetails.message !== undefined)
{
  this.setState({receiverName : this.props.navigation.state.params.chatDetails.username});
}
else
{
  if(this.props.navigation.state.params.chatDetails.receiverName !== undefined)
  {
  this.setState({ receiverName: this.props.navigation.state.params.chatDetails.receiverName});
  }
  else
  {
  this.setState({ receiverName: this.props.navigation.state.params.chatDetails.username});
  }
}
}

service.getUserData('user').then((keyValue) => {
// console.log("local", keyValue);
var parsedData = JSON.parse(keyValue);
console.log("userjson", parsedData);
this.setState({ userResponse: parsedData});
this.setIds(parsedData);

}, (error) => {
console.log(error) //Display error
});

// this.setState({ senderName: this.props.navigation.state.params.chatDetails.user.username});
}
} 

this.setState ({ loading: true});
setTimeout(() => {
this.setState ({ loading: false});
this.getFirebaseChat();
}, 2000)


}

scrollToEndOfList = () => {
alert("scrolling not working")
// setTimeout(() => this.refs.scrollView.scrollTo(0), 200)
}

setIds = (userData) => {
this.setState({ senderId: userData.id});
this.setState({ senderName: userData.username});
if(userData.image_path !== null)
{
  this.setState({ senderImage: userData.image_path}); 
}
else
{
  this.setState({ senderImage: 'https://satishrao.in/wp-content/uploads/2016/06/dummy-profile-pic-male.jpg'});
}

}

getFirebaseChat = ()=>{

this.fireBaseUserIds()


}


NewgetFirebaseChat = (firebaseChatId) => {

this.setState({timeStatus:0});

console.log("receiverId", this.state.receiverId)
// console.log("userResponse", this.state.userResponse);
var receiverId = this.state.receiverId;
var senderId = this.state.senderId;
console.log('receiverId', this.state.receiverId)
console.log('senderId', this.state.senderId)
itemsRef.child(firebaseChatId).on('value', (snapshot) => {
console.log(snapshot.val());
var messages = [];
snapshot.forEach(function(childSnapshot) {
console.log("receiverID", receiverId);
console.log(childSnapshot.val());
if(childSnapshot.val().receiverId == receiverId && childSnapshot.val().senderId == senderId || childSnapshot.val().receiverId == senderId && childSnapshot.val().senderId == receiverId)
{
messages.push(childSnapshot.val());
}
console.log(childSnapshot.val())
});
console.log('messages', messages)

if(messages.length == 0 )
{
this.setState ({ noMessage: strings.NochatFound });
}
else
{
this.setState ({ noMessage: " " }); 
}
if(messages !== null)
{ 
var reversedMessages = messages.reverse();
// this.setState({items: messages});
this.setState({
    messages: reversedMessages
  })
if (messages.length < 10) {

}else{

if (messages.length == null || messages.length == 0 || messages.length == undefined){

}else{
console.log("messagelength", messages.length)
service.getUserData('notificationCount').then((keyValue) => {
  console.log("count", keyValue)
  }, (error) => {
  console.log(error) //Display error
  });


}


}

}
});


}

timeAction =(messages)=>{




}
openMessageList = () => {
this.props.navigation.navigate('Messages')
}


handleChange(e) {
this.setState({
message: e.nativeEvent.text
});
}

goToNotification = () => {
  this.props.navigation.navigate('Notifications')
  }

UserIds = (messages) => 
{
//Check if user1’s id is less than user2'

const FirstString = this.state.senderId + "a" + this.state.receiverId;
const secondString = this.state.receiverId + "a" + this.state.senderId;
var newReturn = "";
var idExist = false;


itemsRef.child(FirstString).once('value').then((snapshot) => {

if (snapshot.val()){

idExist = true;
newReturn = FirstString;
}

});


if (idExist){

this.NewhandleSubmit(newReturn, messages);

}else{

itemsRef.child(secondString).once('value').then((snapshot) => {

if (snapshot.val()){

idExist = true;
newReturn = secondString;
this.NewhandleSubmit(newReturn, messages);

}else{

console.log(FirstString);
console.log(secondString);


newReturn = FirstString;
this.NewhandleSubmit(newReturn, messages);

}
});


}

}


fireBaseUserIds = () => 
{
//Check if user1’s id is less than user2'

const FirstString = this.state.senderId + "a" + this.state.receiverId;
const secondString = this.state.receiverId + "a" + this.state.senderId;
var newReturn = "";
var idExist = false;
var idValue = 0

itemsRef.child(FirstString).on('value', (snapshot) => {

if (snapshot.val()){

idExist = true;
newReturn = FirstString;
}

});


if (idExist){

this.NewgetFirebaseChat(newReturn);

}else{

itemsRef.child(secondString).on('value', (snapshot) => {

if (snapshot.val()){

idExist = true;
newReturn = secondString;
this.NewgetFirebaseChat(newReturn);

}else{

console.log(FirstString);
console.log(secondString);


newReturn = FirstString;
this.NewgetFirebaseChat(newReturn);

}
});


}

}


handleSubmit(messages) {

this.UserIds(messages)


}

 uuidv4 = () => 
 {
  return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function(c) {
    var r = Math.random() * 16 | 0, v = c == 'x' ? r : (r & 0x3 | 0x8);
    return v.toString(16);
  });
}

NewhandleSubmit=(userChatId, messages = [])=>{
    messages.forEach(message => {
        var now = new Date().getTime()
        itemsRef.child(userChatId).push({
                    user: {
                        _id: this.state.senderId,
                        name: 'React Native',
                        avatar: 'https://reactjs.org/img/logo_og.png',
                      },
                    _id: this.uuidv4(),
                    text: message.text,
                    createdAt: now,
                    senderId :this.state.senderId,
                receiverId :this.state.receiverId, 
                senderName : this.state.senderName || " ",
                receiverName : this.state.receiverName || " "
                })

service.messagesPush(this.state.receiverId, message.text,this.state.senderId).then((res) => {
console.log("checkres", res);
})
       


itemsRef.child('users').once('value', (snapshot) => {
const dataRecieved = snapshot.val();
if(!dataRecieved)
{
  console.log(this.state.senderImage)
console.log("users one")
itemsRef.child('users').push({
message: message.text,
senderId :this.state.senderId,
receiverId :this.state.receiverId, 
senderName : this.state.senderName,
receiverName : this.state.receiverName,
time : new Date().toLocaleString(),
receiverImage : this.state.receiverImage,
senderImage : this.state.senderImage
});

}
else
{

console.log("mymessagesss", this.state.message);
var receiverId = this.state.receiverId;
var senderId = this.state.senderId;
var senderName = this.state.senderName;
var receiverName = this.state.receiverName;
console.log('receiverImage447', this.state.receiverImage)
if(this.state.receiverImage != "" || this.state.receiverImage != undefined)
{
 var receiverImage = this.state.receiverImage;
}
else
{
  var receiverImage = 'https://satishrao.in/wp-content/uploads/2016/06/dummy-profile-pic-male.jpg';
}
var senderImage =  this.state.senderImage;
var data = "";
itemsRef.child('users').once('value', (snapshot) => {
// itemsRef.child('users').on('value', (snapshot) => {

// console.log("users", snapshot.val());
var messages = [];
snapshot.forEach(function(childSnapshot) {
// console.log("user", childSnapshot.val())

if(childSnapshot.val().receiverId == receiverId && childSnapshot.val().senderId == senderId || childSnapshot.val().receiverId == senderId && childSnapshot.val().senderId == receiverId)
{
messages.push(childSnapshot.val());
}
// console.log('messagesUsers', messages.length);

})
console.log("messagelength", messages.length)



if (messages.length == 0 )
{
console.log("this messages")
itemsRef.child('users').push({
message : message.text,
senderId : senderId,
receiverId :receiverId, 
senderName : senderName,
receiverName : receiverName,
time : new Date().toLocaleString(),
receiverImage : receiverImage,
senderImage : senderImage
}); 
}
else{
console.log("other messages")
}

});
console.log("data", data)

}
});
})


}

notification = (val) => {
  service.getUserData('notificationCount').then((keyValue) => {
    if(keyValue !== "none" || keyValue !== undefined || keyValue !== "")
    {
    var count = ++keyValue;
    service.saveUserData("notificationCount", count); 
    }
    }, (error) => {
    console.log(error) //Display error
    });
}


chatUserIds = (path) => 
{
//Check if user1’s id is less than user2'

const FirstString = this.state.senderId + "a" + this.state.receiverId;
const secondString = this.state.receiverId + "a" + this.state.senderId;
var newReturn = "";
var idExist = false;
var idValue = 0

itemsRef.child(FirstString).once('value', (snapshot) => {

if (snapshot.val()){

idExist = true;
newReturn = FirstString;
}

});


if (idExist){

this.sendChatImage(newReturn, path);

}else{

itemsRef.child(secondString).once('value', (snapshot) => {

if (snapshot.val()){

idExist = true;
newReturn = secondString;
this.sendChatImage(newReturn, path);

}else{

console.log(FirstString);
console.log(secondString);


newReturn = FirstString;
this.sendChatImage(newReturn, path);

}
});


}

}




handleSubmit(messages) {

this.UserIds(messages)


}

 uuidv4 = () => 
 {
  return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function(c) {
    var r = Math.random() * 16 | 0, v = c == 'x' ? r : (r & 0x3 | 0x8);
    return v.toString(16);
  });
}



notification = (val) => {
  service.getUserData('notificationCount').then((keyValue) => {
    if(keyValue !== "none" || keyValue !== undefined || keyValue !== "")
    {
    var count = ++keyValue;
    service.saveUserData("notificationCount", count); 
    }
    }, (error) => {
    console.log(error) //Display error
    });
}


openGalleryCamera = () => {

  ImagePicker.showImagePicker({title: "", maxWidth: 800, maxHeight: 600}, res => {
    if (res.didCancel) {
      console.log("User cancelled!");
    } else if (res.error) {
      console.log("Error", res.error);
    } else {
      console.log(res);
      
      this.setState({
        pickedImage: { uri: res.uri }
      });
      this.setState({ imagePath: res});
      this.setState({
        imageExists: true
      });
      this.chatUserIds(res.uri)
      
      

    }
  });
}
 guid = () =>  {
  function s4() {
    return Math.floor((1 + Math.random()) * 0x10000)
      .toString(16)
      .substring(1);
  }
  return s4() + s4() + '-' + s4() + '-' + s4() + '-' + s4() + '-' + s4() + s4() + s4();
}


sendChatImage = (child, path) => {
  const imageRef = firebase
        .storage()
        .ref('img')
        .child(this.guid());
      let mime = 'image/jpg';

      imageRef
        .put(path, { contentType: mime })
        .then(() => {
          return imageRef.getDownloadURL();
        })
        .then(url => {
            var now = new Date().getTime()
            itemsRef.child(child).push({
                        user: {
                            _id: this.state.senderId,
                            name: 'React Native',
                            avatar: 'https://reactjs.org/img/logo_og.png',
                          },
                        _id: this.uuidv4(),
                        text: '',
                        createdAt: now,
                        senderId :this.state.senderId,
                    receiverId :this.state.receiverId, 
                    senderName : this.state.senderName || " ",
                    receiverName : this.state.receiverName || " ",
                    image: url
                    })
    
    service.messagesPush(this.state.receiverId, url,this.state.senderId).then((res) => {
    console.log("checkres", res);
    })
           
    
    
    itemsRef.child('users').once('value', (snapshot) => {
    const dataRecieved = snapshot.val();
    if(!dataRecieved)
    {
      console.log(this.state.senderImage)
    console.log("users one")
    itemsRef.child('users').push({
    message: message.text,
    senderId :this.state.senderId,
    receiverId :this.state.receiverId, 
    senderName : this.state.senderName,
    receiverName : this.state.receiverName,
    time : new Date().toLocaleString(),
    receiverImage : this.state.receiverImage,
    senderImage : this.state.senderImage
    });
    
    }
    else
    {
    
    console.log("mymessagesss", this.state.message);
    var receiverId = this.state.receiverId;
    var senderId = this.state.senderId;
    var senderName = this.state.senderName;
    var receiverName = this.state.receiverName;
    var image =  this.state.senderImage;
    var data = "";
    itemsRef.child('users').once('value', (snapshot) => {
    // itemsRef.child('users').on('value', (snapshot) => {
    
    // console.log("users", snapshot.val());
    var messages = [];
    snapshot.forEach(function(childSnapshot) {
    // console.log("user", childSnapshot.val())
    
    if(childSnapshot.val().receiverId == receiverId && childSnapshot.val().senderId == senderId || childSnapshot.val().receiverId == senderId && childSnapshot.val().senderId == receiverId)
    {
    messages.push(childSnapshot.val());
    }
    // console.log('messagesUsers', messages.length);
    
    })
    console.log("messagelength", messages.length)
    
    
    
    if (messages.length == 0 )
    {
    console.log("this messages")
    itemsRef.child('users').push({
    message : message.text,
    senderId : senderId,
    receiverId :receiverId, 
    senderName : senderName,
    receiverName : receiverName,
    time : new Date().toLocaleString(),
    image : image
    }); 
    }
    else{
    console.log("other messages")
    }
    
    });
    console.log("data", data)
    
    }
    });
   
    
          console.log('URL', url);
        });
}

render() {

return (

<SafeAreaView style={{flex:1}}>
<View style={styles.messagestoolbar}>
<TouchableOpacity onPress={() => this.openMessageList()}>
<Image source={constants.backicon} style={styles.hamburgerIcon} />
</TouchableOpacity>
<Text style={styles.toolbarTitle}>{this.state.receiverName}</Text>
<TouchableOpacity onPress={() => this.openGalleryCamera()}>
<Image  style={styles.searchIcon} />
</TouchableOpacity>
</View>
<Text style={styles.centerText}>{this.state.noMessage}</Text>
<GiftedChat
        messages={this.state.messages}
        onSend={messages => this.handleSubmit(messages)}
        user={{
            _id: this.state.senderId,
          }}
      />
</SafeAreaView>
   


);
}
}