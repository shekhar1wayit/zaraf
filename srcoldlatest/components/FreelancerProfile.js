import React, {Component} from 'react';
import {Platform, StyleSheet, Text, Button, Linking, Alert, View, ScrollView, KeyboardAvoidingView, SafeAreaView,Image, TextInput, ImageBackground, ActivityIndicator, TouchableOpacity, TouchableNativeFeedback} from 'react-native';
import styles from '../styles/styles';
import Constants from '../constants/Constants';
import Service from '../services/Service';
import CustomToast from './CustomToast';
import Loader from './Loader';
import ImagePicker from "react-native-image-picker";
import MyView from './MyView';
import OfflineNotice from './OfflineNotice';
import Stars from 'react-native-stars';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import { DocumentPicker, DocumentPickerUtil } from 'react-native-document-picker';
import { strings } from '../services/stringsoflanguages';
export default class FreelancerProfile extends Component {
  
  constructor(props){
    super(props);
    service = new Service();
    constants = new Constants();
    this.state = { 
       userResponse: {},
        email:'',
        name:'',
        about: '',
        loading: false,
        userType : "",
        category :'Category',
        pickedImage: null,
        docImage: null,
        resumeImage: null,
        imagePath : '',
        imageExists : false,
        isFreelancer : false,
        document : 'CV',
        proof : 'ID proof',
        file : "",
        fileID : "",
        ifCV : false,
        skills: '',
        image_path:'https://satishrao.in/wp-content/uploads/2016/06/dummy-profile-pic-male.jpg'
      }
  }
 
 
 
  componentDidMount ()   {
    if(this.props.navigation.state.params)
    {  
        console.log("freelancer", this.props.navigation.state.params);
        service.handleConnectivityChange().then((res) => {
          if(res.type == "none")
          {
            Alert.alert('Alert!', 'Check your internet connection');
          }
          else
          {
            service.getUserProfileData(this.props.navigation.state.params.fDetails.userapi[0].api_token).then((res) => {
              console.log("local", res);
              this.setState({ userResponse: res.user});
              this.setData(res.user);
           }, (error) => {
              console.log(error) //Display error
            });
          }
    })
   }
  }


   setData = (LocalData) => 
   {
    if(LocalData.username !== null)
    {
    this.setState ({ name: LocalData.username});
    }
    if(LocalData.ratings_table !== undefined)
    {
    this.setState ({ rating: ratings_table[0]});
    }
    if(LocalData.image_path !== null)
    {
    this.setState ({ image_path : LocalData.image_path});
    }
    if(LocalData.email !== null)
    {
    this.setState ({ email: LocalData.email});
    }
    if(LocalData.short_bio !== "null")
    {
    this.setState ({ about: LocalData.short_bio});
    }
    if(LocalData.CV  !== null) 
    {
      this.setState({ document : LocalData.CV});
    }
    if(LocalData.identityId !== null) 
    {
      this.setState({ proof : LocalData.identityId});
    }
    if(LocalData.CV !== "") 
    {
      this.setState({ ifCV : true});
    }
    this.setState ({ document: LocalData.CV});
    this.setState ({ proof: LocalData.identityId});
    if(LocalData.categoryId !== null) 
    {
      this.setState ({ category: LocalData.categoryId});
    }
    if(LocalData.skills !== null) 
    {
      this.setState ({ skills: LocalData.skills});
    }
  if (LocalData.usertype == "1")
 {
  this.setState({ isFreelancer: true});
  this.setState({ userType: "Client"});
 }
 else
 {
 this.setState({ userType: "Freelancer"}); 
 }
   }

   
  goToLogin = () =>{
   this.props.navigation.navigate('Login')
  }
  goToSignUp = (userType) =>{
  this.props.navigation.navigate('SignUp', { type: userType })
      }

      openlink = (doc) => {
        if(doc)
        {
        Linking.openURL(doc)
        }
      }
      
      openlink2 = (doc) => {
        if(doc)
        {
        Linking.openURL(doc)
        }
      }



      CheckInternetConnection=()=>{
        service.handleConnectivityChange().then((res) => {
        if(res.type == "none")
        {
          Alert.alert('Alert!', 'Check your internet connection');
        }
        else
        {
          this.updateProfile();
        }
        })
    
     
     }
 
 goBack = () => {
  this.props.navigation.navigate('RequestListsScreen', {   job_id: this.props.navigation.state.params.fDetails.job_id});
}

goToNotification = () =>{

  var Details = {
    id : this.props.navigation.state.params.fDetails.userapi[0].id,
    receiverName : this.props.navigation.state.params.fDetails.userapi[0].username
  }
  this.props.navigation.navigate('Chat', { chatDetails: Details})

  
}
 

  render() {
      defaultImg = 'https://satishrao.in/wp-content/uploads/2016/06/dummy-profile-pic-male.jpg';
      const  ImagePicked =   <TouchableOpacity ><Image source={this.state.pickedImage} style={styles.profilePic}/></TouchableOpacity>
      const  NewImage =   <TouchableOpacity ><Image source={{uri: this.state.userResponse.image_path || defaultImg  }} style={styles.profilePic}/></TouchableOpacity>
    return (
  <SafeAreaView style={styles.MainContainerProfile}>
   <OfflineNotice/> 
       <MyView style={styles.tabsToolbar} >
        <TouchableOpacity onPress={() => this.goBack()}>
        <Image source={constants.backicon} style={styles.hamburgerIcon} />
        </TouchableOpacity>
         <Text style={styles.toolbarTitle}>{strings.Profile}</Text>
         <TouchableOpacity onPress={() => this.goToNotification()}>
        <Image source={constants.messagesIcon} style={{padding:10,tintColor:"white",width:40,height:30,marginRight:10}} />
       
       
        </TouchableOpacity>
      
       
     </MyView>
     
     <ScrollView>
     <KeyboardAvoidingView
      style={styles.container}
      behavior="padding"  
    >
     <View style={{alignItems:'center'}}>
      <MyView style={styles.profileContainer} hide={this.state.imageExists}>
      { NewImage}
      </MyView>
      <MyView style={styles.profileContainer} hide={!this.state.imageExists}>
      { ImagePicked}
      </MyView>
      <View style={{marginTop:5}}>
      <Stars
    default={this.state.rating}
    count={5}
    disabled = {true}
    half={true}
    starSize={50} 
    fullStar={<Icon name={'star'} style={[styles.myStarStyle]}/>}
    emptyStar={<Icon name={'star-outline'} style={[styles.myStarStyle, styles.myEmptyStarStyle]}/>}
    halfStar={<Icon name={'star-half'} style={[styles.myStarStyle]}/>}
  />
  </View>
      </View>
      <TouchableOpacity style={styles.camera} >
         <Image  style={styles.cameraIcon} />
        </TouchableOpacity>
      <View style={{padding:10}}>
      <Text style={styles.themetextColor}>
           {strings.Username}
      </Text>
      <TextInput
            style={styles.postprojectinputprofile}
            underlineColorAndroid="transparent"
            placeholder={strings.Name}
            placeholderTextColor="#AEA9A8"
            autoCapitalize="none"
            editable={false}
            returnKeyType='done'
            value={this.state.name}
          />
          </View>
          <View style={{padding:10}}>
      <Text style={styles.themetextColor}>
           {strings.Emailaddress}
      </Text>
      <TextInput
            style={styles.postprojectinputprofile}
            underlineColorAndroid="transparent"
            placeholder={strings.Email}
            placeholderTextColor="#AEA9A8"
            autoCapitalize="none"
            returnKeyType='done'
            editable={false}
            value={this.state.email}
          />
          </View>
          <View style={{padding:10}}>
      <Text style={styles.themetextColor}>
         {strings.AboutMe}
      </Text>
      <TextInput
            style={styles.about}
            underlineColorAndroid="transparent"
            placeholder={strings.AboutMe}
            placeholderTextColor="#AEA9A8"
            autoCapitalize="none"
            returnKeyType='done'
            editable={false}
            value={this.state.about}
            multiline={true}
            blurOnSubmit={true}
            numberOfLines={4}
          />
          </View>
          <View style={{padding:10}}>
      <Text style={styles.themetextColor}>
           {strings.Usertype}
      </Text>
      <TextInput
            style={styles.postprojectinputprofile}
            underlineColorAndroid="transparent"
            placeholder="User Type"
            placeholderTextColor="#AEA9A8"
            autoCapitalize="none"
            returnKeyType='done'
            value={this.state.userType} editable={false}
          />
          </View>
          
          <MyView style={{padding:10}} >
              <Text style={styles.themetextColor}>
                  {strings.Category}
              </Text>
              <View  style={styles.categoryTextProfile}>
                  <Text style={styles.dateTextColorProfile} >
                  {this.state.category}
                  </Text>
              </View>
          </MyView>
          <MyView style={{padding:10}} >
          <Text style={styles.themetextColor}>
              {strings.Skills}
          </Text>
          <TextInput
                style={styles.postprojectinputprofile}
                underlineColorAndroid="transparent"
                placeholder={strings.Addskills}
                placeholderTextColor="#AEA9A8"
                autoCapitalize="none"
                editable={false}
                returnKeyType='done'
                value={this.state.skills}
                multiline={true}
              />
          </MyView>
          <MyView style={{paddingTop:10}} >
          <View style={styles.rowAlignSideMenuJob2}>
         <View style={{width:"3%"}}> 
                  </View>
                  <View style={{width:"35%"}}>  
                  <Text style={styles.textWrapDetails}> {strings.CV}
                  </Text>
                  </View>
                  <View  style={{width:"5%"}}>
                  </View>
                  <View style={{width:"45%"}}> 
                  <Text style={styles.docDetails}  onPress={() => this.openlink(this.state.document)}> {strings.clicktoview}
                  </Text>
                  </View>
         </View>
         </MyView>
         <MyView style={{paddingTop:10}} >
         <View style={styles.rowAlignSideMenuJob}>
         <View style={{width:"3%"}}> 
                  </View>
                  <View style={{width:"35%"}}>  
                  <Text style={styles.textWrapDetails}> {strings.IDProof}
                  </Text>
                  </View>
                  <View  style={{width:"5%"}}>
                  </View>
                  <View style={{width:"45%"}}> 
                  <Text style={styles.docDetails} onPress={() =>  this.openlink2(this.state.proof)}> {strings.clicktoview}
                  </Text>
                  </View>
         </View>
         </MyView>
              {/* <MyView style={styles.CV} >
                  <Text style={styles.themetextColor}>
                      {strings.Cv}
                  </Text>
                  <View  style={{flexDirection:'row', width:'95%'}}>
                    <View style={styles.inputWidth}>
                    <View  style={styles.docBorder}>
                      <Text style={styles.CVtext}>
                      {this.state.document}
                      </Text>
                    </View>
                    </View>
                    <View style={styles.attachinputWidth}>
                    <TouchableOpacity  style={styles.attachBackground} >
                    <Image style={styles.attachiconWidth} source={constants.attachIcon} />
                    </TouchableOpacity>
                    </View>
                  </View>
                </MyView> 
               <MyView style={styles.proof} >
                  <Text style={styles.themetextColor}>
                     {strings.IdProf}
                  </Text>
                  <View  style={{flexDirection:'row',  width:'95%'}}>
                    <View style={styles.inputWidth}>
                      <View  style={styles.docBorder}>
                        <Text style={styles.CVtext}>
                        {this.state.proof}
                        </Text>
                      </View>
                    </View>
                    <View style={styles.attachinputWidth}>
                      <TouchableOpacity  style={styles.attachBackground} >
                      <Image style={styles.attachiconWidth} source={constants.attachIcon} />
                      </TouchableOpacity>
                    </View>
                  </View>
            </MyView> */}
      </KeyboardAvoidingView>
      </ScrollView>
     
      <View style={styles.toastCenter}>
	    <CustomToast ref = "defaultToastBottom"/>
      </View>
      <Loader
          loading={this.state.loading} />
       </SafeAreaView>
	   
    );
  }
}

