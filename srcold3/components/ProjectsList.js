import React, { Component } from "react";
import {
  Platform,
  StyleSheet,
  FlatList,
  SafeAreaView,
  Alert,
  Text,
  View,
  Image,
  ImageBackground,
  Button,
  TextInput, 
  TouchableOpacity,
  Modal,
  ScrollView
} from "react-native";
import Constants from "../constants/Constants";
import Service from "../services/Service";
import styles from "../styles/styles";
import MyView from './MyView';
import Loader from './Loader';
import SideMenu from './SideMenu';
import OfflineNotice from './OfflineNotice';
import { dimensions } from '../styles/base';
import { strings } from '../services/stringsoflanguages';

export default class ProjectsList extends Component {
  constructor(props) {
    super(props);
    service = new Service();
    sidemenu = new SideMenu();
    constants = new Constants();
    this.state = {
      userResponse: {},
      jobs: {amount : " ", description : " ", freelancer_name : " "}, 
      jobs: {description : " "}, 
      failed: false,
      search : true,
      loading:false,
      dummyText : "",
      projectId : "", 
      jobId: " ",
      loading : "false",
      userResponse: {},
      freeLancerName : "", 
      amount : " ", 
      description : " ",
      message : "",
      isFreelancer : false,
      paid : true
      
    };
   
    
  }

  setModalVisible(visible) {
    this.setState({modalVisible: visible});
  }

  findFreelancer = () => {

  }

  giveRating = () => 
  {
    if(this.state.userResponse.usertype == 1)
    {
    this.props.navigation.navigate('Rating', {project : this.state.jobs})
    }
    else
    {
    // var projectDetails = 
    // {
    //  project : this.state.jobs,
    //  clientParams : this.props.navigation.state.params.client_Details
    // }
    // this.props.navigation.navigate('Rating', {project :  projectDetails})
    }
  }

  componentDidMount() {
    if (this.props.navigation.state.params) {
        console.log(this.props.navigation.state.params.client_Details);
        if(this.props.navigation.state.params.client_Details.jobid !== undefined)
        {
        this.setState({ jobId: this.props.navigation.state.params.client_Details.jobid });
        }
        else
        {
        this.setState({ jobId: this.props.navigation.state.params.client_Details.request_list.jobid });
        }
      }
    this.setState ({ loading: true});
    service.getUserData("user").then(
      keyValue => {
        console.log("local", keyValue);
        var parsedData = JSON.parse(keyValue);
        console.log("json", parsedData);
        if (parsedData.usertype == "2")
   {
  this.setState({ isFreelancer: true});
  }
 
        this.setState({ userResponse: parsedData });
        if(this.state.jobId)
        {

          // CheckInternetConnection=()=>{
          //   service.handleConnectivityChange().then((res) => {
          //   if(res.type == "none")
          //   {
          //     Alert.alert('Alert!', 'Check your internet connection');
          //   }
          //   else
          //   {
              this.getMilestoneList();
            // }
            // })
        
         
       //  }

        
        }
      },
      error => {
        console.log(error); //Display error
      }
    );
    service.saveUserData('count', 1);
  
    
  }

  openDrawer = () => {
     // this.props.navigation.goBack();
    // sidemenu.userData();
  //  this.props.navigation.openDrawer();
  if(this.state.userResponse.usertype == 1)
    {
  this.props.navigation.navigate('Jobs')
    }
    else{
      this.props.navigation.navigate('Home')
    }
  };

  getMilestoneList = () => {
    service.getprojectList(this.state.userResponse.api_token, this.state.jobId).then(res => {
      console.log("reslocal", res);
      if(res.status_code == 400)
      {
        this.setState ({ message: "Invalid job id"});
      }
      setTimeout(() => {
     if (res){
        this.setState ({ loading: false});
         if(res.length ===  0)
        {
        this.setState ({ dummyText: "No Project Found"});
        }
        else
        {
        this.setState({ jobs: res.project});
        if(res.length !== 0 )
        {
        if(res.project)
        {
        service.getMilestoneList(this.state.userResponse.api_token, res.project.id).then(res => {
        console.log(res.length);
        var statusarray = [];
        for (i=0; i<res.milestone.length; i++)
        {
          if(res.milestone[i].status == 3)
          {
          statusarray.push(res)
          }
        }
        console.log('arrylength', statusarray.length)
        console.log('apires', res.milestone.length)
        if(statusarray.length == res.milestone.length)
        {
        this.setState({ paid: false });
        }
        else
        {
          this.setState({ paid: true });
        }
        },  error => {
                console.log(error)
              });
        }
      }
        if (this.state.jobs)
        {
        if(this.state.jobs.freelancer_name !== undefined)
        {
          this.state.freeLancerName = this.state.jobs.freelancer_name
        }
        if(this.state.jobs.amount !== undefined)
        {
          this.state.amount = this.state.jobs.amount
        }
        if(this.state.jobs.description !== undefined)
        {
          this.state.description = this.state.jobs.description 
        }
        this.setState({ projectId: res});
        }
      }
    }
    });
  }, 3000)
  };

  searchPage = () =>{
  this.setState({ search: false});
    }
  
    hideSearch = () =>{
      this.setState({ search: true});
    }

    viewMilestones = () => {
      var projectDetails = {
        project_id : this.state.projectId
      }
    this.props.navigation.navigate('MilestoneList', { details: projectDetails})
    }


  goToPostproject = () => {
    this.props.navigation.navigate("Create", { jobId: this.state.projectId });
  };
 
  goToMilestone()
  {
    if(this.state.userResponse.usertype == 1)
    {
      if(this.state.message == "Invalid job id")
      {
        var projectDetails = {
          freelancerId : this.props.navigation.state.params.client_Details.freelancerid,
          jobId : this.props.navigation.state.params.client_Details.jobid
        }
        this.props.navigation.navigate("createProject", { projectDetails: projectDetails });
      }
      else
      {
      var projectDetails = {
          project_id : this.state.projectId,
          amount : this.state.amount
        }
      this.props.navigation.navigate('MilestoneList', { details: projectDetails})
      }
    }
  }

  render() {
   
    return (
      <SafeAreaView source={constants.loginbg} style={{flex:1}}>
      <OfflineNotice/> 
        <View style={styles.topView}>
        <MyView style={styles.tabsToolbar}>
        <TouchableOpacity onPress={() => this.openDrawer()}>
        <Image source={constants.backicon} style={styles.backIcon} />
        </TouchableOpacity>
         <Text style={styles.toolbarTitle}>{strings.Projectlist}</Text>
         <TouchableOpacity onPress={() => this.goToNotification()}>
        </TouchableOpacity>
         <TouchableOpacity onPress={() => this.goToMilestone()}>
         {this.state.userResponse.usertype == 1 ? <Image source={constants.addIcon} style={styles.searchIcon} /> : <View style={styles.searchIcon}></View> }
         
        </TouchableOpacity>
        </MyView>
       </View>
      {this.state.message == "Invalid job id" ?  <Text style={styles.defaultTextSize3}>{strings.NoProjectFound}</Text> : 
      <View style={{height: dimensions.fullHeight}}>
      <ScrollView style={{flex:1}}>
        <View style={styles.listCenter}>
        <Text style = {styles.defaultTextSize}>{this.state.dummyText}</Text>
        
                <View  style={styles.listCardProjects}>
                <View style={{marginTop:10}}>
                 <View style={styles.rowAlignSideMenuJob }>
                  <View style={{width:"5%"}}> 
                  </View>
                  <View  style={{width:"35%"}}> 
                  <Text style={styles.textWrapDetails}> {strings.Amount}
                  </Text>
                  </View>
                  <View  style={{width:"10%"}}>
                  </View>
                  <View style={{width:"45%"}}> 
                  <Text style={styles.amountDetails}> {this.state.amount } SAR
                  </Text>
                  </View>
                  </View>
            
            
                  <View style={styles.rowAlignSideMenuJob2}>
                  <View style={{width:"5%"}}> 
                  </View>
                  <View style={{width:"35%"}}> 
                  <Text style={styles.textWrapDetails}> {strings.Description}
                  </Text>
                  </View>
                  <View  style={{width:"10%"}}>
                  </View>
                  <View style={{width:"45%"}}> 
                  <Text style={styles.textWrap2Details}> {this.state.description}
                  </Text>
                  </View>
                  </View>

                   <View style={styles.rowAlignSideMenuJob}>
                  <View style={{width:"5%"}}> 
                  </View>
                  <View style={{width:"37%"}}> 
                  <Text style={styles.textWrapDetails}> {strings.Freelancername}
                  </Text>
                  </View>
                  <View  style={{width:"8%"}}>
                  </View>
                  <View style={{width:"35%"}}> 
                  <Text style={styles.textWrap2Details}> {this.state.freeLancerName}
                  </Text>
                  </View>
                  </View>
                  </View>
                </View>
       
                
        </View>
       
      
       
        
       </ScrollView>
       </View>} 
      
        <MyView style = { {justifyContent:"center",alignSelf:"center",   width:'80%',  height:50, backgroundColor:'#F0F0F0',  borderWidth: 1, position:'absolute', bottom:80} } hide={!this.state.isFreelancer}>
         <TouchableOpacity onPress={() => this.viewMilestones()}>
         <Text style={styles.textStyleChat}>{strings.ViewMilestones }</Text>
         </TouchableOpacity>
        </MyView>
        <MyView style = { {justifyContent:"center",alignSelf:"center",   width:'80%',  height:50, backgroundColor:'#F0F0F0',  borderWidth: 1, position:'absolute', bottom:20} } hide={this.state.paid}>
         <TouchableOpacity onPress={() => this.giveRating()}>
         <Text style={styles.textStyleChat}>{strings.GiveRating }</Text>
         </TouchableOpacity>
        </MyView>
      </SafeAreaView>
    );
  }
}

