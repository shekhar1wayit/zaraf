import React, { Component } from "react";
import {
  Platform,
  StyleSheet,
  FlatList,
  SafeAreaView,
  Text,
  View,
  Image,
  ImageBackground,
  Button,
  TextInput, 
  TouchableOpacity,
  Modal,
  ScrollView,
  Dimensions,
  Alert
} from "react-native";
import OfflineNotice from './OfflineNotice';
import CustomToast from './CustomToast';
import Constants from "../constants/Constants";
import Service from "../services/Service";
import styles from "../styles/styles";
import MyView from './MyView';
import Loader from './Loader';
import SideMenu from './SideMenu';
import { strings } from '../services/stringsoflanguages';
import { dimensions } from "../styles/base";
const { width , height } = Dimensions.get('window');



export default class MilestoneList extends Component {
  constructor(props) {
    super(props);
    service = new Service();
    sidemenu = new SideMenu();
    constants = new Constants();
    this.state = {
      userResponse: {},
      jobs: [],
      failed: false,
      search : true,
      loading:false,
      dummyText : "",
      modalVisible: false.getMilestoneList,
      projectId: " ",
      collapsed:false,
      projectAmount : " ",
      milestoneTotalAmount : 0,
      hidePlus : 0
    };
   
    
  }

  setModalVisible(visible) {
    this.setState({modalVisible: visible});
  }

  findFreelancer = () => {

  }

  componentDidMount() {
    if (this.props.navigation.state.params) {
        console.log(this.props.navigation.state.params.details);
        if(this.props.navigation.state.params.details.project_id.project != undefined)
        {
          console.log("this one")
          this.setState({ projectId: this.props.navigation.state.params.details.project_id.project.id });
          this.setState({ jobId: this.props.navigation.state.params.details.project_id.project.job_id });
         
        }
        else
        {
        this.setState({ projectId: this.props.navigation.state.params.details.project_id });
        this.setState({ jobId: this.props.navigation.state.params.details.job_id });
       
        }
        this.setState({ projectAmount: this.props.navigation.state.params.details.amount });
      }
    this.setState ({ loading: true});
    service.getUserData("user").then(
      keyValue => {
        console.log("local", keyValue);
        var parsedData = JSON.parse(keyValue);
        console.log("json", parsedData);
        this.setState({ userResponse: parsedData });
        this.getMilestoneList();
        console.log('projectAmount', this.state.projectAmount)
        console.log('milestoneAmount', this.state.milestoneTotalAmount)
        this.checkamount();
       
      },
      error => {
        console.log(error); //Display error
      }
    );
   
   // service.saveUserData('count', 1);
  }

  changeStatus = (val) => 
  {
    console.log(val)
  if(this.state.userResponse.usertype == 1 )
  {
  var status = 2;
  if(val.status == 2)
  {
    status = 3
  }
  if(val.status != 3)
  {
     this.setState ({ loading: true});   
    service.completeMilestone(this.state.userResponse.api_token, val.id, status).then(res => {
      console.log("reslocal", res);
      if(res != undefined) {
        if(res.status == "success")
        {
           this.setState ({ loading: false}); 
           setTimeout(() => 
           {
          console.log(res)
        Alert.alert(strings.StatusUpdatedSuccessfully);
         this.getMilestoneList();
      }, 500);
    }
        else{
          Alert.alert("an error occured")
        }
      }
            },   error => {
              Alert.alert(JSON.stringify(error))
            }
          )
   }
   }
  }

  openDrawer = () => {
    // sidemenu.userData();
    var jobId = {
      jobid : this.state.jobId
    }
    console.log('jobId', this.state.jobId)
    this.props.navigation.navigate('OpenProjects', { client_Details: jobId}) 
  };

  
  getMilestoneList = () => {
    console.log("projectId",  this.state.projectId)
    if(this.state.projectId)
    {
    service.getMilestoneList(this.state.userResponse.api_token, this.state.projectId).then(res => {
      console.log("reslocal", res);
     // this.setState ({ loading: false});
      if(res.status_code != 400)
      {
              setTimeout(() => {
              if (res){
                this.setState ({ loading: false});
                console.log('length', res.milestone.length)
                var totalAmount = 0;
                for (i=0; i<res.milestone.length; i++)
                {
                 
                  totalAmount += parseFloat(res.milestone[i].amount);
                }
                console.log('TA', totalAmount)
                this.setAmount(totalAmount)
               
                console.log('total', totalAmount);
                if(res.milestone.length ===  0)
                {
                    this.setState ({ dummyText: strings.NoMilestonefound});
                }
                else
                {
                this.setState({ jobs: res.milestone});
                this.setState ({ dummyText: " "});
                }
            }
            });
      }
      else
      {
        this.refs.defaultToastBottom.ShowToastFunction('An Error Occurred');
      }
   }, 3000)
   }
   this.setState ({ loading: false});
   this.setState ({ dummyText: strings.NoMilestonefound});
  };

  searchPage = () =>{
  this.setState({ search: false});
    }
  
    hideSearch = () =>{
      this.setState({ search: true});
    }

    setAmount = (totalAmount) => 
    {
      this.setState ({ milestoneTotalAmount: totalAmount});
      console.log('amount', totalAmount)
      console.log('projectAmount', this.state.projectAmount)
    //  alert(totalAmount);
  if(this.state.projectAmount != 0 && totalAmount != 0 )
  {
    if(this.state.projectAmount <= totalAmount )
    {
    
     this.setState({ hidePlus : 1})
    }
  }
  }


  goToPostproject = () => {
    if(this.state.userResponse.usertype == 1)
    {
    if(this.state.hidePlus == 0)
    {
    var projectMilestoneDetails = {
      projectAmount : this.state.projectAmount,
      milestoneAmount : this.state.milestoneTotalAmount,
      jobId : this.state.projectId
    }
    this.props.navigation.navigate("Create", { milestoneDetails: projectMilestoneDetails });
    }
  }
  };

  openDetails = (val) => {
   var jobData = {
     token : this.state.userResponse.api_token,
     details : val
   }
   this.props.navigation.navigate('JobDetails',  { details: jobData }) 
  }

  render() {
    console.log('amount', this.state.projectAmount)
    return (
      <SafeAreaView source={constants.loginbg} style={styles.container}>
        <OfflineNotice/> 
        <View style={styles.topView}>
        <MyView style={styles.tabsToolbar}>
        <TouchableOpacity onPress={() => this.openDrawer()}>
        <Image source={constants.backicon} style={styles.backIcon} />
        </TouchableOpacity>
         <Text style={styles.toolbarTitle}>{strings.Milestonelist}</Text>
         <TouchableOpacity onPress={() => this.goToNotification()}>
        </TouchableOpacity>
         <TouchableOpacity onPress={() => this.goToPostproject()}>
         {this.state.userResponse.usertype == 1 ? <Image source={ this.state.hidePlus == 0 ? constants.addIcon : null} style={styles.searchIcon} /> : <View style={styles.searchIcon}></View> }
        </TouchableOpacity>
        </MyView>
       </View>
       {/* <View style={styles.searchPadding}>
       <MyView  style={styles.searchContainer}>
          <View style={styles.topSearchbar}>
              <Image source={constants.searchicon} style={styles.newsearchIcon} />
              <View style={styles.empty}>
              </View>
            <TextInput placeholder="Search"  placeholderTextColor="#a2a2a2" style={styles.topInput}/>
          </View>
      </MyView>
      </View> */}
       {/* <ScrollView>
       <Text style = {styles.defaultTextSize}>{this.state.dummyText}</Text>
        <View style={styles.listCenter}>
        </View>
        
       
       </ScrollView> */}
       
        
        <FlatList
              data={this.state.jobs}
              keyExtractor={(item, index) => index}
              style={styles.listCardWidthMilestone}
              extraData={this.state.jobs}
              renderItem={({ item, index }) => (
                <View  style={styles.spaceFromTop}>
                    <TouchableOpacity style={styles.listCardMilestoneList}>
                    <View style={{marginTop:10}}>
                    <TouchableOpacity style={styles.statusButton} activeOpacity={0.5} onPress={() => this.changeStatus(item)}>
                    {this.state.userResponse.usertype == 1 ? (item.status == 1  ?  <Text style={styles.TextStyle2}> {strings.Finished}</Text> : (item.status  == 2) ? <Text style={styles.TextStyle2}>{strings.Pay} </Text> : <Text style={styles.TextStyle2}>{strings.Paid} </Text> ) : (item.status == 1  ?  <Text style={styles.TextStyle2}> {strings.Created}</Text> : (item.status  == 2) ? <Text style={styles.TextStyle2}>{strings.Finished} </Text> : <Text style={styles.TextStyle2}>{strings.Paid} </Text> )}
             </TouchableOpacity>
                  <View style={{marginTop:15, height:100}}>
                   <View style={styles.rowAlignSideMenuJob}>
                   <View style={{width:"5%"}}> 
                  </View>
                  <View  style={{width:"35%"}}> 
                  <Text style={styles.textWrapDetails}>{strings.Amount}
                  </Text>
                  </View>
                  <View  style={{width:"10%"}}>
                  </View>
                  <View style={{width:"45%"}}> 
                  <Text> {item.amount} SAR
                  </Text>
                  </View>
                  </View>
                  <View style={styles.rowAlignSideMenuJob2}>
                  <View style={{width:"5%"}}> 
                  </View>
                  <View style={{width:"35%"}}> 
                  <Text style={styles.textWrapDetails}>{strings.DueDate}
                  </Text>
                  </View>
                  <View  style={{width:"10%"}}>
                  </View>
                  <View style={{width:"45%"}}> 
                  <Text style={styles.textWrap2Details}> {item.end_date}
                  </Text>
                  </View>
                  </View>
                  <View style={styles.rowAlignSideMenuJob}>
                  <View style={{width:"5%"}}> 
                  </View>
                  <View style={{width:"35%"}}> 
                  <Text style={styles.textWrapDetails}>{strings.Description}
                  </Text>
                  </View>
                  <View  style={{width:"10%"}}>
                  </View>
                  
                  <View style={{width:"45%"}}> 
                 
                  <Text style={styles.textWrap2Details}> {item.description}
                  </Text>
                 
                  </View>
                
                 
  
                  </View>
                  </View>
                  </View>
                     
                         
                  
                        </TouchableOpacity>
                </View>
           
     
              )}
            />
           
   
        <Loader
              loading={this.state.loading} />
              <CustomToast ref = "defaultToastBottom"/> 
                    
      </SafeAreaView>
    );
  }
}
