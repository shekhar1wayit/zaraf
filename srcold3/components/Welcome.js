import React, {Component} from 'react';
import {Platform, StyleSheet, Text, View,StatusBar, Animated, ScrollView, Image, ImageBackground, ActivityIndicator, TouchableOpacity} from 'react-native';
import styles from '../styles/styles';
import Service from '../services/Service';
import { colors, fonts, padding, dimensions, align } from "../styles/base.js";
import { strings } from '../services/stringsoflanguages';
import OfflineNotice from './OfflineNotice';
import firebase  from './Config';
import CustomToast from './CustomToast';
export default class Welcome extends Component {
  
  constructor(props){
    super(props);
   // console.log('propvalue', props);
    service = new Service();
    this.Animation = new Animated.Value(0);
    this.state = {
       userResponse: {},
       id: '',
       englishImage: constants.fillrdrIcon,
       arabicImage: constants.unfilledrIcon,
       x: new Animated.Value(0)
    }
     
}

componentDidMount ()   {
  this.StartBackgroundColorAnimation();
  firebase.notifications().setBadge(0);
  service.getUserData('user').then((keyValue) => {
    console.log("local", keyValue);
    var parsedData = JSON.parse(keyValue);
    console.log("sidemenujson", parsedData);
    if(parsedData)
    {
      if(parsedData.usertype !== null)
      {
        if(parsedData.usertype == 1 )
      {
      this.props.navigation.navigate('Jobs')
      }
      else
      {
        this.props.navigation.navigate('Home') 
      }
      }
   }
    this.setState({ userResponse: parsedData});
 }, (error) => {
    console.log(error) //Display error
  });

  
  service.getUserData("language").then(
    keyValue => {
      console.log("local",  keyValue)
    if(keyValue == "none")
    {
      service.saveUserData("language", true);
    }
},
    error => {
      console.log(error); //Display error
    }
  );
 }
 StartBackgroundColorAnimation = () =>
    {
        this.Animation.setValue(0);
 
        Animated.timing(
            this.Animation,
            {
                toValue: 1,
                duration: 15000
            }
        ).start(() => { this.StartBackgroundColorAnimation() });
    }



 englishTap = () => {
  service.saveUserData("language", true);
  strings.setLanguage("en");
  if (this.state.check) {
    this.setState({ englishImage: constants.fillrdrIcon });
    this.setState({ arabicImage: constants.unfilledrIcon });
    this.setState({ check: false });
    strings.setLanguage("en");
  
    service.saveUserData("language", true);
  }
 
};

ArabicTap = () => {
  if (!this.state.check) {

  this.setState({ englishImage: constants.unfilledrIcon });
  this.setState({ arabicImage: constants.fillrdrIcon });
  this.setState({ check: true });
  strings.setLanguage("ar");

  service.saveUserData("language", false);

  }
};


overLang() {
  if (this.state.check) {
    //this.setState({ check: false });
    strings.setLanguage("en");
    //alert("t_f");
  } else {
    //this.setState({ check: true });
    strings.setLanguage("ar");
    //alert("f_t");
  }
}



  // going to next screen
  goToLogin = () =>{
   this.props.navigation.navigate('Login')
  }
  
  goToSelect = () =>{
       this.props.navigation.navigate('Login')
      }

  
  render() {
    const BackgroundColorConfig = this.Animation.interpolate(
      {
          inputRange: [ 0, 0.2, 0.4, 0.6, 0.8, 1, 1.2 ],
          
          outputRange: [ '#ffffff', '#FFEB3B', '#CDDC39', '#009688', '#03A9F4', '#3F51B5', '#FFEB3B' ]

      });
    return (
     
       <ScrollView style={{flex:1}}>
         <OfflineNotice/> 
       
	     {
     
       <Animated.View style = {[ styles.welcomeContainer, { backgroundColor: 'white' } ]}>
	     <View style={styles.welcomeHeadline}>
	      
         <Image
            source={require("../images/logosmal.png")}
            style={{ padding: 2 }}
          />


		   <TouchableOpacity style={styles.buttonBackground} onPress={() => this.goToSelect()}>
		     <Text style={styles.buttonText}>{strings.CreateAnAccount}</Text>
		   </TouchableOpacity>

       <View style={styles.rowAlignSideMenu}>
		    <Text style={styles.accountText}>{strings.Alreadyhaveanaccount} </Text>
        <TouchableOpacity>
        <Text onPress={() => this.goToLogin()} style={styles.welcomeLoginText}>{strings.Login}</Text>
        </TouchableOpacity>
        </View>

 <View
          style={{
            backgroundColor: "#f0f0f0",
            alignItems: "flex-start",
            width: dimensions.fullWidth - 20,
            marginLeft: 10,
            marginTop: 30,
            height: 50,
            justifyContent: "flex-start",
            flexDirection: "row",
            alignItems: "center"
          }}
        >
          <Image
            source={constants.langIcon}
            style={{ marginLeft: 12, width: 25, height: 25 }}
          />
          <Text style={{ marginLeft: 12 }}>{strings.Language}</Text>
          <View
            style={{
              left: dimensions.fullWidth / 2 - 150,
              backgroundColor: "clear",
              width: dimensions.fullWidth / 2,
              height: 50,
              flexDirection: "row"
            }}
          >
            <TouchableOpacity onPress={() => this.englishTap()}>
              <View
                style={{
                  marginLeft: 0,
                  height: 50,
                  backgroundColor: "clear",
                  width: dimensions.fullWidth / 4,
                  flexDirection: "row",
                  alignItems: "center"
                }}
              >
                <Image
                  source={this.state.englishImage}
                  style={{ marginLeft: 0, width: 25, height: 25 }}
                />
                <Text style={{ marginLeft: 8 }}>English</Text>
              </View>
            </TouchableOpacity>
            <TouchableOpacity onPress={() => this.ArabicTap()}>
              <View
                style={{
                  marginLeft: 0,
                  height: 50,
                  backgroundColor: "clear",
                  width: dimensions.fullWidth / 4,
                  flexDirection: "row",
                  alignItems: "center"
                }}
              >
                <Image
                  source={this.state.arabicImage}
                  style={{ marginLeft: 0, width: 25, height: 25 }}
                />
                <Text style={{ marginLeft: 8 }}>Arabic</Text>
              </View>
            </TouchableOpacity>
          </View>
        </View>

	     </View>
       </Animated.View>
       
       
       }
	   </ScrollView>
  
	   
    );
  }
}

