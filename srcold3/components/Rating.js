import React, {Component} from 'react';
import {Platform, StyleSheet,Alert,  TextInput, SafeAreaView, Text, View, Image, ImageBackground, Button, TouchableOpacity} from 'react-native';
import Constants from '../constants/Constants';
import Service from '../services/Service';
import MyView from './MyView';
import { strings } from '../services/stringsoflanguages';
import Stars from 'react-native-stars';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import Loader from './Loader';
// import Stars from 'react-native-stars-rating';

export default class Rating extends Component {
 constructor(props){
     super(props);
     service = new Service();
     constants = new Constants();
     this.state = {
        userData: { picture_large:{ data:{}}},
        search : true,
        starCount : 2,
        reviewText : "",
        userResponse: {},
        freelancer_Key : "",
        project_id:"",
        loading : false, 
      };
   
 }

 componentDidMount() {
  if (this.props.navigation.state.params) {
    console.log(this.props.navigation.state.params);
    this.setState ({project_id : this.props.navigation.state.params.project.id })
    this.setState ({freelancer_Key : this.props.navigation.state.params.project.api_token })
  }
service.getUserData("user").then(
keyValue => {
console.log("local", keyValue);
var parsedData = JSON.parse(keyValue);
console.log("json", parsedData);
this.setState({ userResponse: parsedData });
},
error => {
console.log(error); //Display error
}
);
}

goBack = () => {
  if(this.state.userResponse.usertype == 1)
  {
    this.props.navigation.navigate('Jobs')
  }
  else
  {
  this.props.navigation.navigate('Home')
  }
}


 openDrawer = () => {
   this.props.navigation.openDrawer()}

   submitReview = () => {
    console.log("submit");
    
    if (this.state.reviewText == "" || this.state.reviewText.length == 0){
    
    
    }else{
    
       this.setState({loading : true})
     console.log('token', this.state.userResponse.api_token, 'projectId',this.state.project_id, 'text',this.state.reviewText, 'starCount',   this.state.starCount)
      service.ratingAction(this.state.userResponse.api_token,this.state.freelancer_Key,this.state.project_id, this.state.reviewText,this.state.starCount)
      .then(res => {
        console.log(res);
    if (res != undefined) {
    if (res.status_code == 200) {
    if (res.status == "success") {
       this.setState({loading : false})
      setTimeout(() => 
      {
     console.log(res)
   Alert.alert(strings.SubmitSuccessfully);
   this.props.navigation.navigate('Jobs')
  }, 500);
    
    
    }
    
    }
    else
    {
      Alert.alert('An Error Occured');
    }
    
  }
     else 
     {
      Alert.alert('An Error Occured');
    }
    }
   ,   error => {
      Alert.alert(JSON.stringify(error))
    });
    }
    
    }
    
    cancelReview = () => {
    console.log("cancel");
    
    
    }
    
    
    onStarRatingPress(rating) {
    console.log(rating);
    this.setState({
    starCount: rating
    });
    }


  render() {
   
    return (
        
     <SafeAreaView
      source={constants.loginbg}
      style={styles.container}>
     {/* <CreditCardInput onChange={this._onChange} />  */}
    <View style={styles.topView}>
      
      <View style={styles.toolbar}>
        <Text style={styles.backButton} onPress={() => this.goBack()}>
        <Image source={constants.backicon} style={styles.iconRating}/>
        </Text>
          <Text style={styles.toolbarTitle}>RATE&REVIEW</Text>
          <TouchableOpacity onPress={() => this.goToUpdateProfile()}>
          <Image  style={styles.searchIcon} />
          </TouchableOpacity>
        </View>
     </View>
     <View style={styles.homeContent}>
         
         <TextInput
            style={styles.review}
            underlineColorAndroid="transparent"
            placeholder={strings.Writeareview}
            onChangeText={(text)=>this.setState({ reviewText:text})}
            placeholderTextColor="#AEA9A8"
            autoCapitalize="none"
            returnKeyType='done'
            value={this.state.about}
            multiline={true}
            numberOfLines={4}
          />
         
         
     </View>
     <View style={{marginTop:20 }}>
     <Stars
    default={2.5}
    count={5}
    half={true}
    starSize={50} 
    update={(val)=>{this.onStarRatingPress(val)}}
    fullStar={<Icon name={'star'} style={[styles.myStarStyle]}/>}
    emptyStar={<Icon name={'star-outline'} style={[styles.myStarStyle, styles.myEmptyStarStyle]}/>}
    halfStar={<Icon name={'star-half'} style={[styles.myStarStyle]}/>}
  />
      
       
      </View>
      <View  style={styles.sideAlign}>
              <View style={styles.emptySpaceRating}>
              </View>
              <View style={styles.buttonWidthRating}>
              <TouchableOpacity style={styles.cancelButton} onPress={() => this.goBack()}>
		          <Text style={styles.buttonText}>{strings.Cancel}</Text>
		           </TouchableOpacity>
               {/* <Button  style={styles.buttonColor} title="Accept" onPress={() => this.requestAcceptReject('a')}></Button> */}
              </View>
              <View style={styles.emptySpaceRating}>
              </View>
              <View style={styles.buttonWidthRating}>
              <TouchableOpacity style={styles.cancelButton} onPress={() => this.submitReview()}>
		          <Text style={styles.buttonText}>{strings.Submit}</Text>
		           </TouchableOpacity>
                {/* <Button  style={styles.buttonColor} title="Reject" onPress={() => this.requestAcceptReject('r')}></Button> */}
              </View>
                <View style={styles.emptySpaceRating}>
              </View>
      </View> 
     
     
      <Loader
              loading={this.state.loading} />           
 </SafeAreaView>
      
     
    );
  }
}
