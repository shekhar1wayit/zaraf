import React, {Component} from 'react';
import {Platform,FlatList, StyleSheet, SafeAreaView, Text, View, Image, ImageBackground, Button, TouchableOpacity} from 'react-native';
import Constants from '../constants/Constants';
import Service from '../services/Service';
import FCM, {FCMEvent} from "react-native-fcm";
import firebase  from './Config';
import { strings } from '../services/stringsoflanguages';
import OfflineNotice from './OfflineNotice';
import Loader from './Loader';
import { dimensions } from '../styles/base';

export default class Notifications extends Component {
 constructor(props){
     super(props);
     service = new Service();
     constants = new Constants();
     this.state = {
        userData: { picture_large:{ data:{}}},
        loading: false,
        notCount : "",
        notificationText : " ",
        details : [],
        refreshing: false
      };
   
 }



 
   componentDidMount() {
    service.saveUserData("notificationCount", 0); 
    firebase.notifications().setBadge(1);
    this.setState ({ loading: true});
    setTimeout(() => {
      this.setState ({ loading: false});
      service.getUserData('user').then((keyValue) => {
        console.log("local", keyValue);
        var parsedData = JSON.parse(keyValue);
        console.log("json", parsedData);
        this.setState({ userResponse: parsedData});
        this.getDetails()
     }, (error) => {
        console.log(error) //Display error
      });

        
       
      }, 1000)  
  }

        getDetails = () => {
          // console.log(object)
          service.getNotifications(this.state.userResponse.id).then((res) => {
            console.log("new data", res);
            if(res != undefined)
            {
              this.setState({ details: res.notification_data});
              this.setState({ requestsText : strings.NoNotificationRecieved  });
              console.log("here are details",this.state.details)
      
            }
          })
          this.setState(
            {
              refreshing: false
            })
         }

         itemClickedAction = (item) => {
         console.log('detail', item)
    if(this.state.userResponse.usertype == "1")
   {

    if (item.title == "User Notification"){
      this.props.navigation.navigate("Chat", { chatDetails: item});

    }else{
      this.props.navigation.navigate('Jobs')


    }

  }
   else
   {
    if (item.title == "User Notification"){

      this.props.navigation.navigate("Chat", { chatDetails: item});

    }else{
    this.props.navigation.navigate('Home')

    }
  }
   }

   onRefresh = () => {
    this.setState(
      {
        refreshing: true
      })
      this.getDetails();
  }
        
         openDrawer = () => {
          this.props.navigation.openDrawer()}
  render() {
    const defaultImg =
    'https://satishrao.in/wp-content/uploads/2016/06/dummy-profile-pic-male.jpg'

    return (
        
     <SafeAreaView
      source={constants.loginbg}
      style={styles.container}>
      <OfflineNotice/> 
      <View style={styles.toolbar} >
        <TouchableOpacity onPress={() => this.openDrawer()}>
        <Image source={constants.menuicon} style={styles.hamburgerIcon} />
        </TouchableOpacity>
         <Text style={styles.toolbarTitle}>{strings.Notifications}</Text>
         <TouchableOpacity >
        <Image source={{}} style={styles.searchIcon} />
        </TouchableOpacity>
     </View>
     {this.state.details.length > 0 ? <FlatList
                    data={this.state.details}
                    keyExtractor={(item, index) => index}
                    style={{ marginTop: 10,marginBottom:60 }}
                    onRefresh={() => this.onRefresh()}
                    refreshing={this.state.refreshing}
                    extraData={this.state.details}
                    renderItem={({ item, index }) => (
                        <View style={styles.spaceFromTop}>
                        
                            <TouchableOpacity style={{width:dimensions.fullWidth - 30, backgroundColor:'#F0F0F0'}} onPress={() => this.itemClickedAction(item)}>
                                <View style={styles.textInRow} >
                                <View style={{width:'20%'}} >
                                   
                                <Image source={{ uri: item.image_path || defaultImg  }} style={{margin:5, width:50, height:50, borderRadius:25}} />
                                    </View>
                                    <View style={{width:'80%'}} >
                                    <Text style={{margin:5,fontSize:16,color:'#fb913b', textTransform:"capitalize"}}> {item.title}</Text>
                                    <Text style={{margin:5,fontSize:14, textTransform:"capitalize"}}> {item.message}
                                    </Text>
                                    <Text style={{margin:5,fontSize:14, textTransform:"capitalize"}}> {item.updated_at}
                                    </Text>
                                    </View>
                                </View>                               
                            </TouchableOpacity>
                        </View>
                    )}
                        /> : <Text style={styles.defaultTextSize}> {this.state.requestsText}</Text>}
                        <Loader
loading={this.state.loading} />
      </SafeAreaView>
      
     
    );
  }
}
